<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/sidebars/SidebarManager.php');
?>
<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar(SidebarManager::getInstance()->getSidebarName('sk-homesocial-sidebar')) ) : ?>
	<!--no widgets added to social sidebar-->
<?php endif; ?>