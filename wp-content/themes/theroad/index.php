<?php get_header(); ?>
			<?php
				$menuColors = $view->getMenuColors();
				$menuShadowRGB = $view->html2rgb($menuColors['menuTextShadowColor']);
				$menuItems = $view->getMenuItems('main');
				$fm_items = formatItems($menuItems);
				$menuRGBA = $view->getMenuRGBA();
			?>
    	<!--home page-->
        <div id="homeSectionID" class="slide homeSlide" data-iscover="ON" data-backurl="<?php echo $view->getHompageBackgroundURL();?>" data-stellar-background-ratio="0.5" data-type="homeSection" style="background: url('<?php echo $view->getHompageBackgroundURL();?>');background-attachment:fixed; background-position: center top; background-size:cover;" data-mobile-parallax="<?php echo $view->isParallaxMobile();?>">
        	
	        <!--mobile menu-->
	        <?php if(sizeof($menuItems)!=0): ?>	        
	        <div class="mobileMenu removeMenu" style="background-color: #<?php echo $menuColors['menuBackgroundColor']?>;">
	        	<select id="mobileSelect">
	        		<option class="menuOption" data-linkedid="homeSectionBTN" data-href="#homeSectionID"><?php _e('Home', 'default_textdomain');?></option>
	        		<?php
						for ($j=0; $j < sizeof($fm_items['rootItems']); $j++){
							echo '<option class="menuOption" data-linkedid="'.$fm_items['rootItems'][$j]['ID'].'" data-href="'.$fm_items['rootItems'][$j]['menuItemURL'].'">'.$fm_items['rootItems'][$j]['menuItemTitle'].'</option>';
						}	        			
	        		?>

	        	</select>
	        </div>
	        <?php endif;?>
	        <!--/mobile menu-->         	
        	
        	<!--home wrapper-->
        	<div id="homewrapper">
        		
        		
        		<?php if($view->isHomeSocialSidebar()):?>
	        	<!--social sidebar-->
	        	<div id="homeSocialSidebarContainer" data-stellar-ratio="1.5">
	        	<ul id="homeSocialSidebar">
	            	<?php
	            		require_once(CLASS_PATH.'/com/sakurapixel/php/sidebars/SidebarManager.php');
	            		get_sidebar(SidebarManager::getInstance()->getSidebarFILE('sk-homesocial-sidebar'));            		 
	            	?>  
	        	</ul>
	        	</div>
	        	<!--/social sidebar-->
	        	<?php endif;?>
	        	
	        	<?php if($view->isHomeScrollDown()):?>
	        	<!--scroll down container-->
	        	<div class="homeScrollDown" data-stellar-ratio="1.8">
	        		<div class="scrollDownImgUI">
	        			<div class="scrollDIMG">
	        				<img src="<?php echo IMAGES?>/scroll_down.png" alt="" />
	        			</div>
	        		</div>
	        	</div>
	        	<!--/scroll down container-->
	        	<?php endif;?>
	        	
	        	        		
	        	<!--logo container-->
	        	<div id="logoContainer" data-stellar-ratio="2" data-logo-url="<?php echo $view->getThemeLogoURL();?>">
	        		<div id="innerLogoContainer">
	        			<div id="logoSpacerTop"></div>
	        		</div>
	        		<div id="logoArrowDown"><img src="<?php echo IMAGES."/logo_background_arrow.png"?>" alt="" /></div>
	        	</div>
	        	<!--/logo container-->	        	
        	
        	</div>
        	<!--home wrapper-->
        	
        </div>
        <!--/home page-->
        
			
	        <!--menu-->
	        <div class="mainMenu" style="background-color: #<?php echo $menuColors['menuBackgroundColor']?>; background:rgba(<?php echo $menuRGBA[0];?>,<?php echo $menuRGBA[1];?>,<?php echo $menuRGBA[2];?>,0.90);">
	        	<ul>
	        		<?php if(sizeof($menuItems)==0): ?>
	        			<p><?php _e('Please asign menu from admin - @See documentation', 'default_textdomain')?></p>
	        		<?php else:?>	        			
	        			<li><a style="color: #<?php echo $menuColors['menuTextColor']?>; text-shadow: 0px 0.5px 1px rgba(<?php echo $menuShadowRGB[0];?>,<?php echo $menuShadowRGB[1];?>,<?php echo $menuShadowRGB[2];?>,0.6);" id="homeSectionBTN" href="#homeSectionID"><?php _e('Home', 'default_textdomain');?></a></li>
	        			<?php																
	                        	for ($i=0; $i < sizeof($fm_items['rootItems']); $i++) {							
										echo '
										<li>
											<a style="color: #'.$menuColors['menuTextColor'].'; text-shadow: 0px 0.5px 1px rgba('.$menuShadowRGB[0].','.$menuShadowRGB[1].','.$menuShadowRGB[2].',0.6);" class="firstLevelItem" id="'.$fm_items['rootItems'][$i]['ID'].'" href="'.$fm_items['rootItems'][$i]['menuItemURL'].'">'.$fm_items['rootItems'][$i]['menuItemTitle'].'</a>											
										</li>';
								}       			
	        			?>
	        		<?php endif; ?>
	        	</ul>
	        </div>
	        <!--/menu-->
	        	        
                 
        <?php
        	require_once(CLASS_PATH.'/com/sakurapixel/php/stylehelper.php');
			$stH = new StyleHelper();
			$footerBackgroundStyle = $stH->getFooterBackground();
			$footerColors = $stH->getFooterColors();
        ?>
        <!--footer slide-->
        <div class="slide footerSlide" data-stellar-background-ratio="0.5" style="min-height: 1%;<?php echo $footerBackgroundStyle;?>">
			<div class="slideWrapper container" style="padding-top: 0px;">
								
					<!--footer sidebars-->
					<div class="footerSidebar">
			            	<?php
			            		require_once(CLASS_PATH.'/com/sakurapixel/php/sidebars/SidebarManager.php');
			            		get_sidebar(SidebarManager::getInstance()->getSidebarFILE('sk-footer-sidebar'));            		 
			            	?>
			            	<div class="clear-fx"></div>
					</div>
					<!--/footer sidebars-->
					
					<!--footer links-->
					<div class="sixteen columns footerLinks">
						<div class="slideHeaderLine footerLinesColor"></div>
						<ul class="footerLinksList">
			            	<?php
			            		require_once(CLASS_PATH.'/com/sakurapixel/php/sidebars/SidebarManager.php');
			            		get_sidebar(SidebarManager::getInstance()->getSidebarFILE('sk-footerlinks-sidebar'));            		 
			            	?> 
						</ul>
					</div>
					<!--/footer links-->
					
			</div>
        </div>
        <!--/footer slide-->        
        
    	<!--main preloader-->
	    <div id="mainPreloader" data-logo="<?php echo $view->getThemePreloadLogoURL();?>">
	    	<p class="mainPreloaderText mainPreloaderItem"><?php _e('LOADING', 'default_textdomain')?></p>
	    </div>
	    <!--/main preloader-->
	    
	    <!--secure screen-->
	    <div id="secureScreen" class="secure"></div>
	    <!--/secure screen-->
	    
	    <!--portfolio lightbox-->
	    <div id="lightboxWrapper">
		    <div class="portfolioLightbox hidePortfolioLightbox">
		    	<img class="imgLoadingLightbox" src="<?php echo $view->getThemePreloadLogoURL();?>" alt="" />
		    	<p class="mainPreloaderText mainPreloaderItem"><?php _e('LOADING', 'default_textdomain')?></p>
		    	<div class="lightboxImageContainer"></div>
		    	<img class="preloaderGIF" src="<?php echo IMAGES.'/preloader.gif';?>" alt="" />
		    	<p class="lightboxTitle portfolioMainColor"></p>
		    	<div class="lightboxControls">	    		
		    		<a href="#" class="genericBoxButton portfolioButtonColor floatRight lightBxRight" style="margin-left: 1px;"><img src="<?php echo IMAGES.'/right.png';?>" alt="" /></a>
		    		<a href="#" class="genericBoxButton portfolioButtonColor floatRight lightBxClose" style="margin-left: 1px;"><img src="<?php echo IMAGES.'/close.png';?>" alt="" /></a>
		    		<a href="#" class="genericBoxButton portfolioButtonColor floatRight lightBxLeft"><img src="<?php echo IMAGES.'/left_arrow.png';?>" alt="" /></a>	    		
		    	</div>
		    </div>
	    </div>
	    <!--/portfolio lightbox-->
	    

<?php
//functions
							function formatItems($mItems){
								$rootItems = array();
								$secondLevelItems = array();
								for ($i=0; $i < sizeof($mItems); $i++){
									if($mItems[$i]['parentID']==0){
										array_push($rootItems, $mItems[$i]);
									}else{
										array_push($secondLevelItems, $mItems[$i]);
									}
								}
								return array('rootItems'=>$rootItems, 'secondLevelItems'=>$secondLevelItems);
							}

							function getSubmenu($subDATA, $id){
								$submenuHTML = '';
								$submenuItems = '';
								for ($i=0; $i < sizeof($subDATA); $i++) { 
									if($subDATA[$i]['parentID']==$id){
										$submenuItems .= '<li><a class="secondLevelItem" id="'.$subDATA[$i]['ID'].'" href="'.$subDATA[$i]['menuItemURL'].'">'.$subDATA[$i]['menuItemTitle'].'</a></li>';
									}
								}
								if($submenuItems!=''){
									$submenuHTML.='<ul class="secondLevelMenu">';
									$submenuHTML.=$submenuItems;
									$submenuHTML.='</ul>';
								}
								return $submenuHTML;
							}
?>

<?php get_footer(); ?>