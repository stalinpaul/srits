<?php
/*
Template Name: Recent Projects Template
*/
?>
<?php
$view->checkPreview();
$isPageTitle = $view->isPageTitle(get_the_ID());
$removeTitle = (!$isPageTitle)?"removeElement":"";
?>
<div class="slide" id="<?php echo uniqid('_slide');?>" data-backurl="<?php echo $view->getBackgroundURL(get_the_ID());?>" data-iscover="<?php echo $view->isPageBackgroundCover(get_the_ID());?>" data-stellar-background-ratio="0.2" data-type="recentProjectsPage" style="<?php echo $view->getPageBackground(get_the_ID());?>">
	
	<div class="slideBackgroundOverlay" style="<?php echo $view->getPageOverlayColor(get_the_ID())?>"></div>
		
		<!--slide wrapper-->
		<div class="slideWrapper container">
			
			<div>
				<div class="sixteen columns">
					
					<!--slide header-->
					<div class="slideHeader">
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
						<div class="slideHeaderLineContent">
							<p class="slideTitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageTitleStyle(get_the_ID());?>"><?php echo get_the_title(get_the_ID());?></p>
							<p class="slideSubtitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>">{ <?php echo $view->getPageSubTitle(get_the_ID());?> }</p>
						</div>
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
					</div>
					<!--/slide header-->
				</div>
					
					<div class="clear-fx"></div>
					<!--slide content-->
					<div class="slideContent recentProjectsContainer defaultText" style="<?php echo $view->getPageTextStyle(get_the_ID());?>">
						<?php
							$posts_array = get_posts(array('numberposts'=>4, 'post_type'=>'skmportfolio', 'post_status'=>'publish'));
							$countPosts = 0;
						?>
						
						<?php if(isset($posts_array)&&is_array($posts_array)):?>
							<?php
								$htmlOutput = '';
								$count = 0;
								$recentPHelper = array();
								array_push($recentPHelper, array('sizeClass'=>'rpHeight1', 'imgSafetySRC'=>'http://placehold.it/218x244&text=add+featured+image'));
								array_push($recentPHelper, array('sizeClass'=>'rpHeight2', 'imgSafetySRC'=>'http://placehold.it/218x328&text=add+featured+image'));
								array_push($recentPHelper, array('sizeClass'=>'rpHeight3', 'imgSafetySRC'=>'http://placehold.it/218x287&text=add+featured+image'));
								array_push($recentPHelper, array('sizeClass'=>'rpHeight4', 'imgSafetySRC'=>'http://placehold.it/218x267&text=add+featured+image'));
								foreach ($posts_array as $key => $value) {
									
									$customRecentProjectData = get_post_meta($value->ID, 'skmportfolio-recentProject', false);
									$recentProjectTitle = '';
									if(isset($customRecentProjectData[0])){
										$recentProjectTitle = $customRecentProjectData[0]['title'];
									}
									
									$permalink = get_permalink($value->ID);
																
									$thumbURL = wp_get_attachment_image_src(get_post_thumbnail_id($value->ID), 'skmportfolio-rpHeight'.$count);
									if(!isset($thumbURL[0])){
										$thumbURL = $recentPHelper[$count]['imgSafetySRC'];
									}else{$thumbURL = $thumbURL[0];};									
									$htmlOutput .= '
										<!--recent project item-->
										<div class="four columns recentProjectItem" data-uid="'.$value->ID.'" data-permalink="'.$permalink.'">
											<div class="innerRecentProject">
												<div class="recentProjectContent '.$recentPHelper[$count]['sizeClass'].'">
													<img src="'.$thumbURL.'" />
													<div class="recentProjectTitleArea">
														<p class="recentProjectTitle">'.get_the_title($value->ID).'</p>
														<p class="recentProjectSubtitle">'.$recentProjectTitle.'</p>
													</div>
												</div>
											</div>
										</div>
										<!--/recent project item-->									
									';
									$count++;
								}
								echo $htmlOutput;
							?>																																																	
							<div class="clear-fx"></div>																					
						<?php else:?>
							<p>No portfolio posts were found, please add portfolio first.</p>	
						<?php endif; ?>
											
					</div>
					<!--/slide content-->
				
				
				<div class="sixteen columns">	
					<!--slide footer-->
					<div class="slideFooter">
						<a style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>" href="#" class="slideGoTop alignright">{ <?php _e('go to top', 'default_textdomain');?> }</a>
						<div class="clear-fx"></div>
					</div>
					<!--/slide footer-->
				</div>					
					
				
			</div>
			
		</div>
		<!--/slide wrapper-->
	
	
</div>