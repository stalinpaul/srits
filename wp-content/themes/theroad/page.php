<?php
$view->checkPreview();
$isPageTitle = $view->isPageTitle(get_the_ID());
$removeTitle = (!$isPageTitle)?"removeElement":"";
?>
<div class="slide regularPage" id="<?php echo uniqid('_slide');?>" data-stellar-background-ratio="0.2" data-backurl="<?php echo $view->getBackgroundURL(get_the_ID());?>" data-iscover="<?php echo $view->isPageBackgroundCover(get_the_ID());?>" data-type="regularPage" style="<?php echo $view->getPageBackground(get_the_ID());?>;background-position: center top;">
	
	<div class="slideBackgroundOverlay" style="<?php echo $view->getPageOverlayColor(get_the_ID())?>"></div>
		
		<!--slide wrapper-->
		<div class="slideWrapper container">
			
			<div>
				
				<!--sixteen columns-->
				<div class="sixteen columns">					
					<!--slide header-->
					<div class="slideHeader">
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
						<div class="slideHeaderLineContent">
							<p class="slideTitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageTitleStyle(get_the_ID());?>"><?php echo get_the_title(get_the_ID());?></p>
							<p class="slideSubtitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>">{ <?php echo $view->getPageSubTitle(get_the_ID());?> }</p>
						</div>
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
					</div>
					<!--/slide header-->
				</div>
				<!--end sixteen columns-->
					
					<div class="clear-fx"></div>
					
					<!--slide content-->
					<div class="slideContent defaultText editorContent" id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php echo $view->getPageTextStyle(get_the_ID());?>">																		
						<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>           	
						          	<?php
						                  the_content();						
						            ?> 
						<?php endwhile; else: ?>
						<p><?php _e('No posts were found. Sorry!', 'default_textdomain'); ?></p>
						<?php endif; ?>     
						<?php wp_reset_query(); ?>						
					</div>
					<!--/slide content-->
					
				<!--sixteen columns-->
				<div class="sixteen columns">					
					<!--slide footer-->
					<div class="slideFooter">
						<a style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>" href="#" class="slideGoTop alignright">{ <?php _e('go to top', 'default_textdomain');?> }</a>
						<div class="clear-fx"></div>
					</div>
					<!--/slide footer-->
				</div>
					
				
			</div>
			
		</div>
		<!--/slide wrapper-->
	
	
</div>