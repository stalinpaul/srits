<?php
$view->checkPreview();
?>
							<!--blog single main content-->
							<div class="blogSingleMainContent" data-buttonStyle="<?php echo $view->getBlogButtonStyle();?>">
							
				                <?php
									$the_day = get_the_date( 'd', $post );
									$the_month = get_the_date( 'M', $post );
									$the_year = get_the_date( 'y', $post );
													                
				                	$previousURL = get_permalink(get_adjacent_post(false,'',false));
									$nextURL = get_permalink(get_adjacent_post(false,'',true));									
									$enlargePoz = -1;
									
									$themeOptions = get_option('themeOptions');
									$isBlogComments = (isset($themeOptions['showBlogComments']))?$themeOptions['showBlogComments']:'';									 
				                ?>							
								<div class="singleBlogTop">
									<a href="#" class="genericBoxButton blogButtonColor floatLeft closeBlogSingleBTN"><img src="<?php echo IMAGES.'/close.png';?>" /></a>
				                	<?php if($nextURL!="" && isset($nextURL) && $nextURL!=get_permalink(get_the_ID())):?>
				                		<a href="<?php $enlargePoz+=-27; echo $nextURL;?>" class="genericBoxButton blogButtonColor floatRight nextSingleBTN" style="margin-left: 1px;"><img src="<?php echo IMAGES.'/right.png';?>" /></a>
				                	<?php endif;?>
				                	<?php if($previousURL!="" && isset($previousURL) && $previousURL!=get_permalink(get_the_ID())):?>
				                		<a href="<?php $enlargePoz+=-27; echo $previousURL;?>" class="genericBoxButton blogButtonColor floatRight prevSingleBTN"><img src="<?php echo IMAGES.'/left_arrow.png';?>" /></a>
				                	<?php endif;?>
				                	<?php if($isBlogComments=="ON"):?>
				                	<a href="#" class="genericBoxButton blogButtonColor floatRight commentsBTN" style="position: relative; top: 28px; margin-right: <?php echo $enlargePoz;?>px;"><img src="<?php echo IMAGES.'/comments_icon.png';?>" /></a>
				                	<?php endif;?>								
								</div>
								<div class="clear-fx"></div>
								
								
								
				                <div style="padding: 0px 28px;">
				                	<!--single content-->
				                	<div class="singleContent">
				                		
				                		<div class="singleContentFirst">
											<p class="blogPDay blogMainColor"><?php echo $the_day;?></p>
											<p class="blogPMonth"><?php echo $the_month;?> '<?php echo $the_year;?></p>
											<ul class="blogCommentsBox">
												<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
												<li><a><?php echo get_comments_number(get_the_ID());?></a></li>
												<li><a><img style="height: 19px; margin-left: 3px;" src="<?php echo IMAGES.'/comments_icon01.png'?>" /></a></li>										
											</ul>
											<p class="articleAuthor"><?php _e('by', 'default_textdomain');?><br /> <?php echo get_the_author();?></p>
											<div class="clear-fx"></div>
				                		</div>
				                		
				                		<div class="singleContentSecond">			                			
				                			<!--blog content-->			                			
				                			<div style="padding: 30px 25px;">
				                				<p class="blogPostTitle blogMainColor"><?php echo get_the_title(get_the_ID());?></p>
				                				<div class="defaultText editorContent" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>				                					  
				                					<?php the_content();?>
													<?php endwhile; else: ?>
														<p><?php _e('No posts were found. Sorry!', 'default_textdomain'); ?></p>
													<?php endif; ?> 				                					 
				                				</div>
				                			</div>
				                			<!--/blog content-->
				                		</div>
				                		
				                		<div class="clear-fx"></div>
				                		
				                		<?php if($isBlogComments=="ON"):?>
				                		<!--comments-->
					                	<div class="commentsContent">
					                		<p class="commentsTitle blogMainColor"><?php _e('comments', 'default_textdomain');?></p>
					                		<div id="commentsUI" data-notice="<?php _e('***Your comment is awaiting moderation***', 'default_textdomain');?>" data-error="<?php _e('You might have left one of the fields blank, or be posting too quickly', 'default_textdomain');?>">
					                			<?php comments_template(); ?>
					                		</div>
					                	</div>
				                		<!--/comments-->
				                		<?php endif;?>
				                		
				                	</div>
				                	<!--/ single content-->                                             			               			                	               
				                </div>
			                								
							</div>
							<!--blog single main content-->