<?php
/*
Template Name: Portfolio Template
*/
?>
<?php
$view->checkPreview();
$isPageTitle = $view->isPageTitle(get_the_ID());
$removeTitle = (!$isPageTitle)?"removeElement":"";
require_once(CLASS_PATH.'/com/sakurapixel/php/utils/excerpt/Excerpt.php');
?>
<div class="slide" id="<?php echo uniqid('_slide');?>" data-stellar-background-ratio="0.2" data-backurl="<?php echo $view->getBackgroundURL(get_the_ID());?>" data-iscover="<?php echo $view->isPageBackgroundCover(get_the_ID());?>" data-type="portfolioPage" style="<?php echo $view->getPageBackground(get_the_ID());?>;background-position: center top;">
	
	<div class="slideBackgroundOverlay" style="<?php echo $view->getPageOverlayColor(get_the_ID())?>"></div>
		
		<!--slide wrapper-->
		<div class="slideWrapper container">
			
			<div>
				
				<!--sixteen columns-->
				<div class="sixteen columns">
					
					<!--slide header-->
					<div class="slideHeader">
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
						<div class="slideHeaderLineContent">
							<p class="slideTitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageTitleStyle(get_the_ID());?>"><?php echo get_the_title(get_the_ID());?></p>
							<p class="slideSubtitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>">{ <?php echo $view->getPageSubTitle(get_the_ID());?> }</p>
						</div>
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
					</div>
					<!--/slide header-->
					
					
					<!--slide content-->
					<div class="slideContent" style="margin-top: 0px;">
						<!--isotope menu-->
						<ul class="isotopeMenu">
							<li>{</li>
							<li><a class="eye" href="*"><?php _e('All', 'default_textdomain');?></a></li>						
		                    <?php 
								$categories = $view->getPortfolioCategories();						
								if(isset($categories)){
									for ($i=0; $i < sizeof($categories); $i++) {
										if($i==0){
											echo '<li>|</li>';
										} 
										echo '<li><a class="eye" href="'.$categories[$i]['term_id'].'">'.$categories[$i]['name'].'</a></li>';
										if($i<sizeof($categories)-1){
											echo '<li>|</li>';
										}
									}
								}                 
		                    ?>							
							<li>}</li>
						</ul>
						<!--/isotope menu-->
						<?php echo do_shortcode('[igallery id="95"]'); ?>
						<!--isotope gallery-->
							<div class="isotopeContainer">
								<?php $pageTextColor =  $view->getPageTextStyle(get_the_ID());?>
								<?php $wpbp = new WP_Query(array('post_type' => 'skmportfolio', 'posts_per_page' =>'-1' )); ?>
								<?php if ($wpbp->have_posts()) :  while ($wpbp->have_posts()) : $wpbp->the_post(); ?>
									
								<?php
									$postTitle = get_the_title($post->ID);
									$permalink = get_permalink($post->ID);
									
									//build categories
									$postCategories = get_the_terms($post->ID, 'portfolio_categories');
									$buildCategories = '';										
									if(isset($postCategories)&&is_array($postCategories)){						
										foreach ($postCategories as $key => $value) {
											$buildCategories .= $postCategories[$key]->term_id.' ';
										}
									}
									//end build categories
									
									//item display type
									$itemDisplayType = 'imageItem';
									$displayType = get_post_meta($post->ID, 'skmportfolio-chooseType', false);
									if(isset($displayType[0]) && isset($displayType[0]['itemPreview'])){						
										$itemDisplayType = $displayType[0]['itemPreview'];
									}
									//end item display type
									
									//item size
									$itemSize = "size1";
									$itemSizeTemp = get_post_meta($post->ID, 'skmportfolio-sizeType', false);
									if(isset($itemSizeTemp[0]) && isset($itemSizeTemp[0]['itemPreviewSize'])){						
										$itemSize = $itemSizeTemp[0]['itemPreviewSize'];
									}
									$thumbURL = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'skmportfolio-'.$itemSize);
									if(!isset($thumbURL[0])){
										$thumbURL = IMAGES.'/default/'.$itemSize.'.jpg';
									}else{
										$thumbURL = $thumbURL[0];
									}				
									//end item size
									
									//excerpt
									$portfolioExcerpt = 100;
									$customExcerpt = get_post_meta($post->ID, 'skmportfolio-excerpt', false);
									if(isset($customExcerpt[0])){						
										$portfolioExcerpt = $customExcerpt[0]['excerpt'];
									}
									//$portfolioExcerpt = 500;																
									//end excerpt
									
									//$iso_id = uniqid("iso_item");
									$iso_id = $post->ID;									
								?>
								
								<!--add item-->
								<div class="<?php echo $buildCategories; echo $itemSize;?> isotopeItem" data-uid="<?php echo $iso_id;?>" data-permalink="<?php echo $permalink;?>">
									
									
									<?php
									if ($itemDisplayType=="imageItem"){
										echo '
											<!--imageItem-->
											<div class="imageItem">
												<img class="portfolioPreviewThumb" src="'.$thumbURL.'" />
												<div class="imageItemOverlay">
													<div class="imageItemOverlayTransparent"></div>
													<p class="imageItemTitle">'.$postTitle.'</p>
												</div>
											</div>										
										';
									}
									if ($itemDisplayType=="textItem"){
										$temp = Excerpt::excerpt_get($portfolioExcerpt);																				
										echo '
											<div class="textItem">
												<a href="#" class="portfolioPreviewTitle portfolioMainColor" data-uid="'.$iso_id.'">'.$postTitle.'</a>
												<div class="portfolioExcerpt defaultText" style="'.$pageTextColor.'">
													'.get_the_excerpt().'
												</div>
											</div>										
										';
									}
									?>
									
								</div>																
								<!--//add item-->	
									
								<?php endwhile; else: ?>
								<p><?php echo('<br /><p class="defaultText">'.__('No portfolio posts were found. Please add posts from admin!', 'default_textdomain').'</p>'); ?></p>			
								<?php endif; ?>
								<?php wp_reset_query(); ?>
								<div class="clear-fx"></div>																	
							</div>							
						<!--/isotope gallery-->
						
					</div>
					<!--/slide content-->
					
					<div class="clear-fx"></div>	
					
				</div>
				<!--end sixteen columns-->					
				
				<div class="clear-fx"></div>
					
					<!--single portfolio item-->
					<div class="openedPortfolio">
						<div class="portfolioOpenItemContent"></div>
						<div class="portfolioPreloaderContainer">
							<img id="singleProjectPreloadIMG" src="<?php echo $view->getThemePreloadLogoURL();?>" alt="" />
							<p id="singleProjectPreloadTXT" class="mainPreloaderText"><?php _e('LOADING', 'default_textdomain')?></p>
						</div>	
					</div>
					<!--/single portfolio item-->
				
					
				<!--sixteen columns-->
				<div class="sixteen columns">	
					<!--slide footer-->
					<div class="slideFooter">
						<a style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>" href="#" class="slideGoTop alignright">{ <?php _e('go to top', 'default_textdomain');?> }</a>
						<div class="clear-fx"></div>
					</div>
					<!--/slide footer-->
				</div>	
				
				<div class="clear-fx"></div>	
				
			</div>
			
		</div>
		<!--/slide wrapper-->
	
	
</div>