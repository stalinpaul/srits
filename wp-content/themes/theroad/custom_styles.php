	<style type="text/css">
		<?php 
			require_once(CLASS_PATH.'/com/sakurapixel/php/stylehelper.php');
			$stH = new StyleHelper();
			$menuColors = $stH->getMenuColors();
			$footerColors = $stH->getFooterColors();
			$portfDefCol = $stH->getPortfolioDefaultColor();
			$blogDefCol = $stH->getBlogDefaultColor();
			$contactDefCol = $stH->getContactDefaultColor();		
		?>
			.selectedMenuItem{
				background-color: #<?php echo $menuColors['selectedMenuColor'];?>;
			}
			.footerLinksList li, .footerLinksList li a{
				color: #<?php echo $footerColors['footerLinksColor'];?>;
			}
			.footerWidgetTitleColor{
				color: #<?php echo $footerColors['widgetsTitleColor'];?>;
			}
			.portfolioMainColor{
				color: #<?php echo $portfDefCol;?>;
			}
			a.portfolioMainColor{
				color: #<?php echo $portfDefCol;?>;
			}			
			.portfolioButtonColor{
				background-color: #<?php echo $portfDefCol;?>;
			}
			
			.blogMainColor{
				color: #<?php echo $blogDefCol;?>;
			}
			a.blogMainColor{
				color: #<?php echo $blogDefCol;?>;
			}			
			a.blogButtonColor{
				color: #<?php echo $blogDefCol;?>;
			}			
			.blogButtonColor{
				background-color: #<?php echo $blogDefCol;?>;
			}
			.contactHeadersColor{
				color: #<?php echo $contactDefCol;?>;
			}									
			a.selectedSliderColor{
				background-color: #<?php echo $portfDefCol;?>;
			}
			.isotopeMenuSelected{
				background-color: #<?php echo $portfDefCol;?>;
			}													
	</style>