<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<title><?php bloginfo('name'); ?>  <?php wp_title(); ?></title>    
      
    
    <!-- Mobile Specific Metas
  ================================================== -->    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />    
  
    <!-- CSS Custom
  ================================================== -->        
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />    
    
    <!-- Fonts
  ================================================== -->
  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300,300italic' rel='stylesheet' type='text/css' />
  	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css' />
  	<link href='http://fonts.googleapis.com/css?family=Signika:700' rel='stylesheet' type='text/css' />     
  	
  	<?php get_template_part('custom_styles');?>
<?php
$themeOptions = get_option('themeOptions');
$googleAnalyticsCode = (isset($themeOptions['googleAnalytics']))?$themeOptions['googleAnalytics']:'';
echo $googleAnalyticsCode;
?>
<?php wp_head(); ?>
</head>
<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/utils/rx_mobile_detect.php');
	//is mobile browser
	function isMobile(){				
		$detect_mobile = new Mobile_Detect();
		$is_mobile = 'false';
		if($detect_mobile->isMobile()){
			$is_mobile = 'true';
		}
		return $is_mobile;		
	}
?>
<body data-isMobile="<?php echo isMobile();?>" <?php body_class(); ?>>