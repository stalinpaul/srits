<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/hive.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/customposts/utils/CPTHelper.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/customposts/PortfolioPostType.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/customposts/VideoPostType.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/customposts/PortfolioMImagesPostType.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/optionpages/GenericBackgroundOptionPage.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/SkShortcodes.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/optionpages/ThemeOptionPage.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/optionpages/DocumentionOptionPage.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/sidebars/SidebarManager.php');

class HiveWrapper extends Hive{
	
	//override
	public function start(){
		//activate widgets
		require_once(CLASS_PATH.'/widgets/widgets.php');		
		//footer sidebar
		SidebarManager::getInstance()->registerSidebar(array(
			'name' => __( 'Footer sidebar', 'default_textdomain' ),
			'id' => 'sk-footer-sidebar',
			'description' => 'Footer sidebar, shows at the bottom of the page.',
			'before_widget' => '<!--footer widget--><div class="footerWidget footerWidgetContent" id="%1$s" class="widget %2$s">',
			'after_widget' => '</div><!--/footer widget-->',
			'before_title' => '
							<p class="footerWidgetTitle footerWidgetTitleColor">',
			'after_title' => '</p>'			
		), 'footer');		
		
		//home social sidebar
		SidebarManager::getInstance()->registerSidebar(array(
			'name' => __( 'Homepage social sidebar', 'default_textdomain'),
			'id' => 'sk-homesocial-sidebar',
			'description' => 'Homepage social links sidebar, !!!-Note-!!! only add The Road: Social widget to it.',
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget' => '</li>',
			'before_title' => '',
			'after_title' => ''			
		), 'homesocial');
		
		//footer links sidebar
		SidebarManager::getInstance()->registerSidebar(array(
			'name' => __( 'Footer links sidebar', 'default_textdomain'),
			'id' => 'sk-footerlinks-sidebar',
			'description' => 'Footer links sidebar, !!!-Note-!!! only add The Road: Footer link widget to it.',
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget' => '</li><li>|</li>',
			'before_title' => '',
			'after_title' => ''			
		), 'footerlinks');		
					
		//add menu support
		parent::addSingleMenuSupport();
		//fire up framework
		parent::start();
	}
	
	//init handler - override Hive handler
	public function initializeHandler(){		
		parent::initializeHandler();
		parent::addAutomaticFeedlinksSupport();
		parent::checkContentWidth(960);
		
		//add portfolio
		$this->addPortfolioMCPT();
		
		$this->addTaxonomy();
		
		//add support for page featured image (will be used for background)
		parent::addThumbSize(150, 148, array('post', $this->portfolioMCPT->getPostSlug()));		
		parent::addImageSize(262, 170, 'blog-featured', true);
		/*
		parent::addThumbSize(150, 148, array('page', 'post', $this->portfolioMCPT->getPostSlug(), $this->videoPortfolioCPT->getPostSlug()));		
		parent::addImageSize(53, 53, 'recent-post-preview', true);
		 */
	}
	
	//add taxonomy for portfolio
	private function addTaxonomy(){
		//portfolio taxonomy
		if(isset($this->portfolioMCPT)){
			register_taxonomy('portfolio_categories', $this->portfolioMCPT->getPostSlug(), array('label'=>'Portfolio Categories', 'hierarchical'=>true));
		}				
	}
	
	
	/*
	 * PORTFOLIO CPT
	 */
	private $portfolioMCPT;
	
	private function addPortfolioMCPT(){
		$settings = array('post_type' => 'skmportfolio', 'name' => 'Portfolio', 'menu_icon' => IMAGES_ADMIN.'/icons/images-flickr.png',
		'singular_name' => 'Portfolio', 'rewrite' => 'portfolio', 'add_new' => 'Add new',
		'edit_item' => 'Edit', 'new_item' => 'New', 'view_item' => 'View item', 'search_items' => 'Search items',
		'not_found' => 'No item found', 'not_found_in_trash' => 'Item not found in trash');
		
		$cptHelper = new CPTHelper($settings);
		$this->portfolioMCPT = new PortfolioMImagesPostType();
		$this->portfolioMCPT->create($cptHelper, $settings);
		
		//add image size
		parent::addImageSize(300, 160, $settings['post_type'].'-size1', true);
		parent::addImageSize(300, 277, $settings['post_type'].'-size2', true);
		parent::addImageSize(300, 126, $settings['post_type'].'-size3', true);
		parent::addImageSize(300, 600, $settings['post_type'].'-size4', true);
		
		//recent project sizes
		parent::addImageSize(218, 244, $settings['post_type'].'-rpHeight0', true);
		parent::addImageSize(218, 328, $settings['post_type'].'-rpHeight1', true);
		parent::addImageSize(218, 287, $settings['post_type'].'-rpHeight2', true);
		parent::addImageSize(218, 267, $settings['post_type'].'-rpHeight3', true);
		
		//recent work size
		parent::addImageSize(220, 329, $settings['post_type'].'-recentWork', true);
		
		//inside portfolio
		parent::addImageSize(150, 59, $settings['post_type'].'-thumb', true);
		parent::addImageSize(884, 400, $settings['post_type'].'-large', true);			
	}	

	
	//admin menu handler - override Hive handler
	public function adminMenuHandler(){
		//create pages
		$themeOptionPage = new ThemeOptionPage();
		$themeOptionPage->createMenuPage('Theme Options', 'Theme Options', 'administrator', 'theme-options_opts', IMAGES_ADMIN.'/icons/gear_arrow.png', NULL, 'themeOptions');
		
		/*
		$documentationOptionPage = new DocumentionOptionPage();
		$documentationOptionPage->createMenuPage('Spotless Documentation', 'Spotless Documentation', 'administrator', 'theme-docs_opts', IMAGES_ADMIN.'/icons/notebook-pencil.png', NULL, 'themeDocs');
		 * */				
	}
		
	//admin init handler - override Hive handler
	public function adminInitHandler(){
		//add meta boxes pages
		$this->addPagesMetaBox('sk_page_box_id_page_options', 'Page default options', array($this, 'page_title_mb_content'));
		
		$this->addPagesMetaBox('sk_page_box_id_xhide_title', 'Hide page title', array($this, 'hide_page_title_content'));
		
		$this->addPagesMetaBox('sk_page_box_id_page_no', 'Page background options', array($this, 'page_no_mb_content'));		
				
		//add meta box for PortfolioMImagesPostTypes
		$this->portfolioMCPT->addMetaBox("Add images for this project");
		$this->portfolioMCPT->addMetaBoxVideo("Video item");
		$this->portfolioMCPT->addMetaBoxChooseType("Item extra options");	
		
	}
	
	
	public function hide_page_title_content($post){
		$custom = get_post_meta($post->ID, 'page_options', false);
		
		$hideTitleChecked = '';
		if(isset($custom[0])){
			$hideTitle = (isset($custom[0]['hideTitle']))?$custom[0]['hideTitle']:"";
			if($hideTitle=='ON'){
				$hideTitleChecked = 'checked';
			}
		}
				
		?>
			<!--box-->
			<div class="metaBoxContentBox">
					<label class="customLabel">Hide page title and subtitle (default OFF)</label>
					<div class="hLine"></div>					
					<div class="space01"></div>
					<input type="checkbox" name="page_options[hideTitle]" value="ON" <?php echo $hideTitleChecked;?> />
					<label class="chBLabel">Hide / Show</label>
					<div class="space01"></div>					
			</div>
			<!--/box-->
		<?php
	}	
	
	//page title - MB - option
	public function page_title_mb_content($post){
		$custom = get_post_meta($post->ID, 'page_options', false);
		$pageSubtitle = "page subtitle...";
		
		$pageTitleColor = '6e6a75';
		$pageSubTitleColor = '9b9b9b';
		$pageTitleShadowColor = 'ffffff';
		$pageHeaderLinesColor = 'cccccc';
		$pagetextColor = "5e5a62";
		if(isset($custom[0])){			
			$pageSubtitle = (isset($custom[0]['pageSubtitle']))?$custom[0]['pageSubtitle']:$pageSubtitle;
			$pageSubtitle = wptexturize($pageSubtitle);
			
			$pageTitleColor = (isset($custom[0]['pageTitleColor']))?$custom[0]['pageTitleColor']:"6e6a75";
			$pageSubTitleColor = (isset($custom[0]['pageSubTitleColor']))?$custom[0]['pageSubTitleColor']:"9b9b9b";
			$pageTitleShadowColor = (isset($custom[0]['pageTitleShadowColor']))?$custom[0]['pageTitleShadowColor']:"ffffff";
			
			$pageHeaderLinesColor = (isset($custom[0]['pageHeaderLinesColor']))?$custom[0]['pageHeaderLinesColor']:"cccccc";
			$pagetextColor = (isset($custom[0]['pagetextColor']))?$custom[0]['pagetextColor']:"5e5a62";		
		}				
		?>
			<!--box-->	
			<div class="metaBoxContentBox">						
					<label class="customLabel">Page Subtitle</label>
					<div class="hLine"></div>								
					<input id="pageSubtitle" class="inputText" type="text" name="page_options[pageSubtitle]" value="<?php echo $pageSubtitle;?>" /><br />				
					
					<div class="space01"></div>					
												
					<label class="customLabel">Page Title Color</label>
					<div class="hLine"></div>								
					<input id="pageTitleColor" class="inputText" type="text" name="page_options[pageTitleColor]" value="<?php echo $pageTitleColor;?>" /><br />
					
					<div class="space01"></div>
					
					<label class="customLabel">Page Title Shadow Color</label>
					<div class="hLine"></div>								
					<input id="pageTitleShadowColor" class="inputText" type="text" name="page_options[pageTitleShadowColor]" value="<?php echo $pageTitleShadowColor;?>" /><br />
					
					<div class="space01"></div>					
					
					<label class="customLabel">Page Subtitle Color</label>
					<div class="hLine"></div>								
					<input id="pageSubTitleColor" class="inputText" type="text" name="page_options[pageSubTitleColor]" value="<?php echo $pageSubTitleColor;?>" /><br />					
					
					<div class="space01"></div>
					
					<label class="customLabel">Page Header Lines Color</label>
					<div class="hLine"></div>								
					<input id="pageHeaderLinesColor" class="inputText" type="text" name="page_options[pageHeaderLinesColor]" value="<?php echo $pageHeaderLinesColor;?>" /><br />					
					
					<div class="space01"></div>
									
					<label class="customLabel">Page Default Text Color</label>
					<div class="hLine"></div>								
					<input id="pagetextColor" class="inputText" type="text" name="page_options[pagetextColor]" value="<?php echo $pagetextColor;?>" /><br />					
					
					<a id="resetDefaultOptionsBTN" class="button-secondary">Reset to default</a>																																																		
			</div>
			<!--/box-->			
		<?php
	}



	//page background - MB - option
	public function page_no_mb_content($post){
		$custom = get_post_meta($post->ID, 'page_options', false);
		$pageBackgroundColor = 'e9e9e9';
		$pageOverlayColor = '';
		if(isset($custom[0])){
			$pageBackgroundColor = (isset($custom[0]['pageBackgroundColor']))?$custom[0]['pageBackgroundColor']:"e9e9e9";
			$pageOverlayColor = (isset($custom[0]['pageOverlayColor']))?$custom[0]['pageOverlayColor']:"";			
		}
		$pageBackgroundIMG = "";
		$pageBackgroundAttachementID = (isset($custom[0]['pageBackgroundAttachementID'])?$custom[0]['pageBackgroundAttachementID']:'');
		if(isset($pageBackgroundAttachementID)){
			if($pageBackgroundAttachementID!=""){
				//get image
				$pageBackgroundIMG = wp_get_attachment_image_src($pageBackgroundAttachementID, 'full');
				$pageBackgroundIMG = $pageBackgroundIMG[0];
			}
		}else{
			$pageBackgroundAttachementID = "";
		}
		
		//repeatBackground
		$repeatBackgroundChecked = '';
		if(isset($custom[0])){
			$repeatBackground = (isset($custom[0]['repeatBackground']))?$custom[0]['repeatBackground']:"";
			if($repeatBackground=='ON'){
				$repeatBackgroundChecked = 'checked';
			}
		}		
			
		?>
			<!--box-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Page Background Color</label>
					<div class="hLine"></div>								
					<input id="pageBackgroundColor" class="inputText" type="text" name="page_options[pageBackgroundColor]" value="<?php echo $pageBackgroundColor;?>" /><br />
					
					<div class="space01"></div>
					
					<label class="customLabel">Page Background Image (optional)</label>
					<div class="hLine"></div>					
					<div>
						<img class="imgUI" src="<?php echo $pageBackgroundIMG; ?>" />
					</div>
					<input id="pageBackgroundAttachementID" class="imageAttachementID" class="inputText fullWidth" type="hidden" name="page_options[pageBackgroundAttachementID]" value="<?php echo $pageBackgroundAttachementID;?>" />
					<div class="space01"></div>
					<a class="button-secondary uploadBTN" href="#">Upload background image</a>
					<div class="space01"></div>
					<div class="space01"></div>
										
					<input type="checkbox" name="page_options[repeatBackground]" value="ON" <?php echo $repeatBackgroundChecked;?> />
					<label class="chBLabel">Background image repeat (default Off)</label>
					<div class="space01"></div>
					<div class="space01"></div>					
					
					<label class="customLabel">Page Background Overlay Color - optional - (leave empty if you do not want overlay color over the background)</label>
					<div class="hLine"></div>					
					<input id="pageOverlayColor" class="inputText" type="text" name="page_options[pageOverlayColor]" value="<?php echo $pageOverlayColor;?>" /><br />
					<a id="resetBackgroundBTN" class="button-secondary">Reset to default</a>																																																		
			</div>
			<!--/box-->				
		<?php
	} 
	
	/**
	 * SAVE POST HANDLER - override Hive handler
	 */
	 public function savePostHandler(){
		global $post;		
				
		if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
			return $post_id;
		}		
		//page save
		if(isset($post) && isset($_POST['post_type'])){
			if('page' == $_POST['post_type']){
				if(current_user_can( 'edit_page', $post->ID )){
					update_post_meta($post->ID, 'page_options', $_POST['page_options']);
				}
			}
		}
		
		//portfolio save - choose type
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-chooseType'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-chooseType', $_POST[$this->portfolioMCPT->getPostSlug().'-chooseType']);
		}
		
		//portfolio save - size type
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-sizeType'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-sizeType', $_POST[$this->portfolioMCPT->getPostSlug().'-sizeType']);
		}
		
		//portfolio save - buy
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-buy'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-buy', $_POST[$this->portfolioMCPT->getPostSlug().'-buy']);
		}
		
		//portfolio slider height
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-slider'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-slider', $_POST[$this->portfolioMCPT->getPostSlug().'-slider']);
		}		
		
		//portfolio save - preview
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-preview'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-preview', $_POST[$this->portfolioMCPT->getPostSlug().'-preview']);
		}
		
		//portfolio save - excerpt
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-excerpt'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-excerpt', $_POST[$this->portfolioMCPT->getPostSlug().'-excerpt']);
		}
		
		//portfolio save - recent project subtitle
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-recentProject'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-recentProject', $_POST[$this->portfolioMCPT->getPostSlug().'-recentProject']);
		}
		
		//portfolio save - video
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug().'-videoItem'])){			
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug().'-videoItem', $_POST[$this->portfolioMCPT->getPostSlug().'-videoItem']);
		}														
		
		//portfolio save
		if(isset($post) && isset($_POST[$this->portfolioMCPT->getPostSlug()])){
			update_post_meta($post->ID, $this->portfolioMCPT->getPostSlug(), $_POST[$this->portfolioMCPT->getPostSlug()]);			
		}		
												
	 }
	 
	/*
	 * register shortcodes 
	 */ 
	public function registerShortcodes(){		
		$shorcodesHelper = new SkShortcodes();
		$shorcodesHelper->registerShortcodes();	
	}
	
	//read more link - override
	public function new_excerpt_more($more) {
	       global $post;
		   $val = ' <a class="readMore" href="'. get_permalink($post->ID) . '">'.__('Read more...', 'default_textdomain').'</a>';
		   $p_type = get_post_type($post);
		   if($p_type=="post"){
		   		$val = ' ...';
		   }	   
		return $val;
	}
	
	//ajax comment handler - override
	public function comments_ajax_handler($comment_ID, $comment_status){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			switch($comment_status){
				case "0":
				wp_notify_moderator($comment_ID);
				echo "notify_pending";
				case "1": //Approved comment
				echo "success";
				$commentdata =& get_comment($comment_ID, ARRAY_A);
				$post =& get_post($commentdata['comment_post_ID']);
				wp_notify_postauthor($comment_ID, $commentdata['comment_type']);
				break;
				default:
				echo "error";
			}
			/*
			try {
			    wp_notify_moderator($comment_ID);
			} catch (Exception $e) {}			
			*/
		exit;
		}		
	}
	
	/*
	 * ADMIN SCRIPTS/ STYLES - override Hive handler
	 */	
	public function adminEnqueueScriptsHandler(){		
		//add default scripts
		parent::adminEnqueueScriptsHandler();
		$this->enqueueThickbox();
				
		
		$screenID = get_current_screen()->id;
		$last = substr($screenID, strlen($screenID)-5, strlen($screenID));
		
		if($screenID=="page"){
			 //color picker style
			 $this->enqueColorPicker();
			 //options script	
			 wp_register_script( 'page_options_script', JS_ADMIN.'/page_options/page_options.js', array('jquery', 'color_picker'));			 
			 wp_enqueue_script('page_options_script');			 		   
		}

		if($last=='_opts'){
			//theme options screen
						
			 //color picker style
		     wp_register_style( 'cpicker_style', CSS_ADMIN.'/components/cpicker/css/colorpicker.css');
			 wp_register_style( 'cpicker_layout', CSS_ADMIN.'/components/cpicker/css/layout.css');		 
		     wp_enqueue_style( 'cpicker_style');
			 
			 //color picker script
			 wp_register_script( 'color_picker', JS_ADMIN.'/cpicker/colorpicker.js', array('jquery'));
			 wp_register_script( 'color_picker_eye', JS_ADMIN.'/cpicker/eye.js', array('jquery'));
			 wp_register_script( 'color_picker_layout', JS_ADMIN.'/cpicker/layout.js', array('jquery'));
			 wp_register_script( 'color_picker_utils', JS_ADMIN.'/cpicker/utils.js', array('jquery'));
			 wp_enqueue_script('color_picker');
			 wp_enqueue_script('color_picker_eye');	
			 wp_enqueue_script('color_picker_layout');	
			 wp_enqueue_script('color_picker_utils');				 
			 
			 //options script	
			 wp_register_script( 'theme_options', JS_ADMIN.'/theme_options/theme_options.js', array('jquery', 'color_picker'));			 
			 wp_enqueue_script('theme_options');				 			 			
		}

		//if add portfolio item screen
		$portfolioScreen = $this->portfolioMCPT->getSettings();
		if(get_current_screen()->id == $portfolioScreen['post_type']){
			wp_register_script( 'portfolio_m_images', JS_ADMIN.'/portfolio/portfolio_m_images.js', array('jquery'));
			wp_enqueue_script( 'portfolio_m_images' );
			$js_data = array('POST_TYPE'=>$portfolioScreen['post_type']);
			wp_localize_script( 'portfolio_m_images', 'portfolioPHPObj', $js_data );
		}		

	}
	
	private function enqueColorPicker(){
			 //color picker style
		     wp_register_style( 'cpicker_style', CSS_ADMIN.'/components/cpicker/css/colorpicker.css');
			 wp_register_style( 'cpicker_layout', CSS_ADMIN.'/components/cpicker/css/layout.css');		 
		     wp_enqueue_style( 'cpicker_style');
			 
			 //color picker script
			 wp_register_script( 'color_picker', JS_ADMIN.'/cpicker/colorpicker.js', array('jquery'));
			 wp_register_script( 'color_picker_eye', JS_ADMIN.'/cpicker/eye.js', array('jquery'));
			 wp_register_script( 'color_picker_layout', JS_ADMIN.'/cpicker/layout.js', array('jquery'));
			 wp_register_script( 'color_picker_utils', JS_ADMIN.'/cpicker/utils.js', array('jquery'));
			 wp_enqueue_script('color_picker');
			 wp_enqueue_script('color_picker_eye');	
			 wp_enqueue_script('color_picker_layout');	
			 wp_enqueue_script('color_picker_utils');			 		
	}
	//load thinkbox (for admin)
	private function enqueueThickbox()
	{
		wp_enqueue_script('thickbox');
		wp_enqueue_script('media-upload');
		wp_enqueue_style('thickbox');		
	}
	
	//WP Enqueue scripts handler - Frontend
	public function WPEnqueueScriptsHandler(){
		parent::WPEnqueueScriptsHandler();
		global $is_IE;
		if ( $is_IE ) {
		    wp_register_script( 'html5IE', 'http://html5shim.googlecode.com/svn/trunk/html5.js');
		    wp_register_script( 'pngfix', JS.'/external/png_fix/supersleight_plugin.js', array('jquery'));
			wp_enqueue_script('html5IE');
			wp_enqueue_script('pngfix');			
		}else{}
		
		//libs scripts    	
		wp_register_script( 'easing1_3', JS.'/external/jquery/jquery.easing.1.3.js', array('jquery'));
		wp_register_script( 'jquery_transition', JS.'/external/jquery/jquery.transition.js', array('jquery'));
		wp_register_script( 'EventBus', JS.'/com/sakurapixel/events/eventbus/EventBus.js', array('jquery'));
		wp_register_script( 'genericevents', JS.'/com/sakurapixel/events/genericevents.js', array('jquery'));
		wp_register_script( 'JQueryAjax', JS.'/com/sakurapixel/jqueryajax/JQueryAjax.js', array('jquery'));
				
		wp_register_script('tweenmax', JS.'/external/TweenMax.min.js', array('jquery'));
		wp_register_script('scrollToPlugin', JS.'/external/ScrollToPlugin.min.js', array('jquery'));		
		wp_register_script('stelarjs', JS.'/external/jquery.stellar.min.js', array('jquery'));
		wp_register_script('waypoints', JS.'/external/waypoints.min.js', array('jquery'));
		wp_register_script('detectMobileBrowser', JS.'/external/detectmobilebrowser.js', array('jquery'));
		
		wp_register_script( 'jflickr', JS.'/external/jquery/jquery.flickr.js', array('jquery'));
		wp_register_script( 'isotope', JS.'/external/jquery/jquery.isotope.min.js', array('jquery'));
		wp_register_script( 'sliderX', JS.'/com/sakurapixel/sliders/SliderX.js', array('jquery'));
		
		
		wp_register_script( 'backstretch', JS.'/external/backstretch.js', array('jquery'));
		wp_enqueue_script('backstretch');
		
		
		wp_register_script('main_preloader', JS.'/website/main_preloader/main_preloader.js', array('jquery'));
		wp_register_script('logo_controler', JS.'/website/logo_controler/LogoControler.js', array('jquery'));
		wp_register_script('menu_handler', JS.'/website/menu/MenuHandler.js', array('jquery'));
		wp_register_script('portfolio', JS.'/website/sections/portfolio.js', array('jquery'));
		wp_register_script('recent_projects', JS.'/website/sections/recent_projects.js', array('jquery'));
		wp_register_script('contact', JS.'/website/sections/contact_section.js', array('jquery'));
		wp_register_script('blog_section', JS.'/website/sections/blog_section.js', array('jquery'));
		wp_register_script('main', JS.'/main.js', array('jquery'));
													
		wp_enqueue_script('tweenmax');
		wp_enqueue_script('scrollToPlugin');
		wp_enqueue_script('stelarjs');	
		wp_enqueue_script('waypoints');
		wp_enqueue_script('detectMobileBrowser');
		
		wp_enqueue_script('jflickr');
		wp_enqueue_script('isotope');
		wp_enqueue_script('sliderX');
		
		
		//twitter
		wp_register_script('jquery_tweet', JS.'/libs/Tweet1_1.js', array('jquery'));
		wp_enqueue_script('jquery_tweet');
						
		
		wp_enqueue_script('easing1_3');
		//wp_enqueue_script('jquery_transition');
		wp_enqueue_script('EventBus');
		wp_enqueue_script('genericevents');
		wp_enqueue_script('JQueryAjax');						
		
		
		wp_enqueue_script('main_preloader');
		wp_enqueue_script('logo_controler');
		wp_enqueue_script('menu_handler');
		wp_enqueue_script('portfolio');
		wp_enqueue_script('recent_projects');
		wp_enqueue_script('contact');
		wp_enqueue_script('blog_section');
		wp_enqueue_script('main');
		
		$themeOptions = get_option('themeOptions');
		$emailReceive = (isset($themeOptions['emailReceive']))?$themeOptions['emailReceive']:'youremail@yourwebsite.com';
		$isMenuOnFirstpage = (isset($themeOptions['isMenuOnFirstpage']))?$themeOptions['isMenuOnFirstpage']:"";						
		
		$t_user = (isset($themeOptions['t_user']))?$themeOptions['t_user']:'';
		$tweetsNo = (isset($themeOptions['tweetsNo']))?$themeOptions['tweetsNo']:'3';		
		
		$js_data = array('TWITTER_SERVICE_FEED'=>TWITTER_SERVICE_FEED, 'SK_T_USER'=>$t_user, 'SK_T_COUNT'=>$tweetsNo, 'RESOURCES_PATH'=>TEMPPATH, 'IMAGES_PATH'=>IMAGES, 'MAIL_SCRIPT' => TEMPPATH.'/mail.php', 'emailReceive'=>$emailReceive, 'isMenuOnFirstpage'=>$isMenuOnFirstpage);		
		wp_localize_script('main', 'globalJS', $js_data);		
		
		return;					
	}	
	//utils - convert hex to rgb	
	private function html2rgb($color)
	{
	    if ($color[0] == '#')
	        $color = substr($color, 1);
	    if (strlen($color) == 6)
	        list($r, $g, $b) = array($color[0].$color[1],
	                                 $color[2].$color[3],
	                                 $color[4].$color[5]);
	    elseif (strlen($color) == 3)
	        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
	    else
	        return false;
	    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	    return array($r, $g, $b);
	}			
}

?>