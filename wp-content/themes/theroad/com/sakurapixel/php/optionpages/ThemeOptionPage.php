<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/optionpages/GenericOptionPage.php');
class ThemeOptionPage extends GenericOptionPage{
	
		//page content - override
	public function showPageContent(){
		$options = get_option($this->getOptionGroup());				
		
		//logo
		$logoImage = IMAGES.'/default/logo.png';
		$logoAttachementID = (isset($options['logoAttachementID'])?$options['logoAttachementID']:'');
		if(isset($logoAttachementID)){
			if($logoAttachementID!=""){
				//get image
				$logoImage = wp_get_attachment_image_src($logoAttachementID, 'full');
				$logoImage = $logoImage[0];
			}
		}else{
			//get default image
			$logoAttachementID = "";
		}
		//end logo
		
		
		
		//preload logo
		$preloadLogo = IMAGES.'/default/logo_preload.png';
		$preloadLogoAttachementID = (isset($options['preloadLogoAttachementID'])?$options['preloadLogoAttachementID']:'');
		if(isset($preloadLogoAttachementID)){
			if($preloadLogoAttachementID!=""){
				//get image
				$preloadLogo = wp_get_attachment_image_src($preloadLogoAttachementID, 'full');
				$preloadLogo = $preloadLogo[0];
			}
		}else{
			//get default image
			$logoAttachementID = "";
		}
		//end preload logo
		
		
		//homepage background		
		$homepageBackgroundImage = IMAGES.'/default/default_background.jpg';
		$homepageBackgroundAttachementID = (isset($options['homepageBackgroundAttachementID'])?$options['homepageBackgroundAttachementID']:'');
		if(isset($homepageBackgroundAttachementID)){
			if($homepageBackgroundAttachementID!=""){
				//get image
				$homepageBackgroundImage = wp_get_attachment_image_src($homepageBackgroundAttachementID, 'full');
				$homepageBackgroundImage = $homepageBackgroundImage[0];
			}
		}else{
			//get default image
			$homepageBackgroundAttachementID = "";
		}				
		
		//end homepage background	
		
		
		
		//home social sidebar
		$showSocialSidebarChecked = '';
		$showSocialSidebar = (isset($options['showSocialSidebar']))?$options['showSocialSidebar']:"";
		if($showSocialSidebar=='ON'){
			$showSocialSidebarChecked = 'checked';
		}
		
		$showScrollDownChecked = '';
		$showScrollDown = (isset($options['showScrollDown']))?$options['showScrollDown']:"";
		if($showScrollDown=='ON'){
			$showScrollDownChecked = 'checked';
		}				
		// end home social sidebar			
		
		
		//menu settings
		$menuBackgroundColor = (isset($options['menuBackgroundColor'])&&$options['menuBackgroundColor']!='')?$options['menuBackgroundColor']:'93abbf';
		$menuTextColor = (isset($options['menuTextColor'])&&$options['menuTextColor']!='')?$options['menuTextColor']:'fffde8';
		$menuTextShadowColor = (isset($options['menuTextShadowColor'])&&$options['menuTextShadowColor']!='')?$options['menuTextShadowColor']:'000000';
		$selectedMenuColor = (isset($options['selectedMenuColor'])&&$options['selectedMenuColor']!='')?$options['selectedMenuColor']:'7392ad';		
		
		$isMenuOnFirstpageChecked = '';
		$isMenuOnFirstpage = (isset($options['isMenuOnFirstpage']))?$options['isMenuOnFirstpage']:"";
		if($isMenuOnFirstpage=='ON'){
			$isMenuOnFirstpageChecked = 'checked';
		}		
		//end menu settings
				
		
		
		
		//parallax settings
		$parallaxOnMobileChecked = '';		
		$parallaxOnMobile = (isset($options['parallaxOnMobile']))?$options['parallaxOnMobile']:"";
		if($parallaxOnMobile=='ON'){
			$parallaxOnMobileChecked = 'checked';
		}		
		//end parallax settings		
		
		
		//portfolio settings
		
		$portfolioDefaultColor = "";
		$portfolioDefaultColor = (isset($options['portfolioDefaultColor'])&&$options['portfolioDefaultColor']!='')?$options['portfolioDefaultColor']:'ec761a';
		
		$portfolioOrangeSelected = 'selected="selected"';
		$portfolioRedSelected = '';
		$portfolioBlueSelected = '';
		$portfolioGreenSelected = '';
		$portfolioBlackSelected = '';
		$portfolioGreySelected = '';		
		$portfolioButtonColor = (isset($options['portfolioButtonColor']))?$options['portfolioButtonColor']:"";
		if(isset($portfolioButtonColor)){			
				switch ($portfolioButtonColor) {
					case 'orange':
						$portfolioOrangeSelected = 'selected="selected"';
						break;
					case 'red':
						$portfolioRedSelected = 'selected="selected"';
						break;
					case 'blue':
						$portfolioBlueSelected = 'selected="selected"';
						break;
					case 'green':
						$portfolioGreenSelected = 'selected="selected"';
						break;
					case 'black':
						$portfolioBlackSelected = 'selected="selected"';
						break;
					case 'grey':
						$portfolioButtonColor = 'selected="selected"';
						break;																																																	
					default:
						
						break;
				}			
		}		
		//end portfolio settings
			
		
		
		//blog
		$showBlogCommentsChecked = '';
		$showBlogComments = (isset($options['showBlogComments']))?$options['showBlogComments']:"";
		if($showBlogComments=='ON'){
			$showBlogCommentsChecked = 'checked';
		}
		$showBlogReplayChecked = '';
		$showBlogReplay = (isset($options['showBlogReplay']))?$options['showBlogReplay']:"";
		if($showBlogReplay=='ON'){
			$showBlogReplayChecked = 'checked';
		}

		$blogDefaultColor = (isset($options['blogDefaultColor'])&&$options['blogDefaultColor']!='')?$options['blogDefaultColor']:'ec761a';
		
		$blogOrangeSelected = 'selected="selected"';
		$blogRedSelected = '';
		$blogBlueSelected = '';
		$blogGreenSelected = '';
		$blogBlackSelected = '';
		$blogGreySelected = '';		

		$blogColors = (isset($options['blogButtonColor']))?$options['blogButtonColor']:"";
		
		if(isset($blogColors)){			
				switch ($blogColors) {
					case 'orange':
						$blogOrangeSelected = 'selected="selected"';
						break;
					case 'red':
						$blogRedSelected = 'selected="selected"';
						break;
					case 'blue':
						$blogBlueSelected = 'selected="selected"';
						break;
					case 'green':
						$blogGreenSelected = 'selected="selected"';
						break;
					case 'black':
						$blogBlackSelected = 'selected="selected"';
						break;
					case 'grey':
						$blogGreySelected = 'selected="selected"';
						break;																																																	
					default:
						
						break;
				}			
		}	
		//end blog	
		
		
		//footer settings
		$footerBackgroundColor = (isset($options['footerBackgroundColor'])&&$options['footerBackgroundColor']!='')?$options['footerBackgroundColor']:'2f363e';
		$widgetsTitleColor = (isset($options['widgetsTitleColor'])&&$options['widgetsTitleColor']!='')?$options['widgetsTitleColor']:'ec761a';		
		$footerLinksColor = (isset($options['footerLinksColor'])&&$options['footerLinksColor']!='')?$options['footerLinksColor']:'707070';			
		
		$footerBackgroundImage = "";
		$footerBackgroundAttachementID = (isset($options['footerBackgroundAttachementID'])?$options['footerBackgroundAttachementID']:'');
		if(isset($footerBackgroundAttachementID)){
			if($footerBackgroundAttachementID!=""){
				//get image
				$footerBackgroundImage = wp_get_attachment_image_src($footerBackgroundAttachementID, 'full');
				$footerBackgroundImage = $footerBackgroundImage[0];
			}
		}		
		//end footer settings			
		
		
		//google Analytics
		$googleAnalytics = wptexturize((isset($options['googleAnalytics']))?$options['googleAnalytics']:'');
		
		//contact settings
		$emailReceive = (isset($options['emailReceive']))?$options['emailReceive']:'youremail@yourwebsite.com';		
		$mapEmbedded = (isset($options['mapEmbedded'])&&$options['mapEmbedded']!='')?$options['mapEmbedded']:$this->getDefaultMap();
		
		$conatctDefaultColor = (isset($options['conatctDefaultColor'])&&$options['conatctDefaultColor']!='')?$options['conatctDefaultColor']:'ec761a';
		
		$contactOrangeSelected = 'selected="selected"';
		$contactRedSelected = '';
		$contactBlueSelected = '';
		$contactGreenSelected = '';
		$contactBlackSelected = '';
		$contactGreySelected = '';		

		$contactButtonColor = (isset($options['contactButtonColor']))?$options['contactButtonColor']:"";
		
		if(isset($contactButtonColor)){			
				switch ($contactButtonColor) {
					case 'orange':
						$contactOrangeSelected = 'selected="selected"';
						break;
					case 'red':
						$contactRedSelected = 'selected="selected"';
						break;
					case 'blue':
						$contactBlueSelected = 'selected="selected"';
						break;
					case 'green':
						$contactGreenSelected = 'selected="selected"';
						break;
					case 'black':
						$contactBlackSelected = 'selected="selected"';
						break;
					case 'grey':
						$contactGreySelected = 'selected="selected"';
						break;																																																	
					default:
						
						break;
				}			
		}
		//end contact
						
		?>
		
		<h1>Theme General Settings</h1>
		<form id="landingOptions" method="post" action="options.php">
			<?php settings_fields($this->getOptionGroup()); ?>
	      						
			<!--theme logo-->	
			<div class="metaBoxContentBox" id="logoContainer">				
					<label class="customLabel">Theme logo (464x202px recommended)</label>
					<div class="hLine"></div>				
					<div>
						<img class="imgUI" src="<?php echo $logoImage; ?>" />
					</div>
					<div class="space01"></div>
					<a class="button-secondary uploadBTN" href="#">Upload logo image</a>					
					<input class="imageAttachementID" class="inputText fullWidth" type="hidden" name="<?php echo $this->getOptionGroup();?>[logoAttachementID]" value="<?php echo $logoAttachementID;?>" />																																															
			</div>
			<!--/theme logo-->
			
			<!--theme logo loading-->	
			<div class="metaBoxContentBox" id="loadingLogoContainer">				
					<label class="customLabel">Theme loading logo</label>
					<div class="hLine"></div>				
					<div>
						<img class="imgUI" src="<?php echo $preloadLogo; ?>" />
					</div>
					<div class="space01"></div>
					<a class="button-secondary uploadBTN" id="loadingLogoContainerBTN" href="#">Upload preload logo </a>					
					<input class="imageAttachementID" class="inputText fullWidth" type="hidden" name="<?php echo $this->getOptionGroup();?>[preloadLogoAttachementID]" value="<?php echo $preloadLogoAttachementID;?>" />																																															
			</div>
			<!--/theme logo loading-->			
			
			
			<!--hompage background-->	
			<div class="metaBoxContentBox" id="homepageBackground">				
					<label class="customLabel">Homepage background (1920x1080px recommended 200-300KB)</label>
					<div class="hLine"></div>
					<p>The background will show on the homepage</p>				
					<div>
						<img class="imgUI" src="<?php echo $homepageBackgroundImage; ?>" />
					</div>
					<div class="space01"></div>
					<a class="button-secondary uploadBTN" href="#">Upload background image</a>					
					<input class="imageAttachementID" class="inputText fullWidth" type="hidden" name="<?php echo $this->getOptionGroup();?>[homepageBackgroundAttachementID]" value="<?php echo $homepageBackgroundAttachementID;?>" />																																															
			</div>
			<!--/hompage background-->
			
			<!--hompage social-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Homepage settings</label>
					<div class="hLine"></div>
					<div class="space01"></div>
					<input type="checkbox" name="<?php echo $this->getOptionGroup();?>[showSocialSidebar]" value="ON" <?php echo $showSocialSidebarChecked;?> />
					<label class="chBLabel">Show social sidebar (default Off)</label>
					<div class="space01"></div>
					<input type="checkbox" name="<?php echo $this->getOptionGroup();?>[showScrollDown]" value="ON" <?php echo $showScrollDownChecked;?> />
					<label class="chBLabel">Show scroll down button (default Off)</label>																																																			
			</div>
			<!--/hompage social-->
			
			<?php
				$t_user = (isset($options['t_user']))?$options['t_user']:'';
				$tweetsNo = (isset($options['tweetsNo']))?$options['tweetsNo']:'3';
				$consumerKey = (isset($options['consumerKey']))?$options['consumerKey']:'';
				$consumerSecret = (isset($options['consumerSecret']))?$options['consumerSecret']:'';
				$accessToken = (isset($options['accessToken']))?$options['accessToken']:'';
				$accessTokenSecret = (isset($options['accessTokenSecret']))?$options['accessTokenSecret']:'';									
			?>
			<!--Twitter settings-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Twitter settings</label>
					<div class="hLine"></div>
					<p>Twiter patch! Twitter has changed it's API making a bit harder to set up feeds on your website. In order to display tweets please fill out the fields above. If you do not know what those values mean or where to get it from, you can find a tutorial about it <a target="_blank" href="http://www.webdevdoor.com/php/authenticating-twitter-feed-timeline-oauth/">Here</a></p>
					<label class="customLabel">Twitter user</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[t_user]" value="<?php echo $t_user;?>" /><br />
					<label class="customLabel">Max tweets no</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[tweetsNo]" value="<?php echo $tweetsNo;?>" /><br />
					<label class="customLabel">Consumer key</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[consumerKey]" value="<?php echo $consumerKey;?>" /><br />
					<label class="customLabel">Consumer secret:</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[consumerSecret]" value="<?php echo $consumerSecret;?>" /><br />
					<label class="customLabel">Access token</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[accessToken]" value="<?php echo $accessToken;?>" /><br />
					<label class="customLabel">Access token secret:</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[accessTokenSecret]" value="<?php echo $accessTokenSecret;?>" /><br />																									
																																																								
			</div>
			<!--/Twitter settings-->									
			
			
			<!--menu settings-->	
			<div class="metaBoxContentBox" id="menuBox">				
					<label class="customLabel">Menu settings</label>
					<div class="hLine"></div>
					<label class="customLabel">Menu background color</label><br />
					<input id="menuBackgroundColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[menuBackgroundColor]" value="<?php echo $menuBackgroundColor;?>" /><br />
					
					<div class="space01"></div>
					
					<label class="customLabel">Menu text color</label><br />
					<input id="menuTextColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[menuTextColor]" value="<?php echo $menuTextColor;?>" /><br />
					
					<div class="space01"></div>
					
					<label class="customLabel">Menu text shadow color</label><br />
					<input id="menuTextShadowColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[menuTextShadowColor]" value="<?php echo $menuTextShadowColor;?>" /><br />										

					<div class="space01"></div>
					
					<label class="customLabel">Selected Menu background color</label><br />
					<input id="selectedMenuColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[selectedMenuColor]" value="<?php echo $selectedMenuColor;?>" /><br />
					
					<a id="resetMenuDefaultBTN" class="button-secondary">Reset menu to default</a>
					
					<div class="space01"></div><div class="space01"></div>
					
					<input type="checkbox" name="<?php echo $this->getOptionGroup();?>[isMenuOnFirstpage]" value="ON" <?php echo $isMenuOnFirstpageChecked;?> />
					<label class="chBLabel">Is menu on top of homepage (default Off). On will place the menu on top of the homepage.</label>					
																																																									
			</div>
			<!--/menu settings-->																
			
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'default_textdomain') ?>" />
	      	</p>			
			
			
			<!--parallax settings-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Parallax settings</label>
					<div class="hLine"></div>
					<div class="space01"></div>
					<input type="checkbox" name="<?php echo $this->getOptionGroup();?>[parallaxOnMobile]" value="ON" <?php echo $parallaxOnMobileChecked;?> />
					<label class="chBLabel">Disable parallax effect on touch devices</label>
					<div class="space01"></div>																																																																																									
			</div>
			<!--/parallax settings-->			

			
			<!--portfolio settings-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Portfolio settings</label>
					<div class="hLine"></div>
					<label class="customLabel">Portfolio titles/buttons color</label><br />
					<input id="portfolioDefaultColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[portfolioDefaultColor]" value="<?php echo $portfolioDefaultColor;?>" /><br />

																			

					<a id="resetportfolioDefaultBTN" class="button-secondary">Reset portfolio to default</a>
					
					<div class="space01"></div><div class="space01"></div>
					
					<label class="customLabel">Portfolio buy/preview buttons style</label><br />
					<select class="selectUI" name="<?php echo $this->getOptionGroup()?>[portfolioButtonColor]">
					  <option <?php echo $portfolioOrangeSelected;?> value="orange">Orange style</option>
					  <option <?php echo $portfolioRedSelected;?> value="red">Red style</option>
					  <option <?php echo $portfolioBlueSelected;?> value="blue">Blue style</option>
					  <option <?php echo $portfolioGreenSelected;?> value="green">Green style</option>
					  <option <?php echo $portfolioBlackSelected;?> value="black">Black style</option>
					  <option <?php echo $portfolioGreySelected;?> value="grey">Grey style</option>
					</select>																																																									
			</div>
			<!--/portfolio settings-->				
			
			
			
			<!--blog settings-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Blog settings</label>
					<div class="hLine"></div>
					<div class="space01"></div>
					<input type="checkbox" name="<?php echo $this->getOptionGroup();?>[showBlogComments]" value="ON" <?php echo $showBlogCommentsChecked;?> />
					<label class="chBLabel">Show blog comments</label>
					<div class="space01"></div>
					<input type="checkbox" name="<?php echo $this->getOptionGroup();?>[showBlogReplay]" value="ON" <?php echo $showBlogReplayChecked;?> />
					<label class="chBLabel">Show blog replay form (Show existing comments - users can not post comments anymore)</label>
										
					<div class="space01"></div><div class="space01"></div>
					<label class="customLabel">Blog titles/small buttons color</label><br />
					<input id="blogDefaultColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[blogDefaultColor]" value="<?php echo $blogDefaultColor;?>" /><br />					
					
					<a id="resetBlogDefaultBTN" class="button-secondary">Reset blog titles color</a>
					
					<div class="space01"></div><div class="space01"></div>	
					
					<label class="customLabel">Blog comment button style</label><br />
					<select class="selectUI" name="<?php echo $this->getOptionGroup()?>[blogButtonColor]">
					  <option <?php echo $blogOrangeSelected;?> value="orange">Orange style</option>
					  <option <?php echo $blogRedSelected;?> value="red">Red style</option>
					  <option <?php echo $blogBlueSelected;?> value="blue">Blue style</option>
					  <option <?php echo $blogGreenSelected;?> value="green">Green style</option>
					  <option <?php echo $blogBlackSelected;?> value="black">Black style</option>
					  <option <?php echo $blogGreySelected;?> value="grey">Grey style</option>
					</select>																																																																																										
			</div>
			<!--/blog settings-->
			
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'default_textdomain') ?>" />
	      	</p>
	      	
			<!--footer settings-->	
			<div class="metaBoxContentBox" id="footerBox">				
					<label class="customLabel">Footer settings</label>
					<div class="hLine"></div>
					<label class="customLabel">Footer background color</label><br />
					<input id="footerBackgroundColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[footerBackgroundColor]" value="<?php echo $footerBackgroundColor;?>" /><br />
					
					<div class="space01"></div>
					
					<label class="customLabel">Footer - widgets title color</label><br />
					<input id="widgetsTitleColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[widgetsTitleColor]" value="<?php echo $widgetsTitleColor;?>" /><br />					
					
					<div class="space01"></div>	
					
					<label class="customLabel">Footer links color</label><br />
					<input id="footerLinksColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[footerLinksColor]" value="<?php echo $footerLinksColor;?>" /><br />
														
					<label class="customLabel">Footer background (optional)</label><br />			
					<div>
						<img class="imgUI" src="<?php echo $footerBackgroundImage; ?>" />
					</div>
					<a class="button-secondary uploadBTN" href="#">Upload background image</a>					
					<input class="imageAttachementID" class="inputText fullWidth" type="hidden" name="<?php echo $this->getOptionGroup();?>[footerBackgroundAttachementID]" value="<?php echo $footerBackgroundAttachementID;?>" />						
					

					<div class="space01"></div>	
					<div class="space01"></div>											
					

					<a id="resetfooterDefaultBTN" class="button-secondary">Reset footer to default</a>																																																				
			</div>
			<!--/footer settings-->		      							
					
			
			<!--google analytics-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Google Analytics</label>
					<div class="hLine"></div>				
					<textarea class="boxContentTextarea" name="<?php echo $this->getOptionGroup();?>[googleAnalytics]" rows="4"><?php echo $googleAnalytics; ?></textarea>																																																								
			</div>
			<!--/google analytics-->
			
			<!--contact options gmap-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Google map iframe code</label>
					<div class="hLine"></div>				
					<textarea class="boxContentTextarea" name="<?php echo $this->getOptionGroup();?>[mapEmbedded]" rows="4"><?php echo $mapEmbedded; ?></textarea>																																									
			</div>
			<!--/contact options gmap-->
			
			<!--contact options email-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Contact options</label>
					<div class="hLine"></div>
					<label class="customLabel">Contact email (where to receive emails)</label><br />			
					<input class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[emailReceive]" value="<?php echo $emailReceive;?>" /><br />
					
					<div class="space01"></div><div class="space01"></div>
					<label class="customLabel">Contact titles color</label><br />
					<input id="conatctDefaultColor" class="inputText" type="text" name="<?php echo $this->getOptionGroup();?>[conatctDefaultColor]" value="<?php echo $conatctDefaultColor;?>" /><br />
					<a id="resetContactDefaultBTN" class="button-secondary">Reset contact titles color</a>
					
					<div class="space01"></div><div class="space01"></div>	
					
					<label class="customLabel">Contact form button style</label><br />
					<select class="selectUI" name="<?php echo $this->getOptionGroup()?>[contactButtonColor]">
					  <option <?php echo $contactOrangeSelected;?> value="orange">Orange style</option>
					  <option <?php echo $contactRedSelected;?> value="red">Red style</option>
					  <option <?php echo $contactBlueSelected;?> value="blue">Blue style</option>
					  <option <?php echo $contactGreenSelected;?> value="green">Green style</option>
					  <option <?php echo $contactBlackSelected;?> value="black">Black style</option>
					  <option <?php echo $contactGreySelected;?> value="grey">Grey style</option>
					</select>										
																																																										
			</div>
			<!--/contact options email-->					
												
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'default_textdomain') ?>" />
	      	</p>		
		</form>		
		
		<?php		
		
	}

	public function getDefaultMap(){
		$defMap = '<iframe width="920" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=San+Francisco,+CA,+United+States&aq=0&oq=san+frans&sll=45.824859,24.973973&sspn=0.070099,0.163765&ie=UTF8&hq=&hnear=San+Francisco,+California&t=m&ll=37.774921,-122.419453&spn=0.023745,0.078964&z=14&iwloc=A&output=embed"></iframe>';
		return $defMap;
	}

	
}

?>