<?php
$wp_path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))));
require_once($wp_path.'/wp-config.php');
session_start();
require_once(dirname(__FILE__)."/twitteroauth/twitteroauth.php"); //Path to twitteroauth library

$options = get_option('themeOptions');

				$twitteruser = (isset($options['t_user']))?$options['t_user']:'';
				$notweets = (isset($options['tweetsNo']))?$options['tweetsNo']:'3';
				$consumerkey = (isset($options['consumerKey']))?$options['consumerKey']:'';
				$consumersecret = (isset($options['consumerSecret']))?$options['consumerSecret']:'';
				$accesstoken = (isset($options['accessToken']))?$options['accessToken']:'';
				$accesstokensecret = (isset($options['accessTokenSecret']))?$options['accessTokenSecret']:'';

 
function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
  $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
  return $connection;
}
  
$connection = getConnectionWithAccessToken($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);
 
$tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$twitteruser."&count=".$notweets);
 
echo json_encode($tweets);
?>