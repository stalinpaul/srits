<?php
/**
 * 
 */
class SkShortcodes{
	
	public function registerShortcodes(){						
		//one half
		add_shortcode('one_half', array($this, 'one_half'));
		
		//one third
		add_shortcode('one_third', array($this, 'one_third'));
		
		//two_thirds		
		add_shortcode('two_thirds', array($this, 'two_thirds'));
		
		//full_width		
		add_shortcode('full_width', array($this, 'full_width'));
		
		
		//header	
		add_shortcode('header', array($this, 'sk_header'));
		
		//inner link
		add_shortcode('sk_inner_link', array($this, 'sk_inner_link'));
		
		//team_member			
		add_shortcode('team_member_name', array($this, 'team_member_name'));	
		add_shortcode('team_member_position', array($this, 'team_member_position'));
		add_shortcode('team_member_about', array($this, 'team_member_about'));
		
		//pricing table box
		add_shortcode('pricing_table_box', array($this, 'pricing_table_box'));
		
		//skills shortcode
		add_shortcode('skillsContainer', array($this, 'skillsContainer'));
		add_shortcode('skill', array($this, 'skill'));
			
		
		//landing page
		add_shortcode('landingPageHeader1', array($this, 'landingPageHeader1'));
		add_shortcode('landingPageHeader2', array($this, 'landingPageHeader2'));
		add_shortcode('landingPageHeader3', array($this, 'landingPageHeader3'));
		add_shortcode('landingPageDefaultText', array($this, 'landingPageDefaultText'));
		
		
		//clientbox
		add_shortcode('clientbox', array($this, 'clientbox'));
		add_shortcode('client', array($this, 'client'));
		
		//phone	
		add_shortcode('phone', array($this, 'phone'));
		
		//email	
		add_shortcode('email', array($this, 'email'));
		
		//button	
		add_shortcode('button', array($this, 'button'));
		
		//youtube	
		add_shortcode('youtube', array($this, 'youtube'));
		
		//vimeo	
		add_shortcode('vimeo', array($this, 'vimeo'));
		
		//soundcloud	
		add_shortcode('soundcloud', array($this, 'soundcloud'));				
		
																										
	}
	

	
	//get last code for grid columns
	public function get_last(){
		return '<div class="resetColumns"></div>';
	}
	/* one half
	================================================== */	
	public function one_half($atts, $content = null){
		extract(shortcode_atts(array('lastcol' => ''), $atts));
		$lastCode = ($lastcol=='yes')?$this->get_last():'';
		$return_val = '<div class="eight columns">'.do_shortcode($content).'</div>'.$lastCode;
		return $return_val;		
	}
	
	/* one third
	================================================== */	
	public function one_third($atts, $content = null){
		extract(shortcode_atts(array('lastcol' => ''), $atts));
		$lastCode = ($lastcol=='yes')?$this->get_last():'';
		$return_val = '<div class="one-third column">'.do_shortcode($content).'</div>'.$lastCode;
		return $return_val;		
	}
	
	/* two thirs
	================================================== */	
	public function two_thirds($atts, $content = null){
		extract(shortcode_atts(array('lastcol' => ''), $atts));
		$lastCode = ($lastcol=='yes')?$this->get_last():'';
		$return_val = '<div class="two-thirds column">'.do_shortcode($content).'</div>'.$lastCode;
		return $return_val;		
	}		
	
	/* full_width
	================================================== */	
	public function full_width($atts, $content = null){
		extract(shortcode_atts(array('lastcol' => 'yes'), $atts));		
		$lastCode = $this->get_last();
		$return_val = '<div class="sixteen columns">'.do_shortcode($content).'</div>'.$lastCode;
		return $return_val;		
	}
	
	
	
	/* default header
	================================================== */	
	public function sk_header($atts, $content = null){
		extract(shortcode_atts(array('color' => '#ec761a'), $atts));
		$return_val = '<h2 style="color: '.$color.'">'.$content.'</h2>';
		return $return_val;		
	}
	
	/* inner link
	================================================== */	
	public function sk_inner_link($atts, $content = null){
		extract(shortcode_atts(array('page' => '1'), $atts));		
		$return_val = '<a class="innerLink" href="'.$page.'">'.$content.'</a>';
		return $return_val;		
	}	
	
	
	/* team_member 
	================================================== */	
	public function team_member_name($atts, $content = null){
		extract(shortcode_atts(array('color'=>'#5e5a62'), $atts));
		$return_val = '<p class="teamMemberName" style="color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}
	public function team_member_position($atts, $content = null){
		extract(shortcode_atts(array('color'=>'#5e5a62'), $atts));
		$return_val = '<p class="teamMemberPosition" style="color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}	
	
	public function team_member_about($atts, $content = null){
		extract(shortcode_atts(array('color'=>'#5e5a62'), $atts));
		$return_val = '<p class="teamMemberContent defaultText" style="color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}		
	
	/* pricing table box 
	================================================== */	
	public function pricing_table_box($atts, $content = null){
		extract(shortcode_atts(array('title'=>'box title', 'currency'=>'$', 'price'=>'29', 'plan'=>'month', 'buttonText'=>'Learn More...', 
		'buttonColor'=>'maroon', 'buttonurl'=>'#'), $atts));
		$return_val = '
							<!--pricing box-->
							<div class="pricingTableBox">
								<div class="pricingTableBoxInner">
									<div class="pricingBoxTitleContainer">
										<p class="pricingTitle">'.$title.'</p>
									</div>
									
									<div class="pricingLine1"></div>
									<div class="pricingLine2"></div>
									
									<div class="pricingMainContent">										
										<p class="pricingPrice"><span class="dollar">'.$currency.'</span>'.$price.'<span class="priceMonth">/'.$plan.'</span></p>										
									</div>
									
									<div class="pricingLine3"></div>
									
									<p class="pricingTextContent">
										'.$content.'										
									</p>
									
									<div class="pricingBtnUI">
										<a href="'.$buttonurl.'" target="_blank" class="genericButton '.$buttonColor.'">'.$buttonText.'</a>
									</div>
									
								</div>
							</div>
							<!--/pricing box-->		
		';
		return $return_val;				
	}

	/* skills
	================================================== */
	//skillsContainer
	public function skillsContainer($atts, $content = null){
		extract(shortcode_atts(array('height' => '400px'), $atts));
		$return_val = '
							<div class="skillContainer" style="height: '.$height.';">
								'.do_shortcode($content).'
							</div>		
		';
		return $return_val;			
	}	
		
	public function skill($atts, $content = null){
		extract(shortcode_atts(array('left' => '50px', 'top'=>'50px', 'percent'=>'100', 'color'=>'#c7c48e'), $atts));
		
		$return_val = '
								<div data-percent="'.$percent.'" class="skill" style="left: '.$left.'; top: '.$top.';background-color: '.$color.';">
									<p class="skillPercent">'.$percent.'%</p>
									<p class="defaultText skillText">'.$content.'</p>
								</div>	
		';
		return $return_val;		
	}
	
	/* landing page
	================================================== */		
	public function landingPageHeader1($atts, $content = null){
		extract(shortcode_atts(array('align' => 'center', 'color'=>'#fff7d0'), $atts));		
		$return_val = '<p class="landingPageHeader1" style="text-align: '.$align.';color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}
	public function landingPageHeader2($atts, $content = null){
		extract(shortcode_atts(array('align' => 'center', 'color'=>'#fff7d0'), $atts));		
		$return_val = '<p class="landingPageHeader2" style="text-align: '.$align.';color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}
	public function landingPageHeader3($atts, $content = null){
		extract(shortcode_atts(array('align' => 'center', 'color'=>'#fff7d0'), $atts));		
		$return_val = '<p class="landingPageHeader3" style="text-align: '.$align.';color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}
	public function landingPageDefaultText($atts, $content = null){
		extract(shortcode_atts(array('align' => 'center', 'color'=>'#fff7d0'), $atts));		
		$return_val = '<p class="landingPageDefaultText" style="text-align: '.$align.';color: '.$color.';">'.$content.'</p>';
		return $return_val;		
	}				
	
	
	
	/* clientbox
	================================================== */	
	public function clientbox($atts, $content = null){		
		$return_val = '<ul class="ourClients">'.do_shortcode($content).'</ul>';
		return $return_val;		
	}
	public function client($atts, $content = null){
		extract(shortcode_atts(array('imageurl' => '', 'link'=>'#'), $atts));				
		$return_val = '<li class="ourClientsList"><a target="_blank" href="'.$link.'"><img src="'.$imageurl.'" alt="" /></a></li>';
		return $return_val;		
	}	

	
	/* phone
	================================================== */	
	public function phone($atts, $content = null){
		extract(shortcode_atts(array('tel' => '', 'color'=>'#ec761a'), $atts));				
		$return_val = '<a class="phoneBig" style="color:'.$color.'" href="tel:'.$tel.'">'.$content.'</a>';
		return $return_val;		
	}

	/* email
	================================================== */	
	public function email($atts, $content = null){
		extract(shortcode_atts(array('mailto' => ''), $atts));				
		$return_val = '<a href="mailto:'.$mailto.'">'.$content.'</a>';
		return $return_val;		
	}

	/* button
	================================================== */	
	public function button($atts, $content = null){
		extract(shortcode_atts(array('color' => 'orange', 'align' => '', 'href'=>'', 'target'=>'_self'), $atts));				
		$return_val = '<a href="'.$href.'" target="'.$target.'" class="genericButton '.$align.' '.$color.'">'.$content.'</a>';
		return $return_val;		
	}
	
	/* youtube
	================================================== */	
	public function youtube($atts, $content = null){
		extract(shortcode_atts(array('width' => '100%', 'height'=>'360', 'id'=>''), $atts));				
		$return_val = '<div class="skvideo"><iframe title="youtube video" width="' . $width . '" height="' . $height . '" src="http://www.youtube.com/embed/' . $id . '" frameborder="0" allowfullscreen></iframe></div>';
		return $return_val;		
	}
	
	/* vimeo
	================================================== */	
	public function vimeo($atts, $content = null){
		extract(shortcode_atts(array('width' => '100%', 'height'=>'360', 'id'=>''), $atts));				
		$return_val = '<div class="skvideo"><iframe src="http://player.vimeo.com/video/' . $id . '" width="' . $width . '" height="' . $height . '" frameborder="0"></iframe></div>';
		return $return_val;		
	}

	/* soundcloud
	================================================== */	
	public function soundcloud($atts, $content = null){
		extract(shortcode_atts(array('url' => '', 'width' => '100%', 'height'=>'81', 'comments'=>'false', 'auto_play'=>'false', 'color'=>'ec761a'), $atts));				
		$return_val = '<object height="' . $height . '" width="' . $width . '"><param name="movie" value="http://player.soundcloud.com/player.swf?url=' . urlencode($url) . '&amp;show_comments=' . $comments . '&amp;auto_play=' . $auto_play . '&amp;color=' . $color . '"></param><param name="allowscriptaccess" value="always"></param><embed allowscriptaccess="always" height="' . $height . '" src="http://player.soundcloud.com/player.swf?url=' . urlencode($url) . '&amp;show_comments=' . $comments . '&amp;auto_play=' . $auto_play . '&amp;color=' . $color . '" type="application/x-shockwave-flash" width="' . $width . '"></embed></object>';
		return $return_val;		
	}					

		
}

?>
