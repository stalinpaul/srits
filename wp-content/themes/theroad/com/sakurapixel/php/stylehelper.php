<?php

class StyleHelper{
	
	//get menu colors
	public function getMenuColors(){
		$themeOptions = get_option('themeOptions');
		$menuBackgroundColor = (isset($themeOptions['menuBackgroundColor']))?$themeOptions['menuBackgroundColor']:'93abbf';
		$menuTextColor = (isset($themeOptions['menuTextColor']))?$themeOptions['menuTextColor']:'fffde8';
		$menuTextShadowColor = (isset($themeOptions['menuTextShadowColor']))?$themeOptions['menuTextShadowColor']:'000000';
		$selectedMenuColor = (isset($themeOptions['selectedMenuColor']))?$themeOptions['selectedMenuColor']:'7392ad';
		
		$menuColors = array("menuBackgroundColor"=>$menuBackgroundColor, "menuTextColor"=>$menuTextColor, 
		"menuTextShadowColor"=>$menuTextShadowColor, "selectedMenuColor"=>$selectedMenuColor);
		return $menuColors;
	}
	
	//get footer colors
	public function getFooterColors(){
		$themeOptions = get_option('themeOptions');
		$footerBackgroundColor = (isset($themeOptions['footerBackgroundColor']))?$themeOptions['footerBackgroundColor']:'2f363e';
		$widgetsTitleColor = (isset($themeOptions['widgetsTitleColor']))?$themeOptions['widgetsTitleColor']:'ec761a';
		$footerLinksColor = (isset($themeOptions['footerLinksColor']))?$themeOptions['footerLinksColor']:'707070';
		
		$footerColors = array("footerBackgroundColor"=>$footerBackgroundColor, "widgetsTitleColor"=>$widgetsTitleColor, 
		"footerLinksColor"=>$footerLinksColor);
		return $footerColors;
	}
	
	//get footer background
	public function getFooterBackground(){
		$themeOptions = get_option('themeOptions');
		$backgroundStyle = "";
		$backgroundImageStyle = "";
		
		@$attachementID = $themeOptions['footerBackgroundAttachementID'];		
		if(isset($attachementID)){
			if($attachementID!=""){
				$imageURLTemp = wp_get_attachment_image_src($attachementID, 'full');
				$imageURLTemp = $imageURLTemp[0];
				if(isset($imageURLTemp) && $imageURLTemp!=""){
					$backgroundImageStyle = 'background: url('.$imageURLTemp.'); background-attachment:fixed;';
				}				
			}						
		}
		$footerColors = $this->getFooterColors();
		$backgroundStyle .= $backgroundImageStyle.' '.'background-color: #'.$footerColors['footerBackgroundColor'].';';		
		return $backgroundStyle;		
	}
	
	//get portfolio defaut color
	public function getPortfolioDefaultColor(){
		$themeOptions = get_option('themeOptions');
		$defaultCol = (isset($themeOptions['portfolioDefaultColor']))?$themeOptions['portfolioDefaultColor']:'ec761a';
		return $defaultCol;
	}
	
	//get blog defaut color
	public function getBlogDefaultColor(){
		$themeOptions = get_option('themeOptions');
		$defaultCol = (isset($themeOptions['blogDefaultColor']))?$themeOptions['blogDefaultColor']:'ec761a';
		return $defaultCol;
	}
	
	//get contact defaut color
	public function getContactDefaultColor(){
		$themeOptions = get_option('themeOptions');
		$defaultCol = (isset($themeOptions['conatctDefaultColor']))?$themeOptions['conatctDefaultColor']:'ec761a';
		return $defaultCol;
	}	
						
}
?>