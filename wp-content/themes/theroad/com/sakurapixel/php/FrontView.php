<?php

class FrontView{
	
	private $hive;
	function __construct($hiveWrapper) {
		$this->hive = $hiveWrapper;					
	}
	
	//get menu items
	public function getMenuItems($value='main'){
		$menuItems = $this->hive->getMenuItems($value);
		return $menuItems;
	}
	

	/* THEME OPTIONS HELPER
	================================================== */	 
	//get theme options
	public function getThemeOptions(){
		$themeOptions = get_option('themeOptions');
		return $themeOptions;
	}
	
	//get theme specific options
	public function getThemeSpecificOption($val){
		$themeOptions = get_option('themeOptions');
		return (isset($themeOptions[$val]))?$themeOptions[$val]:'null-option';
	}	
	
	//get theme logo
	public function getThemeLogoURL(){		
		$options = $this->getThemeOptions();
		$logoAttachementID = $options['logoAttachementID'];		
		
		$logoImage = IMAGES.'/default/logo.png';
		if(isset($logoAttachementID)){
			if($logoAttachementID!=""){
				//get image
				$logoImageTemp = wp_get_attachment_image_src($logoAttachementID, 'full');
				$logoImageTemp = $logoImageTemp[0];
				if(isset($logoImageTemp)){
					$logoImage = $logoImageTemp;
				}				
			}
		}						
		return $logoImage;
	}
	
	//get theme preload logo
	public function getThemePreloadLogoURL(){		
		$options = $this->getThemeOptions();
		@$logoAttachementID = $options['preloadLogoAttachementID'];		
		
		$logoImage = IMAGES.'/default/logo_preload.png';
		if(isset($logoAttachementID)){
			if($logoAttachementID!=""){
				//get image
				$logoImageTemp = wp_get_attachment_image_src($logoAttachementID, 'full');
				$logoImageTemp = $logoImageTemp[0];
				if(isset($logoImageTemp)){
					$logoImage = $logoImageTemp;
				}
			}
		}						
		return $logoImage;
	}
	
	//get homepage background
	public function getHompageBackgroundURL(){		
		$options = $this->getThemeOptions();
		@$attachementID = $options['homepageBackgroundAttachementID'];		
		
		$imageURL = IMAGES.'/default/default_background.jpg';
		if(isset($attachementID)){
			if($attachementID!=""){
				//get image
				$imageURLTemp = wp_get_attachment_image_src($attachementID, 'full');
				$imageURLTemp = $imageURLTemp[0];
				if(isset($imageURLTemp)){
					$imageURL = $imageURLTemp;
				}
			}
		}						
		return $imageURL;
	}
	
	//get page background
	public function getPageBackground($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$backgroundStyle = "";
		$pageBackgroundColor = "e9e9e9";
		$backgroundImageStyle = "";		
		if(isset($custom)){
			$pageBackgroundColor = (isset($custom[0]['pageBackgroundColor']))?$custom[0]['pageBackgroundColor']:"e9e9e9";
			$pageBackgroundAttachementID = (isset($custom[0]['pageBackgroundAttachementID']))?$custom[0]['pageBackgroundAttachementID']:"";
			if(isset($pageBackgroundAttachementID) && $pageBackgroundAttachementID!=""){
				$pageBackgroundIMG = wp_get_attachment_image_src($pageBackgroundAttachementID, 'full');
				$repeatBackground = (isset($custom[0]['repeatBackground']))?$custom[0]['repeatBackground']:"";
				($repeatBackground=="ON")?$rpt="repeat":$rpt="no-repeat";				
				$coverBack = 'background-size:cover;';
				($repeatBackground!="ON")?$coverBack="background-size:cover;":$coverBack="";
				$backgroundImageStyle = 'background: url('.$pageBackgroundIMG[0].'); background-attachment:fixed;background-repeat:'.$rpt.';'.$coverBack;
			}			
		}
		$backgroundStyle .= $backgroundImageStyle.' '.'background-color: #'.$pageBackgroundColor.';';		
		return $backgroundStyle;		
	}

	public function isPageBackgroundCover($postID){
		$isCover = "OFF";
		$custom = get_post_meta($postID, 'page_options', false);
		$pageBackgroundAttachementID = (isset($custom[0]['pageBackgroundAttachementID']))?$custom[0]['pageBackgroundAttachementID']:"";
		if(isset($pageBackgroundAttachementID) && $pageBackgroundAttachementID!=""){
			$pageBackgroundIMG = wp_get_attachment_image_src($pageBackgroundAttachementID, 'full');
			$repeatBackground = (isset($custom[0]['repeatBackground']))?$custom[0]['repeatBackground']:"";
			$isCover = ($repeatBackground!='ON')?'ON':'OFF';
		}
		return $isCover;
	}
	
	public function getBackgroundURL($postID){
		$url = "";
		$custom = get_post_meta($postID, 'page_options', false);
		$pageBackgroundAttachementID = (isset($custom[0]['pageBackgroundAttachementID']))?$custom[0]['pageBackgroundAttachementID']:"";
		if(isset($pageBackgroundAttachementID) && $pageBackgroundAttachementID!=""){
			$pageBackgroundIMG = wp_get_attachment_image_src($pageBackgroundAttachementID, 'full');
			$url = $pageBackgroundIMG[0];			
		}
		return $url;		
	}
	
	
	//get page overlay color
	public function getPageOverlayColor($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$overlayBackgroundStyle = "";
		$pageOverlayColor = "";
		if(isset($custom)){
			$pageOverlayColor = (isset($custom[0]['pageOverlayColor']))?$custom[0]['pageOverlayColor']:"";					
		}
		if(isset($pageOverlayColor) && $pageOverlayColor!=""){
			$pageOverlayColorRGB = $this->html2rgb($pageOverlayColor);
			$overlayBackgroundStyle = 'background:rgba('.$pageOverlayColorRGB[0].','.$pageOverlayColorRGB[1].','.$pageOverlayColorRGB[2].',0.80);';
		}			
		return $overlayBackgroundStyle;		
	}
	
	//get page title hide/show
	public function isPageTitle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$isTitle = true;
		if(isset($custom)){
			$hideTitle = (isset($custom[0]['hideTitle']))?$custom[0]['hideTitle']:"";
			if(isset($hideTitle)&&$hideTitle=="ON"){
				$isTitle = false;
			}				
		}		
		return $isTitle;		
	}
	
	//get page subtitle
	public function getPageSubTitle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$pageSubtitle = "page subtitle...";
		if(isset($custom)){
			$pageSubtitle = (isset($custom[0]['pageSubtitle']))?$custom[0]['pageSubtitle']:"";				
		}		
		return $pageSubtitle;		
	}
	
	//get page header shadow style
	private function getPageHeaderShadowStyle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$pageHeaderShadowStyle = "text-shadow: 0px 1px 1px rgba(255,255,255,0.6);";
		if(isset($custom)){
			$pageTitleShadowColor = (isset($custom[0]['pageTitleShadowColor']))?$custom[0]['pageTitleShadowColor']:"FFF";
			$rgb = $this->html2rgb($pageTitleShadowColor);
			$pageHeaderShadowStyle = 'text-shadow: 0px 1px 1px rgba('.$rgb[0].','.$rgb[1].','.$rgb[2].',0.6);';				
		}		
		return $pageHeaderShadowStyle;		
	}	
	
	//get page header lines style
	public function getPageHeaderLinesStyle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$pageHeaderLinesStyle = "background-color: #CCC";
		if(isset($custom)){
			$pageHeaderLinesColor = (isset($custom[0]['pageHeaderLinesColor']))?$custom[0]['pageHeaderLinesColor']:"CCCCCC";
			$pageHeaderLinesStyle = "background-color: #".$pageHeaderLinesColor.";";					
		}				
		return $pageHeaderLinesStyle;		
	}		
		
	//get page title style
	public function getPageTitleStyle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$pageTitleColorStyle = "color: #6e6a75";
		if(isset($custom)){
			$pageTitleColor = (isset($custom[0]['pageTitleColor']))?$custom[0]['pageTitleColor']:"6e6a75";
			$pageTitleColorStyle = "color: #".$pageTitleColor.";";					
		}
		$shadowStyle = $this->getPageHeaderShadowStyle($postID);		
		return $pageTitleColorStyle.$shadowStyle;		
	}
	
	//get page sub title style
	public function getPageSubTitleStyle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$pageSubTitleColorStyle = "color: #9b9b9b";
		if(isset($custom)){
			$pageSubTitleColor = (isset($custom[0]['pageSubTitleColor']))?$custom[0]['pageSubTitleColor']:"9b9b9b";
			$pageSubTitleColorStyle = "color: #".$pageSubTitleColor.";";					
		}		
		return $pageSubTitleColorStyle;		
	}
	
	//get page default text style
	public function getPageTextStyle($postID){
		$custom = get_post_meta($postID, 'page_options', false);
		$pageTextColorStyle = "color: #5e5a62";
		if(isset($custom)){
			$pagetextColor = (isset($custom[0]['pagetextColor']))?$custom[0]['pagetextColor']:"5e5a62";
			$pageTextColorStyle = "color: #".$pagetextColor.";";					
		}		
		return $pageTextColorStyle;		
	}
	
	//get menu colors
	public function getMenuColors(){
		$themeOptions = get_option('themeOptions');
		$menuBackgroundColor = (isset($themeOptions['menuBackgroundColor']))?$themeOptions['menuBackgroundColor']:'93abbf';
		$menuTextColor = (isset($themeOptions['menuTextColor']))?$themeOptions['menuTextColor']:'fffde8';
		$menuTextShadowColor = (isset($themeOptions['menuTextShadowColor']))?$themeOptions['menuTextShadowColor']:'000000';
		$selectedMenuColor = (isset($themeOptions['selectedMenuColor']))?$themeOptions['selectedMenuColor']:'7392ad';
		
		$this->menuBackgroundRGB = $this->html2rgb($menuBackgroundColor);
		
		$menuColors = array("menuBackgroundColor"=>$menuBackgroundColor, "menuTextColor"=>$menuTextColor, 
		"menuTextShadowColor"=>$menuTextShadowColor, "selectedMenuColor"=>$selectedMenuColor);
		return $menuColors;
	}
	
	private $menuBackgroundRGB;
	public function getMenuRGBA(){
		return $this->menuBackgroundRGB;
	}
			
	
	
	//get portfolio categories
	public function getPortfolioCategories(){
		$categData = array();
		$categories = get_terms('portfolio_categories');							
		if(isset($categories)){
			foreach ($categories as $key => $value) {
				array_push($categData, array('term_id'=>$categories[$key]->term_id, 'name'=>$categories[$key]->name));			
				//echo '<li><a class="eye" href="'.$categories[$key]->term_id.'">'.$categories[$key]->name.'</a></li>';
			}
		}
		return $categData;		
	}
	
	
	//get blog button style
	public function getBlogButtonStyle(){
		$options = get_option('themeOptions');
		$blogbuttonStyle = (isset($options['blogButtonColor']))?$options['blogButtonColor']:"orange";
		return $blogbuttonStyle;
	}

	//get portfolio button style
	public function getPortfolioButtonStyle(){
		$options = get_option('themeOptions');
		$buttonStyle = (isset($options['portfolioButtonColor']))?$options['portfolioButtonColor']:"orange";
		return $buttonStyle;
	}
	
	//get contact button style
	public function getContactButtonStyle(){
		$options = get_option('themeOptions');
		$buttonStyle = (isset($options['contactButtonColor']))?$options['contactButtonColor']:"orange";
		return $buttonStyle;
	}
	
	//show social icons on home page
	public function isHomeSocialSidebar(){
		$options = get_option('themeOptions');
		$isSocial = false;
		$showSocialSidebar = (isset($options['showSocialSidebar']))?$options['showSocialSidebar']:"";
		if($showSocialSidebar=='ON'){
			$isSocial = true;
		}
		return $isSocial;		
	}
	
	public function isParallaxMobile(){
		$options = get_option('themeOptions');
		$parallaxOnMobile = (isset($options['parallaxOnMobile']))?$options['parallaxOnMobile']:"";
		return $parallaxOnMobile;
	}
	
	//show scroll down on home page
	public function isHomeScrollDown(){
		$options = get_option('themeOptions');
		$isScrollDown = false;
		$showScrollDown = (isset($options['showScrollDown']))?$options['showScrollDown']:"";
		if($showScrollDown=='ON'){
			$isScrollDown = true;
		}
		return $isScrollDown;		
	}			
	
	//get specific options group
	public function getOptionsGroup($optionsGroup){
		$options = get_option($optionsGroup);
		return $options;
	}
	/* END THEME OPTIONS HELPER
	================================================== */
	
	
	/* PAGE OPTIONS
	 * HIVE 2.1
	================================================== */
	//retrives all page options
	public function getPageOptions($postID){
		return get_post_meta($postID, 'page_options', false);
	}
	
	public function getPageSpecificOption($postID, $optionKey){
		$custom = get_post_meta($postID, 'page_options', false);
		$option = (isset($custom[0]))?$custom[0][$optionKey]:'';
		return $option;
	}	
	/* END PAGE OPTIONS
	================================================== */			
		
	

	
	/**
	 * get section background
	 * used in HIVE 1.*
	 * deprecated
	 */
	public function validateSectionBackground($value=''){
		$themeBackgrundOptions = get_option($value);
		$defaultBackground = IMAGES."/default/default_background.jpg";
		$backgroundImage = (isset($themeBackgrundOptions['backgroundImage'])&&$themeBackgrundOptions['backgroundImage']!="")?$themeBackgrundOptions['backgroundImage']:$defaultBackground;
		return $backgroundImage;
	}
	
	/*
	 * return page background
	 * introduced in HIVE 2.1 (page requires to have a featured image)	 
	 */ 
	public function getPageFeaturedBackgroundImage($pageID){
		$options = $this->getThemeOptions();
		$backgroundImage = IMAGES.'/default/default_background.jpg';
		$backgroundAttachementID = (isset($options['backgroundAttachementID'])?$options['backgroundAttachementID']:'');
		if(isset($backgroundAttachementID)){
			if($backgroundAttachementID!="" && !empty($backgroundAttachementID)){
				//get image
				$backgroundImage = wp_get_attachment_image_src($backgroundAttachementID, 'full');
				$backgroundImage = $backgroundImage[0];
			}
		}		
		
		if (has_post_thumbnail(get_the_ID())){
			$backgroundImage = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
		}
		return $backgroundImage;
	}
	
	/*
	 * check if is preview
	 * introduced in HIVE 2.1 (only used for AJAX themes)	 
	 */ 
	public function checkPreview($isSingle=false){
		$isAjaxRq = $this->checkRobots();
         if(isset($_GET['preview'])){
            if($_GET['preview']==true){		  
				  //buildURL
				  $pageURL = $this->getCurrentURL();
				  $parseHost = parse_url($pageURL);				  
				  $buildURL  = site_url().'/#!'.$parseHost['path'];
				  header('Location: '.site_url());	
				  //header('Location: '.$buildURL);
             }
         }else if(!$isAjaxRq){
         	header('Location: '.site_url());
         }		
    }
	
	private function checkRobots(){
		$isAjax = false;		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$isAjax = true;
		}		
		
		return $isAjax;	
	}
	
    
	/* UTILS
	================================================== */	    
	//returns current url - used by checkPreview()
	private function getCurrentURL(){
		$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		if ($_SERVER["SERVER_PORT"] != "80")
		{
		    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} 
		else 
		{
		    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;		
	}
	
	//utils - convert hex to rgb	
	public function html2rgb($color)
	{
	    if ($color[0] == '#')
	        $color = substr($color, 1);
	    if (strlen($color) == 6)
	        list($r, $g, $b) = array($color[0].$color[1],
	                                 $color[2].$color[3],
	                                 $color[4].$color[5]);
	    elseif (strlen($color) == 3)
	        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
	    else
	        return false;
	    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	    return array($r, $g, $b);
	}	
	/* END UTILS
	================================================== */			
	
}


?>