<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/customposts/GenericPostType.php');
/**
 * Video Post type 
 * Hive 2.0
 */
class VideoPostType extends GenericPostType {
	
	//override
	public function meta_box_content(){
		global $post;
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
				
		$videoCode = '';
		$firstNo = '';
		$secondNo = '';
		$custom = get_post_meta($post->ID, $this->getPostSlug(), false);
		if(isset($custom[0]) && isset($custom[0]['videoCode'])){
			$videoCode = $custom[0]['videoCode'];
			if(isset($custom[0]['firstNumber']) && isset($custom[0]['secondNumber'])){
				$firstNo = $custom[0]['firstNumber'];
				$secondNo = $custom[0]['secondNumber'];
			}
		}
		?>
		
			<!--box-->	
			<div class="metaBoxContentBox" id="imgContainer">				
					<label class="customLabel">Enter iFrame video code ( make sure you choose 100% width videos)</label>
					<div class="hLine"></div>
					<textarea class="boxContentTextarea" name="<?php echo $this->getPostSlug();?>[videoCode]" rows="4"><?php echo $videoCode; ?></textarea>																																																				
			</div>
			<!--/box-->
			
			<!--box-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Optional - Set page number (ex first number: 01/ second number 02)</label>
					<div class="hLine"></div>				
					<div class="space01"></div>
					<label class="customLabel">First number</label><br />
					<input id="primaryTextColor" class="inputText" type="text" name="<?php echo $this->getPostSlug()?>[firstNumber]" value="<?php echo $firstNo;?>" /><br />
					<label class="customLabel">Second number</label><br />
					<input id="primaryTextColor" class="inputText" type="text" name="<?php echo $this->getPostSlug()?>[secondNumber]" value="<?php echo $secondNo;?>" />																																																		
			</div>
			<!--/box-->					
		
		<?php
	}

}

?>