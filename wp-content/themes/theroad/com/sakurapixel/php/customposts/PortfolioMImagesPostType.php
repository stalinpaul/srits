<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/customposts/GenericPostType.php');
/**
 * Portfolio (multiple images / item)
 */
class PortfolioMImagesPostType extends GenericPostType {
	
	public function meta_box_content(){
		global $post;
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
		$custom = get_post_meta($post->ID, $this->getPostSlug(), false);		
		$boxesData = array();
		if(isset($custom[0])){
			foreach ($custom[0] as $key => $value) {
				$boxData = array('data' => $value, 'box_key' => $key);
				array_push($boxesData, $boxData);
			}
		}		
		?>
		<div id="portfolioContainer">
			
			<?php
			if(isset($boxesData)){				
					for ($i=0;$i<sizeof($boxesData);$i++) {						
						$boxData = $boxesData[$i];
						$imgPreview = wp_get_attachment_image_src($boxData['data']['attachementID'], 'medium');
				        $itemHTML = '';
				        $itemHTML .= '<div id="'.$boxData['box_key'].'" class="metaBoxContentBox">';
				        $itemHTML .= '<div class="itemActionMenu">';
				        $itemHTML .= '<a class="button-secondary alignright removeBTN" href="#'.$boxData['box_key'].'">Remove</a>';
				        $itemHTML .= '<a class="button-secondary alignright moveDownBTN" href="#'.$boxData['box_key'].'">Move down</a>';
				        $itemHTML .= '<a class="button-secondary alignright moveUpBTN" href="#'.$boxData['box_key'].'">Move up</a>';
				        $itemHTML .= '<div class="clear-fx"></div>';
				        $itemHTML .= '</div>';
				        $itemHTML .= '<div class="portfolioImgPreview"><img src="'.$imgPreview[0].'" /></div>';
				        $itemHTML .= '<input class="attachementThumbURL" type="hidden" name="'.$this->getPostSlug().'['.$boxData['box_key'].'][attachementThumbURL]" value="'.$boxData['data']['attachementThumbURL'].'" />';
				        $itemHTML .= '<input class="attachementID" type="hidden" name="'.$this->getPostSlug().'['.$boxData['box_key'].'][attachementID]" value="'.$boxData['data']['attachementID'].'" />';						
				        $itemHTML .= '<a class="button-secondary uploadImageBTN" href="#'.$boxData['box_key'].'">Upload image</a>'; 
				        $itemHTML .= '</div>';
						echo $itemHTML;				
					}
				}
			
			?>
						
		</div>
		<a id="addItemBTN" class="button-primary">Add new image</a>
		<?php		
				
	}


	//add meta box video item
	public function addMetaBoxVideo($boxTitle){
		add_meta_box($this->settings['post_type'].'_boxID_videoitem', $boxTitle, array($this, 'meta_box_video'), $this->settings['post_type'], 'normal', 'high');
	}
	
	//is video metabox content
	public function meta_box_video(){
		global $post;
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
		$isVideoItemChecked = '';
		$videoCode = '';		
		$customVideo = get_post_meta($post->ID, $this->getPostSlug().'-videoItem', false);
		if(isset($customVideo[0])){
			$isVideoItem = (isset($customVideo[0]['isVideoItem']))?$isVideoItem=$customVideo[0]['isVideoItem']:$isVideoItem="";
			if($isVideoItem=="ON"){
				$isVideoItemChecked = "checked";
			}
			$videoCode = (isset($customVideo[0]['videoCode']))?$videoCode=$customVideo[0]['videoCode']:$videoCode="";
		}		
		?>
				<!--box-->
				<div class="metaBoxContentBox">	
					<label class="customLabel">Decide if show video instead of the above images</label>
					<div class="hLine"></div>						
					<input type="checkbox" name="<?php echo $this->getPostSlug().'-videoItem';?>[isVideoItem]" value="ON" <?php echo $isVideoItemChecked;?> />
					<label class="chBLabel">Show video (default Off)</label><br /><br />
					<label class="customLabel">Video iFrame code (youtube or vimeo)</label>
					<div class="hLine"></div>				
					<textarea class="boxContentTextarea" name="<?php echo $this->getPostSlug().'-videoItem';?>[videoCode]" rows="4"><?php echo $videoCode; ?></textarea>						
				</div>
				<!--end box-->		
		<?php		
	}


	
	//add meta box extra options
	public function addMetaBoxChooseType($boxTitle){
		add_meta_box($this->settings['post_type'].'_boxID_choosetype', $boxTitle, array($this, 'meta_box_content_choose'), $this->settings['post_type'], 'normal', 'high');
	}
	
	public function meta_box_content_choose(){
		global $post;
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
			
		$imageItemSelected = 'selected="selected"';
		$textItemSelected = '';
		$custom = get_post_meta($post->ID, $this->getPostSlug().'-chooseType', false);		
		$boxesData = array();
		if(isset($custom[0])){
			foreach ($custom[0] as $key => $value) {				
				switch ($value) {
					case 'imageItem':
						$imageItemSelected = 'selected="selected"';
						break;
					case 'textItem':
						$textItemSelected = 'selected="selected"';
						break;																			
					default:
						
						break;
				}
			}			
		}
		
		$size1Selected = 'selected="selected"';
		$size2Selected = '';
		$size3Selected = '';
		$size4Selected = '';
		
		$customSize = get_post_meta($post->ID, $this->getPostSlug().'-sizeType', false);		
		
		if(isset($customSize[0])){
			foreach ($customSize[0] as $key => $value) {				
				switch ($value) {
					case 'size1':
						$size1Selected = 'selected="selected"';
						break;
					case 'size2':
						$size2Selected = 'selected="selected"';
						break;
					case 'size3':
						$size3Selected = 'selected="selected"';
						break;
					case 'size4':
						$size4Selected = 'selected="selected"';
						break;																																					
					default:
						
						break;
				}
			}			
		}
		
		//excerpt
		$portfolioExcerpt = 100;
		$customExcerpt = get_post_meta($post->ID, $this->getPostSlug().'-excerpt', false);
		if(isset($customExcerpt[0])){
			$portfolioExcerpt = $customExcerpt[0]['excerpt'];
		}
		
		$customBuy = get_post_meta($post->ID, $this->getPostSlug().'-buy', false);
		$buyTitle = '';
		$buyLink = '';	
		if(isset($customBuy[0])){
			$buyTitle = wptexturize($customBuy[0]['title']);
			$buyLink = $customBuy[0]['link'];
		}
		
		$customPreview = get_post_meta($post->ID, $this->getPostSlug().'-preview', false);
		$previewTitle = '';	
		$previewLink = '';
		if(isset($customPreview[0])){
			$previewTitle = wptexturize($customPreview[0]['title']);
			$previewLink = $customPreview[0]['link'];
		}
		
		$recentProjectTitle = '';
		$customRecentProjectData = get_post_meta($post->ID, $this->getPostSlug().'-recentProject', false);
		if(isset($customRecentProjectData[0])){
			$recentProjectTitle = wptexturize($customRecentProjectData[0]['title']);
		}		
		//$recentProjectTitle							
			
		?>
			<!--box item preview type-->	
			<div class="metaBoxContentBox" id="imgContainer">				
					<label class="customLabel">Choose item preview type</label>
					<div class="hLine"></div>				

					<select name="<?php echo $this->getPostSlug().'-chooseType'?>[itemPreview]">
					  <option <?php echo $imageItemSelected;?> value="imageItem">Portfolio Image Item</option>
					  <option <?php echo $textItemSelected;?> value="textItem">Portfolio Text Item</option>
					</select>
					<p>You can choose a featured image from the right panel (Featured Image).</p>
					
					<div class="space01"></div>
					
					<label class="customLabel">Preview item (text) excerpt length</label><br />								
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-excerpt'?>[excerpt]" value="<?php echo $portfolioExcerpt;?>" /><br />																																																				
			</div>
			<!--/box item preview type-->
				
			
			<!--box-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Choose item preview size</label>
					<div class="hLine"></div>				

					<select class="selectUI" name="<?php echo $this->getPostSlug().'-sizeType'?>[itemPreviewSize]">
					  <option <?php echo $size1Selected;?> value="size1">Size 1 (300x160)</option>
					  <option <?php echo $size2Selected;?> value="size2">Size 2 (300x277)</option>
					  <option <?php echo $size3Selected;?> value="size3">Size 3 (300x126)</option>
					  <option <?php echo $size4Selected;?> value="size4">Size 4 (300x600)</option>
					</select>
					<p>For the text type preview item the height will adjust dynamically.</p>																																															
			</div>
			<!--/box-->
			
			<!--box-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Choose slider height</label>
					<div class="hLine"></div>				
					<?php
						//slider height
						$sliderHeight = 400;
						$customSliderData = get_post_meta($post->ID, $this->getPostSlug().'-slider', false);
						if(isset($customSliderData[0])){
							$sliderHeight = $customSliderData[0]['sliderHeight'];
						}					
					?>
					<label class="customLabel">Choose slider height for this portfolio item (insert value without px).</label><br />								
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-slider'?>[sliderHeight]" value="<?php echo $sliderHeight;?>" /><br />																																																		
			</div>
			<!--/box-->			
			
			<!--box-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Portfolio external links (ex: Buy/Preview link)</label>
					<div class="hLine"></div>				
					<label class="customLabel">Buy button label</label><br />								
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-buy'?>[title]" value="<?php echo $buyTitle;?>" /><br />
					<label class="customLabel">Buy button link</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-buy'?>[link]" value="<?php echo $buyLink;?>" /><br />
					
					<div class="hLine"></div>
					<label class="customLabel">Preview button label</label><br />								
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-preview'?>[title]" value="<?php echo $previewTitle;?>" /><br />
					<label class="customLabel">Preview button link</label><br />
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-preview'?>[link]" value="<?php echo $previewLink;?>" /><br />					
					
					<div class="space01"></div>						
					<p>Leave blank if you don't want to show the buttons.</p>																																															
			</div>
			<!--/box-->
			
			<!--box-->	
			<div class="metaBoxContentBox">				
					<label class="customLabel">Recent project subtitle</label>
					<div class="hLine"></div>				
					<label class="customLabel">Shows subtitle for recent project box</label><br />								
					<input class="inputText" type="text" name="<?php echo $this->getPostSlug().'-recentProject'?>[title]" value="<?php echo $recentProjectTitle;?>" /><br />
																																																			
			</div>
			<!--/box-->											
					
				
		<?php		
	}	

}


?>