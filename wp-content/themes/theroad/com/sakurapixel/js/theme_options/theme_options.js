$ = jQuery.noConflict();
$(document).ready(function(){
    try{
        var instance = new OptionsUtil();
        instance.cpicker();
    }catch(e){
        console.log(e);
    }
});

function OptionsUtil(){
    
    //upload loading logo
    var loadingLogoBox = $('#loadingLogoContainer');
    var uploadLoadingLogoBTN = loadingLogoBox.find('.uploadBTN');
    
    uploadLoadingLogoBTN.click(function(e){
        e.preventDefault();
        imageSelectedCallback = loadingLogoCallback;    
        tb_show('Upload loading logo image', 'media-upload.php?post_id=0&type=image&TB_iframe=true', false);  
        return false;        
    });
    var loadingLogoCallback = function(originalImage, thumbImage, imageID){
        loadingLogoBox.find('.imgUI').attr('src', thumbImage);
        loadingLogoBox.find('.imageAttachementID').val(imageID);       
    }
    //end upload loading logo    
    
    
    //logo upload
    var uploadLogoBox = $('#logoContainer');
    var uploadLogoBTN = uploadLogoBox.find('.uploadBTN');
    uploadLogoBTN.click(function(e){
        e.preventDefault();       
        imageSelectedCallback = uploadLogoCallback;    
        tb_show('Upload logo image', 'media-upload.php?post_id=0&type=image&TB_iframe=true', false);  
        return false;        
    });
    var uploadLogoCallback = function(originalImage, thumbImage, imageID){
        uploadLogoBox.find('.imgUI').attr('src', thumbImage);
        uploadLogoBox.find('.imageAttachementID').val(imageID);       
    }            
    //end logo upload
    
    
    //homepage background
    var homepageBackgroundBox = $('#homepageBackground');
    var homepageUploadBTN = homepageBackgroundBox.find('.uploadBTN');
    homepageUploadBTN.click(function(e){
        e.preventDefault();       
        imageSelectedCallback = homepageBackgroundCallback;    
        tb_show('Upload homepage background image', 'media-upload.php?post_id=0&type=image&TB_iframe=true', false);  
        return false;        
    });
    var homepageBackgroundCallback = function(originalImage, thumbImage, imageID){
        homepageBackgroundBox.find('.imgUI').attr('src', thumbImage);        
        homepageBackgroundBox.find('.imageAttachementID').val(imageID);       
    }    
    //end homepage background
    
    
    //menu settings
    menuCP();
    function menuCP(){
        $('#menuBackgroundColor, #menuTextColor, #menuTextShadowColor, #selectedMenuColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });
        
        jQuery('#resetMenuDefaultBTN').click(function(e){
            e.preventDefault();
            jQuery('#menuBackgroundColor').val('93abbf');
            jQuery('#menuTextColor').val('fffde8');
            jQuery('#menuTextShadowColor').val('000000');
            jQuery('#selectedMenuColor').val('7392ad');
        });       
    }
    //end menu settings
    
    
    
    //footer settings
    footerCP();
    function footerCP(){
        $('#footerBackgroundColor, #widgetsTitleColor, #footerLinksColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });
        jQuery('#resetfooterDefaultBTN').click(function(e){
            e.preventDefault();
            jQuery('#footerBackgroundColor').val('2f363e');
            jQuery('#widgetsTitleColor').val('ec761a');
            jQuery('#footerLinksColor').val('707070');
            footerBox.find('.imgUI').attr('src', '');        
            footerBox.find('.imageAttachementID').val('');            
        });
        
        var footerBox = $('#footerBox');
        var footerUploadBTN = footerBox.find('.uploadBTN');
        footerUploadBTN.click(function(e){
            e.preventDefault();       
            imageSelectedCallback = footerBackgroundCallback;    
            tb_show('Upload background image', 'media-upload.php?post_id=0&type=image&TB_iframe=true', false);  
            return false;        
        });
        var footerBackgroundCallback = function(originalImage, thumbImage, imageID){
            footerBox.find('.imgUI').attr('src', thumbImage);        
            footerBox.find('.imageAttachementID').val(imageID);       
        }                          
    }
    //end footer settings
    
    
    //portfolio settings
    portf_settings();
    function portf_settings(){
        $('#portfolioDefaultColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        }); 
        
        jQuery('#resetportfolioDefaultBTN').click(function(e){
            e.preventDefault();
            jQuery('#portfolioDefaultColor').val('ec761a');         
        });        
               
    }
    //end portfolio settings
    
    //blog settings
    blog_settings();
    function blog_settings(){
        $('#blogDefaultColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        }); 
        
        jQuery('#resetBlogDefaultBTN').click(function(e){
            e.preventDefault();
            jQuery('#blogDefaultColor').val('ec761a');         
        });        
               
    }
    //end blog settings    

    
    //contact settings    
    contact_settings();
    function contact_settings(){
        $('#conatctDefaultColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        }); 
        
        jQuery('#resetContactDefaultBTN').click(function(e){
            e.preventDefault();
            jQuery('#conatctDefaultColor').val('ec761a');         
        });        
               
    }    
    //end contact settings

    window.send_to_editor = function(html) {
        var originalImage = $(html).attr('href');
        var thumbImage = $(html).find('img').attr('src');
        var imgid = $(html).find('img').attr('class');
        var imageID = imgid.slice( ( imgid.search(/wp-image-/) + 9 ), imgid.length );
        imageSelectedCallback(originalImage, thumbImage, imageID);
        tb_remove();  
    }
    
    this.cpicker = function(){        
        $('#primaryTextColor, #secondaryTextColor, #thirdTextColor, #mainBackgroundColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });
    }
    
    //reset theme colors
    $('#resetColorsBTN').click(function(e){
        e.preventDefault();
        $('#primaryTextColor').val('FFFFFF');
        $('#secondaryTextColor').val('e8e8e8');
        $('#thirdTextColor').val('ec1c4d');
        $('#mainBackgroundColor').val('000000');
    });              
}
