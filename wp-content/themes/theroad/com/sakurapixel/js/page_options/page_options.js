
jQuery(document).ready(function(){
    new PageOptions();
});
function PageOptions(){
    
    pagecolor();
    function pagecolor(){
        jQuery('#pageBackgroundColor, #pageOverlayColor, #pageTitleColor, #pageSubTitleColor, #pagetextColor, #pageHeaderLinesColor, #pageTitleShadowColor').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                jQuery(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            jQuery(this).ColorPickerSetColor(this.value);
        });
    }
    
    //reset default options
    jQuery('#resetDefaultOptionsBTN').click(function(e){
        e.preventDefault();
        jQuery('#pageSubtitle').val('page subtitle...');
        jQuery('#pageTitleColor').val('6e6a75');
        jQuery('#pageTitleShadowColor').val('ffffff');              
        jQuery('#pageSubTitleColor').val('9b9b9b');
        jQuery('#pageHeaderLinesColor').val('cccccc');        
        jQuery('#pagetextColor').val('5e5a62');
    });
    
    //reset background button
    jQuery('#resetBackgroundBTN').click(function(e){
        e.preventDefault();
        jQuery('#pageBackgroundColor').val('e9e9e9');
        jQuery('#pageOverlayColor').val('');
        jQuery('.imgUI').attr('src', '');         
        jQuery('#pageBackgroundAttachementID').val('');        
    }); 
    
    
    //upload background image
    
    jQuery('.uploadBTN').click(function(e){
        e.preventDefault();
        uploadImageHandler();
    });    
    
    window.restore_send_to_editor = window.send_to_editor;
    var imageSelectedCallback;
    function uploadImageHandler(){     
        window.send_to_editor = backgroundImageUploadCallback;
        //imageSelectedCallback = portfolioImageUploadCallback;
        tb_show('Upload background image', 'media-upload.php?post_id=0&type=image&TB_iframe=true', false);  
        return false;            
    }
    
    //catch background upload
    var backgroundImageUploadCallback = function(html){  
        var originalImage = jQuery(html).attr('href');
        var thumbImage = jQuery(html).find('img').attr('src');
        var imgid = jQuery(html).find('img').attr('class');
        var imageID = imgid.slice( ( imgid.search(/wp-image-/) + 9 ), imgid.length );
        tb_remove();
        window.send_to_editor = window.restore_send_to_editor;
                      
         jQuery('.imgUI').attr('src', thumbImage);         
         jQuery('#pageBackgroundAttachementID').val(imageID);
    }        
    
}
