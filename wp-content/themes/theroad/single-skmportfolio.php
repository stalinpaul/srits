<?php
$view->checkPreview();
require_once(CLASS_PATH.'/com/sakurapixel/php/libs/sk_resizer.php');
							//is video
							$isVideo = false;
							$videoCode = '';
							$customVideo = get_post_meta($post->ID, 'skmportfolio-videoItem', false);
							if(isset($customVideo[0])){
								$isVideoItem = (isset($customVideo[0]['isVideoItem']))?$isVideoItem=$customVideo[0]['isVideoItem']:$isVideoItem="";
								if($isVideoItem=="ON"){
									$isVideo = true;
								}
								$videoCode = (isset($customVideo[0]['videoCode']))?$videoCode=$customVideo[0]['videoCode']:$videoCode="";
							}
							//end is video
							
							
							$customBuy = get_post_meta($post->ID, 'skmportfolio-buy', false);
							$buyTitle = '';
							$buyLink = '';	
							if(isset($customBuy[0])){
								$buyTitle = $customBuy[0]['title'];
								$buyLink = $customBuy[0]['link'];
							}
							
							$sliderHeight = 400;
							$customSliderData = get_post_meta($post->ID, 'skmportfolio-slider', false);														
							if(isset($customSliderData[0])){
								$sliderHeight = $customSliderData[0]['sliderHeight'];
							}						
							
							$customPreview = get_post_meta($post->ID, 'skmportfolio-preview', false);
							$previewTitle = '';	
							$previewLink = '';
							if(isset($customPreview[0])){
								$previewTitle = $customPreview[0]['title'];
								$previewLink = $customPreview[0]['link'];
							}							
							
								
							$custom = get_post_meta(get_the_ID(), 'skmportfolio', false);
							//parse result							
							$groupsData = array();
							if(isset($custom[0])){
								$customData = $custom[0];
								foreach ($customData as $key => $value){
									$imageSmall = wp_get_attachment_image_src($value['attachementID'], 'skmportfolio-thumb');
									$imageLarge = wp_get_attachment_image_src($value['attachementID'], 'skmportfolio-large');
									$imageFull = wp_get_attachment_image_src($value['attachementID'], 'full');
									
									$slider_image_url = sk_resize($imageFull[0], 884, $sliderHeight, true);
									$slider_image_url = ($slider_image_url)?$slider_image_url:'http://placehold.it/350x150';
										
									$groupData = array('imageSmall'=>$imageSmall, 'imageLarge'=>$slider_image_url, 'imageFull'=>$imageFull);
									array_push($groupsData, $groupData);								
								}
				                $largeImages = '';
								$thumImages = '';
								
				                for ($i=0; $i < sizeof($groupsData); $i++) {				                	
				                    $largeImages .= '<li data-full="'.$groupsData[$i]['imageFull'][0].'"><img src="'.$groupsData[$i]['imageLarge'].'" alt="" /></li> ';
									$thumImages .= '<li><a href="#"></a><img src="'.$groupsData[$i]['imageSmall'][0].'" alt="" /></li>';
				                }
							}														
?>


        <!--ei-slider container-->

        	<?php if(isset($custom[0])||$isVideo==true):?>
        	
        	<!--sixteen columns-->	
            <div class="sixteen columns">
                <?php
                	$previousURL = get_permalink(get_adjacent_post(false,'',false));
					$nextURL = get_permalink(get_adjacent_post(false,'',true));
					$enlargePoz = -1; 
                ?>
                <div class="singleProjTop" data-p_title="<?php echo get_the_title(get_the_ID());?>">
                	<a href="#" class="genericBoxButton portfolioButtonColor floatLeft closeBTN"><img src="<?php echo IMAGES.'/close.png';?>" /></a>
                	<?php if($nextURL!="" && isset($nextURL) && $nextURL!=get_permalink(get_the_ID())):?>
                		<a href="<?php $enlargePoz+=-27; echo $nextURL;?>" class="genericBoxButton portfolioButtonColor floatRight nextBTN" style="margin-left: 1px;"><img src="<?php echo IMAGES.'/right.png';?>" /></a>
                	<?php endif;?>
                	<?php if($previousURL!="" && isset($previousURL) && $previousURL!=get_permalink(get_the_ID())):?>
                		<a href="<?php $enlargePoz+=-27; echo $previousURL;?>" class="genericBoxButton portfolioButtonColor floatRight prevBTN"><img src="<?php echo IMAGES.'/left_arrow.png';?>" /></a>
                	<?php endif;?>
                	<?php if(!$isVideo):?>
                	<a href="#" class="genericBoxButton portfolioButtonColor floatRight enlargeBTN" style="position: relative; top: 28px; margin-right: <?php echo $enlargePoz;?>px;"><img src="<?php echo IMAGES.'/enlarge.png';?>" /></a>
                	<?php endif;?>               	
                </div>
                
                <div class="clear-fx"></div>
                
    
                               
                <div style="padding: 0px 28px;">
                	
                	<!--sliderx-->
                	<?php if(!$isVideo):?>                                             	                
	                <div data-sliderHeight="<?php echo $sliderHeight;?>" id="sliderX" class="sliderX">
	                    <ul class="sliderx-large">
	                        <?php 
	                        	echo $largeImages;
	                        ?>                                                                                                                                                                                                
	
	                    </ul>
	                    <ul class="sliderx-thumbs">
	                        <?php 
	                        	echo $thumImages;
	                        ?>                                                                 
	                    </ul>
	                </div>	                
	                <?php endif;?>
	                <!--/sliderx--> 
	                
	                
	                <!--video-->
	                <?php if($isVideo):?>
	                <div class="videoProject">
	                	<?php echo $videoCode;?>
	                </div>
	                <?php endif;?>
	                <!--/video-->
	                
	                	               
                </div>    
           
                
            </div>         
        <!--/sixteen columns-->
        <?php endif;?>
        


<!--portfolio item content-->
<div class="sixteen columns">
	<!--buy/preview buttons-->
	<div class="buyNowContainer">
		<?php $margin='style="margin-right: 25px;"';?>
		<?php if($buyTitle!=''):?>
			<a target="_blank" href="<?php echo $buyLink;?>" class="genericButton <?php echo $view->getPortfolioButtonStyle();?> alignright" style="margin-right: 25px; margin-left: 15px;"><?php echo $buyTitle;?></a>
			<?php $margin='';?>
		<?php endif;?>
		<?php if($previewTitle!=''):?>
		<a target="_blank" href="<?php echo $previewLink;?>" class="genericButton <?php echo $view->getPortfolioButtonStyle();?> alignright" <?php echo $margin;?>><?php echo $previewTitle;?></a>
		<?php endif;?>
		<div class="clear-fx"></div>		
	</div>
	<!--/buy/preview buttons-->	
	<div class="portfoliItemCOntentWrapper" style="padding: 0px 28px; margin-top: -30px;">
		<div class="portfoliItemCOntent defaultText contentTextColor editorContent" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<p class="portfolioOpenItemTitle portfolioMainColor"><?php echo get_the_title(get_the_ID());?></p>	
						<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>           	
						          	<?php
						                  the_content();						
						            ?> 
						<?php endwhile; else: ?>
						<p><?php _e('No content was found. Sorry!', 'default_textdomain'); ?></p>
						<?php endif; ?>     
						<?php wp_reset_query(); ?>	
		</div>
		<div class="clear-fx"></div>
	</div>
</div>		
<!--portfolio item content-->