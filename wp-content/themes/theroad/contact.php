<?php
/*
Template Name: Contact Template
*/
?>
<?php
$view->checkPreview();
$isPageTitle = $view->isPageTitle(get_the_ID());
$removeTitle = (!$isPageTitle)?"removeElement":"";
?>
<div class="slide" id="<?php echo uniqid('_slide');?>" data-backurl="<?php echo $view->getBackgroundURL(get_the_ID());?>" data-iscover="<?php echo $view->isPageBackgroundCover(get_the_ID());?>" data-stellar-background-ratio="0.2" data-type="contactPage" style="<?php echo $view->getPageBackground(get_the_ID());?>;background-position: center top;">
	
	<div class="slideBackgroundOverlay" style="<?php echo $view->getPageOverlayColor(get_the_ID())?>"></div>
		
		<!--slide wrapper-->
		<div class="slideWrapper container">
			
			<div>
				<div class="sixteen columns">
					
					<!--slide header-->
					<div class="slideHeader">
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
						<div class="slideHeaderLineContent">
							<p class="slideTitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageTitleStyle(get_the_ID());?>"><?php echo get_the_title(get_the_ID());?></p>
							<p class="slideSubtitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>">{ <?php echo $view->getPageSubTitle(get_the_ID());?> }</p>
						</div>
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
					</div>
					<!--/slide header-->
										
				</div>
				
				<div class="clear-fx"></div>	
				
				<!--slide content-->
				<div class="slideContent">
										
					<!--google map-->
					<div class="row">
						<div class="sixteen columns">
							<p class="contactInnerSectionTitle contactHeadersColor"><?php _e('visit us', 'default_textdomain');?></p>
							<div class="mapContainer">
			                    <?php
									$themeOptions = get_option('themeOptions');
									$gmap = (isset($themeOptions['mapEmbedded']))?$themeOptions['mapEmbedded']:'';
									echo $gmap;                    
			                    ?>							
							</div>
						</div>
					</div>
					<!--/google map-->
					
					<div class="clear-fx"></div>
					
					<!--contact content-->
					<div class="row">
						<!--contact info-->												
		<!--				<div class="one-third column">
							<p class="contactInnerSectionTitle contactHeadersColor"><?php _e('contact info', 'default_textdomain');?></p>
							<div class="defaultText" id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php echo $view->getPageTextStyle(get_the_ID());?>">
								<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>           	
								          	<?php
								                  the_content();						
								            ?> 
								<?php endwhile; else: ?>
								<p><?php _e('Please add contact info from contact page editor!', 'default_textdomain'); ?></p>
								<?php endif; ?>     
								<?php wp_reset_query(); ?>						
							</div>
						</div> -->
						<!--/contact info-->
						
						<!--contact form-->
						<div class="two-thirds column">
							<p class="contactInnerSectionTitle contactHeadersColor"><?php _e('message us', 'default_textdomain');?></p>
							<div class="formContainer">
								<form class="contactForm form-horizontal">
									<div class="formGroup">
			                              <div id="name" class="inputField control-group">
			                                <input type="text" class="input-small defaultText" placeholder="<?php _e('Name', 'default_textdomain')?>" />
			                              </div>
			                              <div id="email" class="inputField control-group">
			                                <input type="text" class="input-small defaultText" placeholder="<?php _e('Email', 'default_textdomain')?>" />
			                              </div>
			                              <div id="website" class="inputField control-group" style="margin-right: 0px;">
			                                <input type="text" class="input-small defaultText" placeholder="<?php _e('Website', 'default_textdomain')?>" />
			                              </div>		                              		                              									
									</div>
									<p class="formThankYou defaultText" style="<?php echo $view->getPageTextStyle(get_the_ID());?>"><?php _e('Thank you! Your message has been sent.', 'default_textdomain')?></p>
		                            <div class="formGroup">
		                                <div id="message" class="inputTextArea control-group">
		                                  <textarea id="txt" class="commentTextArea field defaultText" rows="6" placeholder="<?php _e('Type your message here...', 'default_textdomain')?>"></textarea>
		                                </div>
		                            </div>
		                            
		                         	 <div class="formGroup">
		                          	    <button id="sendBTN" type="submit" class="genericButton <?php echo $view->getContactButtonStyle();?>"><?php _e('Send', 'default_textdomain')?></button>
		                              	<div class="clear-fx"></div>                               
		                          	 </div>                                                                                                          
	                            </form>                     
	                          	 <div class="clear-fx"></div>	                            								
							</div>
						</div>
						<!--/contact form-->
					</div>
					<!--/contact content-->
				
				</div>
				<!--/slide content-->
				
				<div class="clear-fx"></div>	
				
				<div class="sixteen columns">	
					<!--slide footer-->
					<div class="slideFooter">
						<a style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>" href="#" class="slideGoTop alignright">{ <?php _e('go to top', 'default_textdomain');?> }</a>
						<div class="clear-fx"></div>
					</div>
					<!--/slide footer-->
				</div>					
					
				
			</div>
			
		</div>
		<!--/slide wrapper-->
	
	
</div>
