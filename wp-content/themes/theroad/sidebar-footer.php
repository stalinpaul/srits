<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/sidebars/SidebarManager.php');
?>
<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar(SidebarManager::getInstance()->getSidebarName('sk-footer-sidebar')) ) : ?>
	<!--no widgets added to social sidebar-->
	<p class="defaultText" style="color: #FFF;"><?php _e('In order to add widgets please read documentation around "Sidebars and Widgets"', 'default_textdomain')?></p>
<?php endif; ?>