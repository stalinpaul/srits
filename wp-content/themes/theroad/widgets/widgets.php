<?php


/**
 * Social widget
 */
class SKSocialWidget extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'sk_social_widget',
			'The Road: Social Widget',
			array('description'=>'Displays a social link/icon to the homepage. Please make sure images are 31x31px. Only add this widget type to Homepage Social links sidebar')
		);		
	}
	
	public function form($instance){		
		$url = (isset($instance['url' ])) ? $instance['url'] : 'http://socialnewtwork';
		$imageURL = (isset($instance['imageURL' ])) ? $instance['imageURL'] : 'http://image_url.png';		
		?>
			<p>
			    <input id="<?php echo $this->get_field_id( 'url'); ?>" name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php echo esc_attr($url); ?>" />
			    <label for="<?php echo $this->get_field_id('url');?>">Link</label>
			    <br />
			    <input id="<?php echo $this->get_field_id( 'imageURL'); ?>" name="<?php echo $this->get_field_name('imageURL'); ?>" type="text" value="<?php echo esc_attr($imageURL); ?>" />
			    <label for="<?php echo $this->get_field_id('imageURL');?>">Image URL</label>			    								
			</p>		
		<?php		
	}
	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['url'] = urldecode($new_instance['url']);
		$instance['imageURL'] = urldecode($new_instance['imageURL']);
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);				
		$url = (isset($instance['url']))?$instance['url']:'';
		$imageURL = (isset($instance['imageURL']))?$instance['imageURL']:'';
		
		//http://localhost:8888/temp/facebook_icon.png
		echo $before_widget;
			echo '<a class="normalTip exampleTip" href="'.$url.'" target="_blank"><img src="'.$imageURL.'" alt="" /></a>';
		echo $after_widget;
	}

}


/**
 * Footer link widget
 */
class SKFooterLinkWidget extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'sk_footerlink_widget',
			'The Road: Footer Link Widget',
			array('description'=>'Displays a link to the footer. Only add this widget type to the Footer Links Sidebar ')
		);		
	}
	
	public function form($instance){
		$linkName = (isset($instance['linkName' ])) ? $instance['linkName'] : 'Link name';
		$url = (isset($instance['url' ])) ? $instance['url'] : 'http://yourlink.com';				
		?>
			<p>
				<input id="<?php echo $this->get_field_id( 'linkName'); ?>" name="<?php echo $this->get_field_name('linkName'); ?>" type="text" value="<?php echo esc_attr($linkName); ?>" />
			    <label for="<?php echo $this->get_field_id('linkName');?>">Link name</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'url'); ?>" name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php echo esc_attr($url); ?>" />
			    <label for="<?php echo $this->get_field_id('url');?>">Link</label>		    								
			</p>		
		<?php		
	}
	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['linkName'] = strip_tags($new_instance['linkName']);
		$instance['url'] = urldecode($new_instance['url']);
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);				
		$linkName = (isset($instance['linkName']))?$instance['linkName']:'Link name';
		$url = (isset($instance['url']))?$instance['url']:'';		
		
				
		echo $before_widget;			
			echo '<a target="_blank" href="'.$url.'">'.$linkName.'</a>';
		echo $after_widget;
	}

}


/**
 * Twitter widget
 */
class SKTwitterWidget extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'sk_twitter_widget',
			'The road: Twitter Widget',
			array('description'=>'Displaying tweets, you can add this widget to the Footer Sidebar')
		);		
	}
	
	public function form($instance){		
		$title = (isset($instance['title' ])) ? $instance['title'] : 'Widget title';
		?>
			<p>
				<input id="<?php echo $this->get_field_id( 'title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			    <label for="<?php echo $this->get_field_id('title');?>">Title</label>						    			    			    			    					    						    								
			</p>
			<p>Twiter patch! Twitter has changed it's API, @see Theme Options for the new settings.</p>		
		<?php		
	}
	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;		
		$instance['title'] = strip_tags($new_instance['title']);		
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);					
		$title = apply_filters('widget_title', $instance['title']);											

		echo $before_widget;
			if($title){
				echo $before_title.$title.$after_title;
			}		
			echo '<div id="t_feed_ui"></div>';
		echo $after_widget;
	}

}



/**
 * Flickr widget
 */
class SKFlickrWidget extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'sk_flickr_widget',
			'The Road: Flickr Widget',
			array('description'=>'Displaying flickr images, you can add this widget to the Footer Sidebar.')
		);		
	}
	
	public function form($instance){
		$username = (isset($instance['username' ])) ? $instance['username'] : 'username';
		$title = (isset($instance['title' ])) ? $instance['title'] : 'Widget title';
		$thumbsNo = (isset($instance['thumbsNo' ])) ? $instance['thumbsNo'] : 6;
		$api_key = (isset($instance['api_key' ])) ? $instance['api_key'] : '';
		?>
			<p>
				<input id="<?php echo $this->get_field_id( 'title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			    <label for="<?php echo $this->get_field_id('title');?>">Title</label>				
			    <br />				
				<input id="<?php echo $this->get_field_id( 'username'); ?>" name="<?php echo $this->get_field_name('username'); ?>" type="text" value="<?php echo esc_attr($username); ?>" />
			    <label for="<?php echo $this->get_field_id('username');?>">Username</label>
			    <br />				
				<input id="<?php echo $this->get_field_id( 'thumbsNo'); ?>" name="<?php echo $this->get_field_name('thumbsNo'); ?>" type="text" value="<?php echo esc_attr($thumbsNo); ?>" />
			    <label for="<?php echo $this->get_field_id('thumbsNo');?>">Thumbs no</label>
			    <br />				
				<input id="<?php echo $this->get_field_id( 'api_key'); ?>" name="<?php echo $this->get_field_name('api_key'); ?>" type="text" value="<?php echo esc_attr($api_key); ?>" />
			    <label for="<?php echo $this->get_field_id('api_key');?>">API key</label>			    					    						    								
			</p>		
		<?php		
	}
	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['username'] = strip_tags($new_instance['username']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['thumbsNo'] = strip_tags($new_instance['thumbsNo']);
		$instance['api_key'] = strip_tags($new_instance['api_key']);
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);					
		$title = apply_filters('widget_title', $instance['title']);
		$username = (isset($instance['username']))?$instance['username']:'';
		$thumbsNo = (isset($instance['thumbsNo']))?$instance['thumbsNo']:3;
		$api_key = (isset($instance['api_key']))?$instance['api_key']:'';					
		

		echo $before_widget;
			if($title){
				echo $before_title.$title.$after_title;
			}		
			echo '<div class="flickrContainer" data-api="'.$api_key.'" data-username="'.$username.'" data-thumbsNo="'.$thumbsNo.'"></div>';
		echo $after_widget;
	}
}




/**
 * Usefull links widget
 */
class SKUsefullFooterLinkWidget extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'sk_usefulfooterlink_widget',
			'The Road: Useful Links Widget',
			array('description'=>'Displays useful links to the footer sidebar. Add this widget type to the Footer Sidebar ')
		);		
	}
	
	public function form($instance){
		$title = (isset($instance['title' ])) ? $instance['title'] : 'Widget title';
		$linkName1 = (isset($instance['linkName1' ])) ? $instance['linkName1'] : 'Link name';
		$url1 = (isset($instance['url1' ])) ? $instance['url1'] : 'http://yourlink.com';
		$linkName2 = (isset($instance['linkName2' ])) ? $instance['linkName2'] : 'Link name';
		$url2 = (isset($instance['url2' ])) ? $instance['url2'] : 'http://yourlink.com';
		$linkName3 = (isset($instance['linkName3' ])) ? $instance['linkName3'] : 'Link name';
		$url3 = (isset($instance['url3' ])) ? $instance['url3'] : 'http://yourlink.com';
		$linkName4 = (isset($instance['linkName4' ])) ? $instance['linkName4'] : 'Link name';
		$url4 = (isset($instance['url4' ])) ? $instance['url4'] : 'http://yourlink.com';										
		?>
			<p>
				<input id="<?php echo $this->get_field_id( 'title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			    <label for="<?php echo $this->get_field_id('title');?>">Title</label>					    								
			</p>		
			<p>
				<input id="<?php echo $this->get_field_id( 'linkName1'); ?>" name="<?php echo $this->get_field_name('linkName1'); ?>" type="text" value="<?php echo esc_attr($linkName1); ?>" />
			    <label for="<?php echo $this->get_field_id('linkName1');?>">Link name</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'url1'); ?>" name="<?php echo $this->get_field_name('url1'); ?>" type="text" value="<?php echo esc_attr($url1); ?>" />
			    <label for="<?php echo $this->get_field_id('url1');?>">URL</label>		    								
			</p>
			<p>
				<input id="<?php echo $this->get_field_id( 'linkName2'); ?>" name="<?php echo $this->get_field_name('linkName2'); ?>" type="text" value="<?php echo esc_attr($linkName2); ?>" />
			    <label for="<?php echo $this->get_field_id('linkName2');?>">Link name</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'url2'); ?>" name="<?php echo $this->get_field_name('url2'); ?>" type="text" value="<?php echo esc_attr($url2); ?>" />
			    <label for="<?php echo $this->get_field_id('url2');?>">URL</label>		    								
			</p>
			<p>
				<input id="<?php echo $this->get_field_id( 'linkName3'); ?>" name="<?php echo $this->get_field_name('linkName3'); ?>" type="text" value="<?php echo esc_attr($linkName3); ?>" />
			    <label for="<?php echo $this->get_field_id('linkName3');?>">Link name</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'url3'); ?>" name="<?php echo $this->get_field_name('url3'); ?>" type="text" value="<?php echo esc_attr($url3); ?>" />
			    <label for="<?php echo $this->get_field_id('url3');?>">URL</label>		    								
			</p>
			<p>
				<input id="<?php echo $this->get_field_id( 'linkName4'); ?>" name="<?php echo $this->get_field_name('linkName4'); ?>" type="text" value="<?php echo esc_attr($linkName4); ?>" />
			    <label for="<?php echo $this->get_field_id('linkName4');?>">Link name</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'url4'); ?>" name="<?php echo $this->get_field_name('url4'); ?>" type="text" value="<?php echo esc_attr($url4); ?>" />
			    <label for="<?php echo $this->get_field_id('url4');?>">URL</label>		    								
			</p>											
		<?php		
	}
	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['linkName1'] = strip_tags($new_instance['linkName1']);
		$instance['url1'] = urldecode($new_instance['url1']);
		$instance['linkName2'] = strip_tags($new_instance['linkName2']);
		$instance['url2'] = urldecode($new_instance['url2']);
		$instance['linkName3'] = strip_tags($new_instance['linkName3']);
		$instance['url3'] = urldecode($new_instance['url3']);
		$instance['linkName4'] = strip_tags($new_instance['linkName4']);
		$instance['url4'] = urldecode($new_instance['url4']);						
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);
		$title = (isset($instance['title']))?$instance['title']:'Widget Title';				
		$linkName1 = (isset($instance['linkName1']))?$instance['linkName1']:'';
		$url1 = (isset($instance['url1']))?$instance['url1']:'';		
		$linkName2 = (isset($instance['linkName2']))?$instance['linkName2']:'';
		$url2 = (isset($instance['url2']))?$instance['url2']:'';		
		$linkName3 = (isset($instance['linkName3']))?$instance['linkName3']:'';
		$url3 = (isset($instance['url3']))?$instance['url3']:'';
		$linkName4 = (isset($instance['linkName4']))?$instance['linkName4']:'';
		$url4 = (isset($instance['url4']))?$instance['url4']:'';								
				
        	require_once(CLASS_PATH.'/com/sakurapixel/php/stylehelper.php');
			$stH = new StyleHelper();
			$footerColors = $stH->getFooterColors();
							
		echo $before_widget;
		if($title){
			echo $before_title.$title.$after_title;
		}		
		echo '<ul class="usefullLinksList">';			
			if($linkName1!=""){
				echo '<li>';
				echo '<a target="_blank" href="'.$url1.'">'.$linkName1.'</a>';
				echo '</li>';
				echo '<li class="slideHeaderLine footerLinesColor"></li>';				
			}
			if($linkName2!=""){
				echo '<li>';
				echo '<a target="_blank" href="'.$url2.'">'.$linkName2.'</a>';
				echo '</li>';
				echo '<li class="slideHeaderLine footerLinesColor"></li>';
			}
			if($linkName3!=""){
				echo '<li>';
				echo '<a target="_blank" href="'.$url3.'">'.$linkName3.'</a>';
				echo '</li>';
				echo '<li class="slideHeaderLine footerLinesColor"></li>';
			}
			if($linkName4!=""){
				echo '<li>';
				echo '<a target="_blank" href="'.$url4.'">'.$linkName4.'</a>';
				echo '</li>';
			}						
		echo '</ul><div class="clear-fx"></div>';
		echo $after_widget;
	}

}



/**
 * register widgets
 */
function skregister_widgets(){	
	register_widget('SKSocialWidget');
	register_widget('SKFooterLinkWidget');
	register_widget('SKTwitterWidget');	
	register_widget('SKFlickrWidget');
	register_widget('SKUsefullFooterLinkWidget');
}
add_action('widgets_init', 'skregister_widgets');
 
 
?>