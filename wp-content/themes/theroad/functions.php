<?php
//define global constants
define( 'TEMPPATH', get_stylesheet_directory_uri());
define( 'IMAGES', TEMPPATH. "/images");
define( 'JS', TEMPPATH. "/js");

define( 'JS_ADMIN', TEMPPATH. "/com/sakurapixel/js");
define( 'CSS_ADMIN', TEMPPATH. "/com/sakurapixel/css");
define( 'IMAGES_ADMIN', TEMPPATH. "/com/sakurapixel/images");
define('CLASS_PATH', realpath(dirname(__FILE__)));


require_once(CLASS_PATH.'/com/sakurapixel/php/hive_wrapper.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/FrontView.php');
define('TWITTER_SERVICE_FEED', TEMPPATH.'/com/sakurapixel/php/twitter/get-tweets1.1.php');

// Translation
//load_theme_textdomain('default_textdomain', TEMPPATH.'/languages');

// Translation
add_action('after_setup_theme', 'sk_theme_setup');
function sk_theme_setup(){		
	$txtD = load_theme_textdomain('default_textdomain', get_template_directory() . '/languages');		    
}


//init framework
$hive = new HiveWrapper();
$hive->start();
$view = new FrontView($hive);


?>