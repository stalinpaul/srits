<?php
/*
Template Name: Blog Template
*/
?>
<?php
require_once(CLASS_PATH.'/com/sakurapixel/php/utils/excerpt/Excerpt.php');
require_once(CLASS_PATH.'/com/sakurapixel/php/utils/blogutils/BlogPagination.php');
$view->checkPreview();
$isPageTitle = $view->isPageTitle(get_the_ID());
$removeTitle = (!$isPageTitle)?"removeElement":"";
function the_titlesmall($post, $limit) {

	if (strlen($post->post_title) > $limit) {
	echo substr(the_title($before = '', $after = '', FALSE), 0, $limit) . '...'; } else {
	the_title();
	} 

}
?>
<div class="slide" id="<?php echo uniqid('_slide');?>" data-backurl="<?php echo $view->getBackgroundURL(get_the_ID());?>" data-iscover="<?php echo $view->isPageBackgroundCover(get_the_ID());?>" data-stellar-background-ratio="0.2" data-type="blogPage" style="<?php echo $view->getPageBackground(get_the_ID());?>;background-position: center top;">
	
	<div class="slideBackgroundOverlay" style="<?php echo $view->getPageOverlayColor(get_the_ID())?>"></div>
		
		<!--slide wrapper-->
		<div class="slideWrapper container">
			
			<div>
				
				<!--sixteen columns-->
				<div class="sixteen columns">					
					<!--slide header-->
					<div class="slideHeader">
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
						<div class="slideHeaderLineContent">
							<p class="slideTitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageTitleStyle(get_the_ID());?>"><?php echo get_the_title(get_the_ID());?></p>
							<p class="slideSubtitle <?php echo $removeTitle;?>" style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>">{ <?php echo $view->getPageSubTitle(get_the_ID());?> }</p>
						</div>
						<div class="slideHeaderLine headerLineColor <?php echo $removeTitle;?>" style="<?php echo $view->getPageHeaderLinesStyle(get_the_ID());?>"></div>
					</div>
					<!--/slide header-->
				</div>
				<!--end sixteen columns-->
					
				<div class="clear-fx"></div>
				
					
				<!--blog posts list-->
				<div class="slideContent">
					
					<!--blog page content-->
					<div class="blogPageContent">
						<div class="innerPageContent">
						
				        <?php query_posts('paged='.get_query_var('paged').'&post_type=post&post_status=publish&posts_per_page=3'); ?>
						  <?php if( have_posts() ): ?>
						        <?php while( have_posts() ): the_post(); ?>					
								<?php
									
									$id_box = uniqid('ids_box');								
									$post_thumbnail_id = get_post_thumbnail_id($post->ID);
									$feat_image = wp_get_attachment_image_src($post_thumbnail_id, 'blog-featured');							
									if(!isset($feat_image[0])){
										$feat_image = "http://placehold.it/262x170";
									}else{
										$feat_image = $feat_image[0];
									}
									$the_day = get_the_date( 'd', $post );
									$the_month = get_the_date( 'M', $post );
									$the_year = get_the_date( 'y', $post );								
								?>
						<!--post box-->
						<div class="one-third column oneThirdBlog">
							<div class="postBox" id="<?php echo $id_box;?>">
								<div class="blogboxWrapper">
									<a class="aBlogFeaturedImg" data-boxid="<?php echo $id_box;?>" href="<?php the_permalink(); ?>"><img class="blogFeaturedImg" src="<?php echo $feat_image;?>" /></a>
									<img class="blogPImageShadow" src="<?php echo IMAGES.'/blog_post_shadow.png'?>" />
									<div class="blogExcerptContainer defaultText" style="<?php echo $view->getPageTextStyle(get_the_ID());?>">
										<?php
											
										?>
										<?php echo Excerpt::length(62); ?>
									</div>								
								</div>
								<div class="blogPFooter">
									<div class="blogFooterFirst">
										<p class="blogPDay blogMainColor"><?php echo $the_day;?></p>
										<p class="blogPMonth"><?php echo $the_month;?> '<?php echo $the_year;?></p>
										<ul class="blogCommentsBox">
											<li><a><?php echo get_comments_number(get_the_ID());?></a></li>
											<li><a><img style="height: 19px; margin-left: 3px;" src="<?php echo IMAGES.'/comments_icon01.png'?>" /></a></li>										
										</ul>
										<div class="clear-fx"></div>									
									</div>
									<div class="blogFooterSecond">
										<a href="<?php the_permalink(); ?>" class="blogBoxTitle blogMainColor"><?php the_titlesmall($post, 15);?></a>
										<a href="<?php the_permalink(); ?>" class="blogReadMore"><?php _e('Read more...', 'default_textdomain');?></a>
									</div>
									<div class="clear-fx"></div>
								</div>
							</div>
						</div>
						<!--/post box-->
								
								<?php endwhile; ?>
								
						        <!--page nav-->
						        <div class="sixteen columns">
							        <div class="blogPagination">
							        	<?php 
							        		$pagination = new BlogPagination();
											$pagination->kriesi_pagination("", 2);					        							        	
							        	?>
							        </div>
						        </div>					        
						        <!--page nav-->
						<div class="clear-fx"></div>
						
						
						
						<!--open blog post-->
						<div class="sixteen columns openedBlogPost" style="position: relative">														
							
						</div>
						<!--/open blog post-->
						
											
						
						        
						 <div class="clear-fx"></div>
								
								
						  <?php else: ?>
						    <div id="post-404" class="noposts">
						        <p><?php _e('No posts found', 'default_textdomain');?></p>
						    </div><!-- /#post-404 -->
						  <?php endif; wp_reset_query(); ?>							
															
						<div class="clear-fx"></div>
					
						</div>
					</div>
					<!--/blog page content-->
					
				</div>
				<!--/blog posts list-->
					
					
					
				<!--sixteen columns-->
				<div class="sixteen columns">					
					<!--slide footer-->
					<div class="slideFooter">
						<a style="<?php echo $view->getPageSubTitleStyle(get_the_ID());?>" href="#" class="slideGoTop alignright">{ <?php _e('go to top', 'default_textdomain');?> }</a>
						<div class="clear-fx"></div>
					</div>
					<!--/slide footer-->
				</div>
					
				
			</div>
			
		</div>
		<!--/slide wrapper-->
	
	
</div>