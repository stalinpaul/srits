mobile_min_width = 920;
isMobile = false;
jQuery(document).ready(function(){    
    mainWebsite = new Main();
    mainWebsite.init();
});
function Main(){
    internalLikAtStart = "";
    handleSectionFirstLoad();
    var main_preloader;
    var siteUrl;
    this.init = function(){
        if(jQuery('body').attr('data-isMobile')=="true"){
            isMobile = true;
        }
        /*
        if(jQuery.browser.mobile || is_Touch()){
            isMobile = true;            
        }else{
            isMobile = false;
        } 
        */         
        siteUrl = "http://" + top.location.host.toString();     
        IMAGES_PATH = globalJS.IMAGES_PATH;
        windowResize();
        addWindowListener();                
        
        secure_screen = new SecureScreen();
        secure_screen.init();
        main_preloader = new MainPreloader();
        main_preloader.init(logoLoadingComplete);              
    }
    
    function logoLoadingComplete(){
        extractMenu();        
    }
    
    var menuItems = [];
    //extract menu items
    function extractMenu(){
        menuItems = new Array();
        jQuery('.mainMenu ul li').each(function(indx){
            var itemLink = jQuery(this).find('a');
            var itemLinkStrippedEnd = itemLink.attr('href').substring(siteUrl.length, itemLink.attr('href').length);
            menuItems.push({pageURL: itemLink.attr('href'), menuItem: itemLink, index: indx, itemLinkStrippedEnd: itemLinkStrippedEnd});             
        });
        loadSections();
    }        
    
    var sectionsData;
    var countLoadedSections;
    function loadSections(){
        sectionsData = new Array();
        countLoadedSections = -1;
        loadSection();
    }
    
    //load specific section
    function loadSection(){
        countLoadedSections++;
        if(countLoadedSections<menuItems.length){
            //load section
            var sectionURL = menuItems[countLoadedSections].pageURL;
            if(sectionURL=="#homeSectionID"){
                //skip load section
                var sectionObject = jQuery('#homeSectionID');                    
                sectionsData.push({sectionObject: sectionObject, sectionType: sectionObject.attr("data-type"), slideID: sectionObject.attr('id'), menuID: getMenuItemID(countLoadedSections),  menuIndex: countLoadedSections});                
                loadSection();
                return;                
            }else{                
                //continue load section
                var itemLinkStripped = sectionURL.substring(0, siteUrl.length);                                
                if(itemLinkStripped!=siteUrl){
                     //skip load section (external link)                     
                     loadSection();
                     return;   
                }else{
                    //load section
                    getPageContent(sectionURL, countLoadedSections);                   
                }                   
            }
         }else{
                //done load sections
                addSectionsToDom();
                menuHandler = new MenuHandler();
                menuHandler.init();
                if(jQuery.browser.mobile){
                    //jQuery(window).stellar({horizontalScrolling: false});    
                }
                                                
                
                var mobileParallax = jQuery('.homeSlide').attr('data-mobile-parallax');                
                
                if(isMobile){                    
                    //jQuery(".homeSlide").backstretch(jQuery('.homeSlide').attr('data-backurl'));
                    jQuery('.slide').each(function(indx){
                        var isCover = jQuery(this).attr('data-iscover');
                        var backUrl = jQuery(this).attr('data-backurl');
                        if(isCover=='ON'){                            
                            jQuery(this).backstretch(backUrl);
                        }
                    });
                    
                    if(mobileParallax=="ON"){
                        jQuery(window).stellar({horizontalScrolling: false});
                    }    
                }
                if(!isMobile){
                    jQuery(window).stellar({horizontalScrolling: false}); 
                }

                
                jQuery('.slide').waypoint(function(direction) { 
                    if(!isMenuMovement){                  
                        selectMenuItem(jQuery(this).attr('id'));
                    }
                });                                                
                menuBehavior();
                jQuery(window).scrollTop(0);
                goTopBehavior();
                main_preloader.gcc();
                try{
                removeLastWidgetLine();
                initTweetssearch();
                initFlickrSearch();                                
                }catch(e){}
                
                if(!isMobile){
                    addExtraWaypoints();                    
                    jQuery('.slideExtraPoint').waypoint(function(direction) { 
                        slideFirstTime(jQuery(this).data('slideIndex'));
                    });                     
                }                
                initSections();
                handleSocialLinks();
                handleSkillsComponents();
                handleClientBox();
                handleHomeScrollDown();
                handleMobileMenu();
                handleStartlink();
                handleInnerLinks();
                initIgalleryPatch(); 
                
                //supersleight.init();                
                try{                    
                    //supersleight.init();
                    jQuery('body').supersleight(); 
                }catch(e){}                                                                                        
         }
    }
    
    
    
    
    var sk_igallerys;
    function initIgalleryPatch(){
        try{
            sk_igallerys = new SkIGallerys();
            sk_igallerys.init();            
        }catch(e){}       
    }
    
    function handleInnerLinks(){
        jQuery('.innerLink').each(function(indx){
            jQuery(this).click(function(e){
                e.preventDefault();
                var indx = jQuery(this).attr('href');
                if(indx<sectionsData.length){
                    try{
                        goToSlide(sectionsData[indx], indx);
                    }catch(e){}
                }                
            });
        });
    }
    
    function handleStartlink(){
        if(internalLikAtStart!=""){
            var sectionIndx = extractSection(internalLikAtStart);
            if(sectionIndx!=-1){
                try{
                    goToSlide(sectionsData[sectionIndx], sectionIndx);
                }catch(e){}
            }
        }
    }
    
    
    function extractSection(link){
        var sectionIndx = -1;
        for(var i=0;i<sectionsData.length;i++){
            var menuIndx = sectionsData[i].menuIndex;
            var pageUrl = menuItems[menuIndx].pageURL;
            if(pageUrl==(siteUrl+'/'+link)){
                sectionIndx = i;
                break;
            }
        }
        return sectionIndx;
    }
    
    //var internalLikAtStart="";
    function handleSectionFirstLoad(){
        var indexOf = window.location.href.indexOf('#!');
        if(indexOf!=-1){                                  
            var firstPart = window.location.href.substring(0, indexOf);
            var secondPart = window.location.href.substring(indexOf+3, window.location.href.length);
            if(secondPart!=""){
                internalLikAtStart = secondPart;
            }  
            return;
        }        
    }
    
    
    function handleMobileMenu(){
        jQuery('#mobileSelect').change(function(){
                var sectionURL = jQuery("#mobileSelect option:selected").attr('data-href');
                                                                            
                var itemLinkStripped = sectionURL.substring(0, siteUrl.length);
                if(itemLinkStripped==siteUrl || itemLinkStripped=="#homeSectionID"){
                    //go to section                    
                    var menuItemID = jQuery("#mobileSelect option:selected").attr('data-linkedid');
                    for(var i=0;i<sectionsData.length;i++){
                        if(sectionsData[i].menuID==menuItemID){
                            goToSlide(sectionsData[i], i);
                            break;
                        }
                    }
                }           
        });
    }
    
    //handle scroll down
    function handleHomeScrollDown(){
        jQuery('.scrollDIMG').click(function(e){
            try{
                if(sectionsData[1]!=undefined){
                    goToSlide(sectionsData[1], 1);
                }
            }catch(e){}             
        });       
    }
    function handleClientBox(){        
        jQuery('.ourClientsList').each(function(indx){
            jQuery(this).css('opacity', .5);
            jQuery(this).hover(function(e){
                TweenMax.to(jQuery(this), .2, {css:{opacity:1}, ease:Power3.easeIn});
            }, function(e){
                TweenMax.to(jQuery(this), .2, {css:{opacity:.8}, ease:Power3.easeIn});
            });
        });
    }
    function handleSocialLinks(){
        jQuery('.normalTip').each(function(indx){
            jQuery(this).hover(function(e){
                //TweenMax.to(jQuery(this), 0, {css:{scale:1.1}, ease:Power4.easeOut});
            }, function(e){
                //TweenMax.to(jQuery(this), .1, {css:{scale:1}, ease:Power4.easeOut});
            });
        });
    }
    
    function handleSkillsComponents(){
        jQuery('.skillContainer .skill').each(function(indx){
            var percent = jQuery(this).attr('data-percent');
            TweenMax.to(jQuery(this), 0, {css:{scale:(percent/100)}});
            jQuery(this).hover(function(e){
                jQuery(this).addClass('skillTop');
                TweenMax.to(jQuery(this), .2, {css:{scale: 1}, ease:Back.easeIn});
            }, function(e){
                jQuery(this).removeClass('skillTop');
                TweenMax.to(jQuery(this), .2, {css:{scale: (jQuery(this).attr('data-percent')/100)}, ease:Back.easeIn});
            });
            //data-percent
        });
    }
    
    function addExtraWaypoints(){
        for(var i=0;i<sectionsData.length;i++){
            var point = jQuery('<div class="slideExtraPoint"></div>');
            point.appendTo(sectionsData[i].sectionObject);
            point.data('slideIndex', i);
        }
    }
    
    function slideFirstTime(slideIndex){
        if(sectionsData[slideIndex+1]!=null&&sectionsData[slideIndex+1]!=undefined){
            if(sectionsData[slideIndex+1].sectionFirstTime==true){
                sectionsData[slideIndex+1].sectionFirstTime = false; 
                //recent projects
                if(sectionsData[slideIndex+1].sectionType=="recentProjectsPage"){
                    if(recentProjects!=undefined){
                        recentProjects.showFirstTime();
                    }
                }
                //contact
                if(sectionsData[slideIndex+1].sectionType=="contactPage"){
                    if(conatctSection!=undefined){
                        conatctSection.showFirstTime();
                    }
                }                
                         
            }                     
        }       
    }    
       
    //go top
    function goTopBehavior(){
        jQuery('.slideGoTop').click(function(e){
            e.preventDefault();
            try{
                goToSlide(sectionsData[0], 0, true);
            }catch(e){};
        });
    }
    
    var isMenuMovement;
    //menu behavior
    function menuBehavior(){
        for(var i=0;i<menuItems.length;i++){                 
            menuItems[i].menuItem.click(function(e){
                var sectionURL = jQuery(this).attr('href');                                                             
                var itemLinkStripped = sectionURL.substring(0, siteUrl.length);
                if(itemLinkStripped==siteUrl || itemLinkStripped=="#homeSectionID"){
                    //go to section
                    e.preventDefault();
                    var menuItemID = jQuery(this).attr('id');
                    for(var i=0;i<sectionsData.length;i++){
                        if(sectionsData[i].menuID==menuItemID){
                            goToSlide(sectionsData[i], i);
                            break;
                        }
                    }
                } 
            });
        }
        jQuery('#logoContainer').click(function(){
            try{
                if(sectionsData[1]!=undefined){
                    goToSlide(sectionsData[1], 1);
                }
            }catch(e){}
        });
    }
    
    var currentMovingSlideObj;
    //go to slide
    function goToSlide(slideObj, slideIndex, isTop){
        currentMovingSlideObj = slideObj;
        if(isTop!=true){
            selectMenuItem(slideObj.slideID);
        }
        isMenuMovement = true;
        
        var pozition = slideObj.sectionObject.offset();
        if(isMobile){           
            jQuery('html,body').animate({
                scrollTop: pozition.top
            }, 2000, 'easeOutQuad', function onComplete(){
                slideMoved();
            });
        }else{
            var is_safari = navigator.userAgent.indexOf("Safari") > -1;
            is_safari = true;
            if(is_safari){
                jQuery('html,body').animate({
                    scrollTop: pozition.top
                }, 1500, 'easeInOutQuad', function onComplete(){
                    slideMoved();
                });                
            }else{
                TweenMax.to(window, 1.5, {scrollTo:{y: pozition.top}, ease:Power3.easeInOut, onComplete: slideMoved});
            }            
        }

    }
    
    
    //accessed from outside
    this.goToSlideByDataType = function(sectionType){
        for(var i=0;i<sectionsData.length;i++){
            if(sectionsData[i].sectionType==sectionType){
                goToSlide(sectionsData[i], i);
                break;
            }
        }
    }    
    
    function slideMoved(){
        isMenuMovement = false;       
    }  
    
    //select menu item
    function selectMenuItem(slideID){
        var menuIndex = extractMenuIndexFromSlides(slideID);
         for(var i=0;i<menuItems.length;i++){
             //selectedMenuItem
             menuItems[i].menuItem.removeClass('selectedMenuItem');
             if(i==menuIndex){
                 menuItems[i].menuItem.addClass('selectedMenuItem');
                if(window.location.href != "#!"+menuItems[i].itemLinkStrippedEnd){
                    window.location.href = "#!"+menuItems[i].itemLinkStrippedEnd;
                }                 
             }
         }
    }    
    
    //extract menu index
    function extractMenuIndexFromSlides(slideID){
        var menuIndex = 0;
        for(var i=0;i<sectionsData.length;i++){
            if(sectionsData[i].slideID==slideID){
                menuIndex = sectionsData[i].menuIndex;
                break;
            }
        }
        return menuIndex;        
    }
    
    //load page content 
    function getPageContent(sectionURL, menuIndex){
        var axjReq = new JQueryAjax();
        axjReq.getData(sectionURL, function(data){
            //section loaded
           var sectionObject = jQuery(data);                 
           sectionsData.push({sectionObject: sectionObject, sectionType: sectionObject.attr("data-type"), slideID: sectionObject.attr('id'), menuID: getMenuItemID(menuIndex),  menuIndex: menuIndex, sectionFirstTime: true});
           loadSection();
        }, function(){alert('could not load section: '+sectionURL)});        
    }    
    
    //get menu item id
    function getMenuItemID(indx){
        var id=0;
        for(var i=0;i<menuItems.length;i++){
            if(indx==i){
                id = menuItems[i].menuItem.attr('id');
                break;
            }
        }
        return id;
    }
    
    //add sections to document
    function addSectionsToDom(){
        var mainMenu = jQuery('.mainMenu');
        mainMenu.remove();
        for(var i=0;i<sectionsData.length;i++){
            var sectionObj = sectionsData[i].sectionObject;
            sectionObj.insertBefore(jQuery('.footerSlide'));
            if(i==1){
                mainMenu.appendTo(sectionObj);
            }
        }
    }
    
    
       //tweet
       function initTweetssearch(){
        var t_ui = jQuery('#t_feed_ui');
        var t_feed = new Tweet1_1();
        t_feed.init(t_ui, globalJS.TWITTER_SERVICE_FEED, globalJS.SK_T_USER, globalJS.SK_T_COUNT);
       }
       
       //flickr
       function initFlickrSearch(){
           jQuery('.flickrContainer').each(function(indx){
               jQuery(this).css('opacity', 0);
               jQuery(this).flickr({ user_name: jQuery(this).attr('data-username'), api_key: jQuery(this).attr('data-api'), view_num: jQuery(this).attr('data-thumbsNo'), size: 'square' });
               jQuery(this).removeAttr('data-username');
               jQuery(this).removeAttr('data-thumbsNo'); 
               jQuery(this).removeAttr('data-api');
               jQuery(this).delay(1500).animate({
                   opacity: 1
               }, 500, 'easeOutQuint');                 
           });                    
       }       
           
    //remove last widget line (footer links widgets)
    function removeLastWidgetLine(){
        var lis = new Array();
        jQuery('.footerLinksList li').each(function(indx){
            lis.push(jQuery(this));
        });
        if(lis.length>0){
            lis[lis.length-1].remove();
        }
    }     
    
    
    //called by recent projects
    this.goToPortfolio = function(){
        var portfolioSection;
        var slideIndx;
        for(var i=0;i<sectionsData.length;i++){
            if(sectionsData[i].sectionType=="portfolioPage"){
                slideIndx = i;
                portfolioSection = sectionsData[i].sectionObject;
                break;
            }
        }
        if(portfolioSection!=undefined){
            goToSlide(sectionsData[slideIndx], slideIndx);
        }else{
            alert('Please add a portfolio page template from admin!');
        }
    }
    
    
    //sections
    function initSections(){
        portfolioSection = new PortfolioSection();
        portfolioSection.init(false);
        recentProjects = new RecentProjects();
        recentProjects.init();
        conatctSection = new ContactSection();
        conatctSection.init();
        if(!isMobile){
            recentProjects.hideItems();
            conatctSection.hideItems();
        }
        blog_section = new BlogSection();
        blog_section.init();
        
        //footer flow style
        jQuery('.footerWidget').each(function(indx){
            var modulo = (indx+1)%3;
            if(indx!=0&&modulo==0){
                jQuery('<div class="resetColumns"></div>').insertAfter(jQuery(this));
            }
        });
        disableRegularImgClicks();        
    }
    
    function disableRegularImgClicks(){        
        jQuery('.regularPage a').each(function(indx){
            var val = jQuery(this).attr('rel');
            if(val!=undefined){
                if(val.substring(0, 10)=="attachment"){
                    jQuery(this).css('cursor', 'default');
                    jQuery(this).click(function(e){
                        e.preventDefault();
                    });
                }                
            }
        });
    }
    
    
    //dispatch event to all listeners
    function addWindowListener(){
        EventBus.addEventListener(Event.WINDOW_RESIZE, windowResize);
        jQuery(window).resize(function(){
            EventBus.dispatchEvent(new Event(Event.WINDOW_RESIZE, ""));
        });      
    }    
        
    //handle resize
    function windowResize(){ 
        /*     
        if(jQuery(window).width()<mobile_min_width){
            isMobile = true;            
        }else{
            isMobile = false;
        } 
        */           
    }
    
    //extract number
    function extractNumber(pxValue){
        var striped = pxValue.substring(0, pxValue.length-2);
        var val = parseFloat(striped);
        return val;
    }
    
    function is_Touch() {
        var isTouch = false;
        try{
            if(window.Touch){
                isTouch = true; 
            }
        }catch(e){}
        return isTouch;
    }    
    
             
}




function SecureScreen(){
    
    
    var secureUI;
    this.init = function(){
        secureUI = jQuery('#secureScreen');
    }
    
    this.secure = function(val){
        if(val){
            //secure
            secureUI.removeClass('secure');
        }else{
            //dismiss
            secureUI.addClass('secure');
        }
    }
}



