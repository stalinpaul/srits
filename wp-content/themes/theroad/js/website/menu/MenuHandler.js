function MenuHandler(){

    this.init = function(){
        resizeEventHandler();
        EventBus.addEventListener(Event.WINDOW_RESIZE, resizeEventHandler);
        
        //handle menu position
        if(globalJS.isMenuOnFirstpage=="ON"){
            jQuery('.mainMenu').addClass("fixedMenu");
        }else{
            jQuery(window).scroll(function() {
                if (jQuery(this).scrollTop() > jQuery('.homeSlide').height()) {
                    jQuery('.mainMenu').addClass("fixedMenu");
                } else {
                    jQuery('.mainMenu').removeClass("fixedMenu");
                }
            });
        } 
        
        if(isMobile){          
            jQuery('.mainMenu').addClass('removeMenu');
            jQuery('.mobileMenu').removeClass('removeMenu');
            //mobileMenu
        }else{
            jQuery('.mobileMenu').addClass('removeMenu');
            jQuery('.mainMenu').removeClass('removeMenu');
        }                   
    }
    
    
    //resize event
    function resizeEventHandler(){

        jQuery('.mainMenu').css('width', jQuery('.homeSlide').width()+'px');

        //handle mobile devices menu
    }
    
    function positionLow(){
        //jQuery('.mainMenu').css('top', 0+jQuery('.homeSlide').height()+'px');
    }
}
