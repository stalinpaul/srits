var logoContainer;
function LogoControler(){
        
    var initialW;
    var mainLogoIMG;
    this.init = function(mainLogo){
        mainLogoIMG = mainLogo;
        logoContainer = jQuery('#logoContainer');
        initialW = logoContainer.width();
        EventBus.addEventListener(Event.WINDOW_RESIZE, resizeEventHandler);
        mainLogo.appendTo(jQuery('#innerLogoContainer'));        
        resizeEventHandler();
    }
    
    function resizeEventHandler(){        
        if(jQuery('#homewrapper').width()<logoContainer.width()+80){
            logoContainer.css('width', jQuery('#homewrapper').width()-60+'px');
        }else{
            logoContainer.css('width', initialW+'px');
        }        
        logoContainer.css('left', jQuery('#homewrapper').width()/2-logoContainer.width()/2+'px');
        
        var total_height = jQuery('#homewrapper').height();                        
        var logoContainerHeight = (total_height*60)/100;
        logoContainer.css('height', logoContainerHeight+'px');
        
        var temp_space = 50;
        var innerLogoH = logoContainer.height()-jQuery('#logoArrowDown').height();
        if(innerLogoH<mainLogoIMG.height()+temp_space){
            innerLogoH = mainLogoIMG.height()+temp_space;
        }
        jQuery('#innerLogoContainer').css('height', innerLogoH+'px');
        var spacerH = (innerLogoH-mainLogoIMG.height())/2;

        spacerH = spacerH+(spacerH*40)/100;
        jQuery('#logoSpacerTop').css('height', spacerH+'px');
        
        //position social sidebar
        //jQuery('#homeSocialSidebar').css('top', jQuery('.homewrapper').height()-100+'px');
    }
}
