function MainPreloader(){
    
    var preloaderContainer;
    var loadingCallBack;
    this.init = function(loadingComplete){
        loadingCallBack = loadingComplete;
        preloaderContainer = jQuery('#mainPreloader');
        showLogoPreloader();        
    }
    
    var preloadLogo;
    var preloadText;
    function showLogoPreloader(){
       
       preloadText = preloaderContainer.find('.mainPreloaderText');
       preloadText.css('opacity', 0);
       
       preloadTextGlobal = preloadText;
       preloadTextGlobalW = preloadText.width();
       
       var axjReq = new JQueryAjax();
       var logoURL = preloaderContainer.attr('data-logo');       
       axjReq.loadImage(logoURL, function(img){
          //loaded
          preloadLogo = img;          
          preloadLogo.addClass('mainPreloaderItem');
          preloadLogo.appendTo(preloaderContainer);
          
          //global
          mainPreloaderLogo = preloadLogo;
          mainPreloaderLogoW = mainPreloaderLogo.width();
          mainPreloaderLogoH = mainPreloaderLogo.height();
          
          EventBus.addEventListener(Event.WINDOW_RESIZE, resizeEventHandler);
          resizeEventHandler();
          //return;
          
          TweenMax.to(preloadLogo, 0, {css:{scale:0}});
          TweenMax.to(preloadLogo, .5, {css:{scale:1}, ease:Power3.easeIn});
          
          preloadText.css('opacity', 1);
          TweenMax.to(preloadText, 0, {css:{scale:0}});
          TweenMax.to(preloadText, .4, {css:{scale:1}, delay: .3, ease:Power3.easeIn, onComplete:preoadTextComplete});                  
      });         
    }
    
    var loadTextAnimation;
    function preoadTextComplete(){
        //loadTextAnimation = TweenMax.to(preloadText, 1, {css:{opacity:.6}, repeat:-1, yoyo:true, delay: 2});
        loadTextAnimation = TweenMax.to(preloadText, 1, {css:{opacity:.6}, repeat:-1, yoyo:true, delay: 2});
        loadAssets();       
    }
    
    var mainLogo;
    function loadAssets(){
        var mainLogoURL = jQuery('#logoContainer').attr('data-logo-url');
        var axjReq = new JQueryAjax();        
        axjReq.loadImage(mainLogoURL, function(img){
            mainLogo = img;            
            logoControler = new LogoControler();
            logoControler.init(mainLogo);
            loadingCallBack();
        });
    }
    
    
    this.gcc = function(){
        closeMainPreloader();
    }
    
    //close reloader
    function closeMainPreloader(){
        TweenMax.to(preloadLogo, .5, {css:{scale:0}, ease:Back.easeIn});
        TweenMax.to(preloadText, .4, {css:{scale:0}, ease:Power3.easeOut, delay: .3});
        TweenMax.to(preloaderContainer, .6, {css:{width:"0px"}, ease:Power3.easeOut, delay: .7, onComplete: runCleaner});        
    }
    
    function runCleaner(){
        try{
            loadTextAnimation.kill();
            preloaderContainer.remove();
        }catch(e){}
    }
    
    function resizeEventHandler(){        
        try{
            preloadLogo.css('left', preloaderContainer.width()/2-preloadLogo.width()/2+'px');
            preloadLogo.css('top', preloaderContainer.height()/2-preloadLogo.height()+'px');
            
            preloadText.css('left', preloaderContainer.width()/2-preloadText.width()/2+'px');
            preloadText.css('top', extractNumber(preloadLogo.css('top'))+preloadLogo.height()+15+'px');                        
        }catch(e){}
    }
    
    //extract number
    function extractNumber(pxValue){
        var striped = pxValue.substring(0, pxValue.length-2);
        var val = parseFloat(striped);
        return val;
    }              
}
