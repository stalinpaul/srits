function ContactSection(){
    
    var gMapIframe;
    this.init = function(){
        gMapIframe = jQuery('.mapContainer iframe');
        handleContactForm();
    }
    
    this.hideItems = function(){
        gMapIframe.remove();
    }
    
    this.showFirstTime = function(){
        var isIE8 = jQuery.browser.msie && + jQuery.browser.version === 8;
        if(!isIE8){
            gMapIframe.appendTo(jQuery('.mapContainer'));
        }
    }
    
    
    /**
     * handle contact form
     */
    var contactFormFields=[];
    var thankYouMsg;
    function handleContactForm(){
        contactFormFields = new Array();
        thankYouMsg = jQuery('.contactForm').find('.formThankYou').html();
        jQuery('.contactForm').find('.formThankYou').html('');        
        contactFormFields = new Array();
        jQuery('.contactForm').find('.inputField').each(function(index){
            contactFormFields.push({element: jQuery(this), id: jQuery(this).attr('id'), placeholder: jQuery(this).find('input').attr('placeholder'), val: ""});           
        });
        jQuery('.contactForm').find('.inputTextArea').each(function(){
            contactFormFields.push({element: jQuery(this), id: jQuery(this).attr('id'), placeholder: jQuery(this).find('textarea').attr('placeholder'), val: ""});
        });
        
        jQuery('.contactForm').find('#sendBTN').click(function(e){
            e.preventDefault();
            var allValid = true;                        
            for(var i=0;i<contactFormFields.length;i++){
                var valid = true;
                var val = contactFormFields[i].element.find('input').val();
                if(!validateFormValue(val)){
                    valid = false;
                }
                if(contactFormFields[i].id=="email"){
                    if(!validateEmail(val)){
                        valid = false;
                    }else{
                        valid = true;
                    }
                }
                if(contactFormFields[i].id=="message"){
                    val = document.getElementById('txt').value;
                    if(!validateFormValue(val)){
                        valid = false;
                    }else{
                        valid = true;
                    }               
                }
                if(!valid){                    
                    contactFormFields[i].element.addClass('error');
                    allValid = false;                    
                }else{
                    contactFormFields[i].val = val;
                    contactFormFields[i].element.removeClass('error');
                }
            }
        
            if(allValid){
                //send email
                jQuery.post(globalJS.MAIL_SCRIPT, {name:contactFormFields[0].val, email:contactFormFields[1].val, website:contactFormFields[2].val, message:contactFormFields[3].val, receiveEmail: globalJS.emailReceive}, function(data){
                    if(data.success=="true"){
                        jQuery('.contactForm').find('.formThankYou').html(thankYouMsg);
                        resetFields();
                    }else{
                        jQuery('.contactForm').find('.formThankYou').html("something went wrong!");
                        resetFields();                        
                        //console.log('NOT OK')
                        //alert('Could not send email. Please check with your hosting provider! Error log: '+data.success);
                    }
                    
                }, 'json');                
            }
        });
    }
    
    function resetFields(){
        for(var i=0;i<contactFormFields.length;i++){
            if(contactFormFields[i].id=="message"){
                document.getElementById('txt').value = "";
            }else{
                contactFormFields[i].element.find('input').val('');
            }
        }
    }
    function validateFormValue(val){
        var valid = false;
            if(val!=undefined && val.length>2){
                valid = true;
            }
        return valid;       
    }
    function validateEmail(val){
            var atpos = val.indexOf("@");
            var dotpos = val.lastIndexOf(".");
            var valid = true;
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=val.length){
               valid = false;
            }
        return valid;        
    }    
    
    
    
}
