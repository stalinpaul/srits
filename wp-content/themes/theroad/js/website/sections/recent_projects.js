function RecentProjects(){
    
    var recentProjects;
    this.init = function(){
        recentProjects = new Array();
        jQuery('.recentProjectItem').each(function(indx){
            recentProjects.push({jitem: jQuery(this)});
        });
        for(var i=0;i<recentProjects.length;i++){          
            recentProjects[i].jitem.click(function(e){
                e.preventDefault();
                try{
                    //mainWebsite.goToPortfolio();
                    portfolio_open.open(jQuery(this).attr('data-permalink'), jQuery(this).attr('data-uid'));
                }catch(e){}
            });
        }
    }
    
    this.hideItems = function(){
        for(var i=0;i<recentProjects.length;i++){
            TweenMax.to(recentProjects[i].jitem, 0, {css:{scale:0}});
        }        
    }
    
    this.showFirstTime = function(){
        var delay = 0;
        for(var i=0;i<recentProjects.length;i++){
            TweenMax.to(recentProjects[i].jitem, .5, {css:{scale:1}, delay: delay, ease:Power3.easeIn});
            delay+=.2;
        }        
    }
    
}
