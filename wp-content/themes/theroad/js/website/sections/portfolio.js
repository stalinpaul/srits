function PortfolioSection(){
    
    var isShowOff = false;
    this.init = function(showOff){
        portfolio_open = new PortfolioOpen();
        portfolio_lightbox = new PortfolioLightbox();
        portfolio_lightbox.setup();
        isShowOff = showOff;
        imagesLoad();
        //readMore
    }
    
    
    function imagesLoad(){
        jQuery('.isotopeContainer').imagesLoaded( function(){
                initIsotopeMenu(); 
                handleItems();
                handleImageItems();                               
             });       
    }
    
    this.showFirstTime = function(){
        var delay = 0;
        jQuery('.isotopeItem').each(function(indx){
            TweenMax.to(jQuery(this), .5, {css:{scale:1}, delay: delay, ease:Power3.easeIn});
            delay+=.2;            
        });        
    }
    
    var items =[];
    function handleItems(){
        items = new Array();
        jQuery('.isotopeItem').each(function(indx){
            if(isShowOff==true){
                TweenMax.to(jQuery(this), 0, {css:{scale:0}});
            }
            var id = jQuery(this).attr('data-uid');
            var url = jQuery(this).attr('data-permalink');
            var itemType = jQuery(this).children(":first").attr('class');
            if(itemType=="imageItem"){
               //image type 
               jQuery(this).click(function(e){
                   e.preventDefault();
                   openItem(jQuery(this).attr('data-uid'));
               });
            }else{
                //text item
               jQuery(this).find('.portfolioPreviewTitle').click(function(e){
                   e.preventDefault();
                   openItem(jQuery(this).attr('data-uid'));
               });
               jQuery(this).find('.readMore').attr('data-uid', id);
               jQuery(this).find('.readMore').click(function(e){
                   e.preventDefault();
                   openItem(jQuery(this).attr('data-uid'));
               });               
            }           
            items.push({item: jQuery(this), itemType: '', url: url, id: id});            
        });
        portfolio_open.init(items);
    }
    
    function openItem(id){
        portfolio_open.open(extractURL(id), id);
    }
    
    function extractURL(id){
        var url = '';
        for(var i=0;i<items.length;i++){
            if(items[i].id==id){
                url = items[i].url;
                break;
            }
        }
        return url;
    }
        
    
    //handle images hover
    function handleImageItems(){
        jQuery('.imageItem').each(function(indx){
            jQuery(this).css('height', jQuery(this).find('.portfolioPreviewThumb').height()+'px');
            //jQuery(this).find('.imageItemOverlayTransparent').css('height', jQuery('.portfolioPreviewThumb').height()-1+'px');
            jQuery(this).hover(function(e){
                //TweenMax.to(jQuery(this).find('.imageItemOverlayTransparent'), .2, {css:{left:"0px"}, ease:Sine.easeIn});
                TweenMax.to(jQuery(this).find('.imageItemOverlayTransparent'), .2, {css:{opacity:1}, ease:Sine.easeIn});
                TweenMax.to(jQuery(this).find('.imageItemTitle'), .3, {css:{left:"-10px"}, ease:Sine.easeIn});
                //jQuery(this).find('.imageItemOverlayTransparent').css('height', jQuery('.portfolioPreviewThumb').height()+10+'px');               

            }, function(e){                
                TweenMax.to(jQuery(this).find('.imageItemOverlayTransparent'), .2, {css:{opacity:0}, ease:Sine.easeIn});
                TweenMax.to(jQuery(this).find('.imageItemTitle'), .2, {css:{left:"305px"}, ease:Sine.easeIn});
            });

        });
    }
    
    var isotopeMenu;
    function initIsotopeMenu(){
        isotopeMenu = new Array();     
        jQuery('.isotopeMenu li a').each(function(index){
            var link = jQuery(this);
            isotopeMenu.push({categoryID: link.attr('href'), link: link, iconClass: link.attr('class')});            
            link.click(function(e){
                e.preventDefault();
                selectMenuItem(index);              
            });
        });
        selectMenuItem(0);
    }
    
    
    //select isotope menu
    function selectMenuItem(indx){
        for(var i=0;i<isotopeMenu.length;i++){
            if(i==indx){
                isotopeMenu[i].link.addClass('isotopeMenuSelected');
                filterIsotopeItems(indx);
            }else{
                isotopeMenu[i].link.removeClass('isotopeMenuSelected');
            }
        }
    }

    function filterIsotopeItems(index){
         var selector = "."+isotopeMenu[index].categoryID;
         (selector==".*")?selector="*":null;
                  
         jQuery('.isotopeContainer').isotope({
               layoutMode : 'masonry',
               filter: selector,
               animationOptions: {
                   duration: 750,
                   easing: 'linear',
                   queue: false
              }});       
    }        
        
}



//opened portfolio
function PortfolioOpen(){
    
    var portfolioContainer;
    var isPortfolioOpened;
    var currentURL = "-2891371";
    var portfolioItems;
    
    var imageLoad;
    var txtLoad;
    /*
    var originalImageLoadH;
    var originalImageLoadW;    
    var originalTextW;
    */
    var originalTextH;
    this.init = function(items){
        portfolioItems = items;
        portfolioContainer = jQuery('.openedPortfolio');
        
        imageLoad = jQuery('#singleProjectPreloadIMG');
        txtLoad = jQuery('#singleProjectPreloadTXT');  
        originalImageLoadH = imageLoad.height(); 
        originalImageLoadW = imageLoad.width();
        
        originalTextH = txtLoad.height(); 
        originalTextW = txtLoad.width();            
    }    
    
    var tempURL;
    this.open = function(url, id){
        openP(url, id);     
    }
    
    function openP(url, id){
        tempURL = url;
        var pozition = jQuery('.openedPortfolio').offset();
        if(isMobile){           
            jQuery('html,body').animate({
                scrollTop: pozition.top-80
            }, 1500, 'easeOutQuad', function onComplete(){
                scrolledToSlider();
            });
        }else{                    
            var is_safari = navigator.userAgent.indexOf("Safari") > -1;
            is_safari = true;
            if(is_safari){
                jQuery('html,body').animate({
                    scrollTop: pozition.top-80
                }, 1500, 'easeInOutQuad', function onComplete(){
                    scrolledToSlider();
                });
            }else{
                TweenMax.to(jQuery(window), 1.1, {scrollTo:{y: pozition.top-80}, ease:Power3.easeInOut, onComplete: scrolledToSlider});
            }            
        }         
    }
        
    //implement close portfolio
    function closeReopen(){
        TweenMax.to(jQuery('.portfolioOpenItemContent'), .3, {css:{opacity: 0}, ease:Power3.easeInOut});
        TweenMax.to(portfolioContainer, 1, {css:{height: '150px'}, ease:Power3.easeInOut});
        TweenMax.to(jQuery('.portfolioPreloaderContainer'), .3, {css:{opacity: 1}, ease:Power3.easeInOut});
        showPreload();
    }
    
    function closeProject(){
        TweenMax.to(jQuery('.portfolioOpenItemContent'), .3, {css:{opacity: 0}, ease:Power3.easeInOut});
        TweenMax.to(portfolioContainer, 1, {css:{height: '0px'}, ease:Power3.easeInOut, onComplete: projectClosed});
    }
    //clear data
    function projectClosed(){
            secure_screen.secure(false);
            jQuery('.portfolioOpenItemContent').empty();
            currentURL = "-2891371";
            mainWebsite.goToSlideByDataType('portfolioPage');                              
    }
    
    function scrolledToSlider(){
        if(currentURL==tempURL){
            return;
        }
        openProject();
    }
    
    function openProject(){
        secure_screen.secure(true);
        currentURL = tempURL;
        closeReopen();
    }
    
    var newProjectData;
    function loadProjectPage(){
        var axjReq = new JQueryAjax();
        axjReq.getData(currentURL, function(data){
           newProjectData = jQuery(data);
           hidePrelaod();          
        }, function(){alert('could not load portfolio item: '+item.url)});         
    }
    
    var existingContent;
    function initAddContent(){
        secure_screen.secure(false);
        jQuery('.portfolioPreloaderContainer').addClass('hidePortfolioItemPreloader');
        jQuery('.portfolioOpenItemContent').empty();
        addContent();
    }
    
    function removeExistingContent(){
        try{
            existingContent.remove();
        }catch(e){}
        addContent();
    }
    
    var largeImagesAC;
    function addContent(){
        existingContent = newProjectData;
        jQuery('.portfolioOpenItemContent').css('opacity', 0);
        existingContent.appendTo(jQuery('.portfolioOpenItemContent'));
        TweenMax.to(jQuery('.portfolioOpenItemContent'), .5, {css:{opacity: 1}, ease:Power3.easeInOut});
        
        portfolioContainer.css('height', 'auto');
        jQuery('.portfolioOpenItemContent').show();
        
        largeImagesAC = extractLargeImages();  
        eiSliderStart();
        initProject(); 
        
        projectTitle = jQuery('.singleProjTop').attr('data-p_title');      
    }
    var projectTitle;
    
    function extractLargeImages(){
        var imagesAC = new Array();
        jQuery('#sliderX .sliderx-large li').each(function(indx){
            imagesAC.push({url: jQuery(this).attr('data-full'), img: ""});
        });
        return imagesAC;
    }
    
    var sliderX;
    //init eislider
    function eiSliderStart(){
        sliderX = new SliderX();
        var sliderXHeight = jQuery('#sliderX').attr('data-sliderHeight');
        
        sliderX.init('sliderX', sliderXHeight);        
    }    
    
    function getPortfolioItem(id){
        var item;
        for(var i=0;i<portfolioItems.length;i++){
            if(portfolioItems[i].id==id){
                item = portfolioItems[i];
                break;
            }
        }
        return item;
    }
    
    
    //show prelaoder
    function showPreload(){
        handleLoadingPosition(loadProjectPage);
        jQuery('.portfolioPreloaderContainer').removeClass('hidePortfolioItemPreloader');
    }

    
    //hide preloader
    function hidePrelaod(){
        TweenMax.to(imageLoad, .5, {css:{scale:0}, ease:Power3.easeIn});
        TweenMax.to(txtLoad, .4, {css:{scale:0}, delay: .3, ease:Power3.easeIn, onComplete:initAddContent});                        
    }    
    
    //handle loading position
    function handleLoadingPosition(callBack){
        var top = 25;
        imageLoad.css('left', portfolioContainer.width()/2-originalImageLoadW/2+'px');
        imageLoad.css('top', top+'px');
        txtLoad.css('left', portfolioContainer.width()/2-originalTextW/2+'px');
        txtLoad.css('top', originalImageLoadH+top+10+'px');
        
        TweenMax.to(imageLoad, 0, {css:{scale:0}});
        TweenMax.to(imageLoad, .5, {css:{scale:1}, ease:Power3.easeIn});
        TweenMax.to(txtLoad, 0, {css:{scale:0}});
        TweenMax.to(txtLoad, .4, {css:{scale:1}, delay: .3, ease:Power3.easeIn, onComplete:callBack});               
    }
    
    
    
    //OPENED PROJECT
    //handle open project behavior
    function initProject(){
        buttonsHover();
        buttonsAction();
        adjustMovieSize();
    }
    
    //adjust movie size
    function adjustMovieSize(){
        jQuery('.videoProject').find('iframe').css('width', '100%');
    }
    
    function buttonsAction(){
        jQuery('.singleProjTop .closeBTN').click(function(e){
            e.preventDefault();
            secure_screen.secure(true);
            closeProject();
        });
        jQuery('.singleProjTop .nextBTN').click(function(e){
            e.preventDefault();            
            tempURL = jQuery(this).attr('href');
            openProject();
        });
        jQuery('.singleProjTop .prevBTN').click(function(e){
            e.preventDefault();            
            tempURL = jQuery(this).attr('href');
            openProject();
        });
        jQuery('.singleProjTop .enlargeBTN').click(function(e){
            e.preventDefault();  
            portfolio_lightbox.init(largeImagesAC, sliderX.getCurrent(), projectTitle);                      
        });                         
    }
    
    function buttonsHover(){
        jQuery('.singleProjTop .genericBoxButton').each(function(indx){
            jQuery(this).hover(function(e){
                TweenMax.to(jQuery(this), .1, {css:{opacity:.8}, ease:Power3.easeIn});
            }, function(e){
                TweenMax.to(jQuery(this), .1, {css:{opacity:1}, ease:Power3.easeIn});
            });
        });
    }
    
}


function PortfolioLightbox(){
    
    var imagesAC;
    var lightboxContainerUI;
    var lightboxImageContainer;
    var imgLoading;
    var loadingTextUI;
    var isActive;
    var lightboxTitle;
    var lightboxControls;
    
    var leftBtn;
    var closeBtn;
    var rightBtn;
    
    var preloaderGIF;
    
    var lightboxHTMLData;
    this.setup = function(){
        lightboxHTMLData = jQuery('#lightboxWrapper').html();
    }
    
    var currentSliderIndex;
    var title;
    this.init = function(images, current, projectTitle){
        
        if(current==undefined||current==null){
            current = 0;
        }
        currentSliderIndex = current;
        imagesAC = images;
        lightboxContainerUI = jQuery(lightboxHTMLData);
        lightboxContainerUI.appendTo('body');
        lightboxImageContainer = lightboxContainerUI.find('.lightboxImageContainer');
        imgLoading = lightboxContainerUI.find('.imgLoadingLightbox');
        loadingTextUI = lightboxContainerUI.find('.mainPreloaderText');
        lightboxTitle = lightboxContainerUI.find('.lightboxTitle');
        lightboxControls = lightboxContainerUI.find('.lightboxControls');
        preloaderGIF = lightboxContainerUI.find('.preloaderGIF');
        
        lightboxTitle.html(projectTitle);
        
        leftBtn = lightboxControls.find('.lightBxLeft');
        closeBtn = lightboxControls.find('.lightBxClose');
        rightBtn = lightboxControls.find('.lightBxRight');
        
        buttonsHover();
        buttonsClick();

        PortfolioLightbox.first = false;
        EventBus.addEventListener(Event.WINDOW_RESIZE, windowResize);
        lightboxTitle.css('opacity', 0);
        lightboxControls.css('opacity', 0);
        preloaderGIF.css('opacity', 0);
        open();
    }
    
    function buttonsHover(){
        jQuery('.portfolioLightbox .lightBxLeft').hover(function(e){
            if(!leftValid){return;}
                TweenMax.to(jQuery(this), .1, {css:{opacity:.8}, ease:Power3.easeIn});
            }, function(e){
                if(!leftValid){return;}
                TweenMax.to(jQuery(this), .1, {css:{opacity:1}, ease:Power3.easeIn});
            }); 
        jQuery('.portfolioLightbox .lightBxClose').hover(function(e){
                TweenMax.to(jQuery(this), .1, {css:{opacity:.8}, ease:Power3.easeIn});
            }, function(e){
                TweenMax.to(jQuery(this), .1, {css:{opacity:1}, ease:Power3.easeIn});
            }); 
        jQuery('.portfolioLightbox .lightBxRight').hover(function(e){
            if(!rightValid){return;}
                TweenMax.to(jQuery(this), .1, {css:{opacity:.8}, ease:Power3.easeIn});
            }, function(e){
                if(!rightValid){return;}
                TweenMax.to(jQuery(this), .1, {css:{opacity:1}, ease:Power3.easeIn});
            });                         
    }
    
    function buttonsClick(){
        jQuery('.portfolioLightbox .lightBxLeft').click(function(e){
            e.preventDefault();
            if(!leftValid){
                return;
            }
            currentIndex--;
            initopenNewImage();
        });
        jQuery('.portfolioLightbox .lightBxClose').click(function(e){
            e.preventDefault();
            closeLightbox();
        });
        jQuery('.portfolioLightbox .lightBxRight').click(function(e){
            e.preventDefault();
            if(!rightValid){
                return;
            }
            currentIndex++;
            initopenNewImage();
        });                
    }
    
    function initopenNewImage(){
        secure_screen.secure(true);
        validateButtons();
        TweenMax.to(preloaderGIF, 0, {css:{opacity: 0, scale: 0}});
        TweenMax.to(currentImage, .4, {css:{opacity: 0}, ease:Power3.easeIn});
        TweenMax.to(preloaderGIF, .2, {css:{opacity: 1, scale: 1}, delay: .4, ease:Power3.easeIn, onComplete: loadNewImage});
        //preloaderGIF
    }
    
    function loadNewImage(){
        try{            
            currentImage.remove();
        }catch(e){}
        loadImage(newImageLoaded);
    }
    
    function newImageLoaded(img){
        if(imagesAC[currentIndex].img==""){
            imagesAC[currentIndex].img = img;             
        }
        TweenMax.to(preloaderGIF, .3, {css:{scale: 0, opacity: 0}, ease:Power3.easeIn});
        showImage();      
    }
    
    var leftValid = false;
    var rightValid = false;
    function validateButtons(){
        leftBtn.css('visibility', 'visible');
        rightBtn.css('visibility', 'visible');
        leftBtn.removeClass('lightboxDisabledButton');
        rightBtn.removeClass('lightboxDisabledButton');
        leftBtn.addClass('portfolioButtonColor');
        rightBtn.addClass('portfolioButtonColor');
        leftValid = true;
        rightValid = true;
        if(currentIndex==0){
            //leftBtn.css('visibility', 'hidden');
            leftBtn.removeClass('portfolioButtonColor');
            leftBtn.addClass('lightboxDisabledButton');
            leftValid = false;
        }
        if(currentIndex==imagesAC.length-1){
            //rightBtn.css('visibility', 'hidden');
            rightBtn.removeClass('portfolioButtonColor');
            rightBtn.addClass('lightboxDisabledButton');
            rightValid = false;
        }
        if(currentIndex!=imagesAC.length-1 && currentIndex!=0){
            leftBtn.css('visibility', 'visible');
            rightBtn.css('visibility', 'visible');
            leftBtn.removeClass('lightboxDisabledButton');
            rightBtn.removeClass('lightboxDisabledButton');            
            leftValid = true;
            rightValid = true;            
        }                 

    }
    
    function closeLightbox(){           
        secure_screen.secure(true);
        TweenMax.to(currentImage, .4, {css:{opacity: 0}, ease:Power3.easeIn});
        TweenMax.to(lightboxTitle, .4, {css:{opacity: 0}, delay: .2, ease:Power3.easeIn});
        TweenMax.to(lightboxControls, .4, {css:{opacity: 0}, delay: .2, ease:Power3.easeIn});
        
        
        //return;
        TweenMax.to(lightboxContainerUI, .5, {css:{height: '2px'}, delay: .6, ease:Power3.easeIn});
        TweenMax.to(lightboxContainerUI, .5, {css:{top: jQuery(window).height()/2+'px'}, delay: .6, ease:Power3.easeIn, onComplete: lightboxClosed});                
    }
    
    function lightboxClosed(){
        secure_screen.secure(false);
        TweenMax.to(lightboxContainerUI, .2, {css:{height: '0px'}, ease:Power3.easeIn, onComplete: gcc});
    }
    
    function gcc(){
        isActive = false;
        try{
            lightboxImageContainer.empty();
            lightboxContainerUI.remove();
        }catch(e){}
        //lightboxContainerUI.addClass('hidePortfolioLightbox');
    }
    
    function open(){
        secure_screen.secure(true);
        lightboxContainerUI.removeClass('hidePortfolioLightbox');
        lightboxContainerUI.css('width', jQuery(window).width()+'px');
        lightboxContainerUI.css('top', jQuery(window).height()/2+'px');
        
        //originalTextH
        
        var firstGap = 152;
        var imageTop = 30;
        imgLoading.css('left', lightboxContainerUI.width()/2-originalImageLoadW/2+'px');
        imgLoading.css('top', imageTop+'px');
        
        loadingTextUI.css('left', lightboxContainerUI.width()/2-originalTextW/2+'px');
        loadingTextUI.css('top', imageTop+originalImageLoadH+10+'px');        
        
        TweenMax.to(imgLoading, 0, {css:{scale: 0}, ease:Power3.easeIn});
        TweenMax.to(loadingTextUI, 0, {css:{scale: 0}, ease:Power3.easeIn});
        
        lightboxContainerUI.css('height', '1px');
        TweenMax.to(lightboxContainerUI, .3, {css:{height: firstGap+'px'}, ease:Power3.easeIn});
        TweenMax.to(lightboxContainerUI, .3, {css:{top:jQuery(window).height()/2-firstGap/2+'px'}, ease:Power3.easeIn});
        
        TweenMax.to(imgLoading, .3, {css:{scale: 1, opacity: 1}, delay: .2, ease:Power3.easeIn});
        TweenMax.to(loadingTextUI, .3, {css:{scale: 1, opacity: 1}, delay: .3, ease:Power3.easeIn, onComplete: loadImageFirstTime});
    }
    
    var currentIndex;
    function loadImageFirstTime(){
        currentIndex = currentSliderIndex;
        loadImage(imageLoadedFirstTime);
    }
    
    //load first time
    function imageLoadedFirstTime(img){
        if(imagesAC[currentIndex].img==""){
            imagesAC[currentIndex].img = img;
        }
        expandLightbox();
    }
    
    //expand lightbox
    function expandLightbox(){
        TweenMax.to(imgLoading, .3, {css:{scale: 0, opacity: 0}, ease:Power3.easeIn});
        TweenMax.to(loadingTextUI, .3, {css:{scale: 0, opacity: 0}, delay: .1, ease:Power3.easeIn});
        
        TweenMax.to(lightboxContainerUI, .5, {css:{height: '100%'}, ease:Power3.easeIn});
        TweenMax.to(lightboxContainerUI, .5, {css:{top: '0px'}, ease:Power3.easeIn, onComplete: showImage});        
    }
    
    var currentImage;
    function showImage(){
        secure_screen.secure(false);
        isActive = true;
        lightboxContainerUI.css('width', '100%');
        lightboxContainerUI.css('height', '100%');
        if(imagesAC[currentIndex].img==undefined || imagesAC[currentIndex].img == "" || imagesAC[currentIndex].img ==null){
            //broken image
            return;
        }
        currentImage = imagesAC[currentIndex].img;
        currentImage.css('position', 'absolute');
        currentImage.css('max-width', '80%');
        currentImage.css('max-height', '80%');
        currentImage.css('opacity', 0);
        currentImage.appendTo(lightboxImageContainer);
            currentImage.bind('contextmenu', function(e) {
                return false;
            });        
        windowResize();
        TweenMax.to(currentImage, .4, {css:{opacity: 1}, ease:Power3.easeIn});
        TweenMax.to(lightboxTitle, .4, {css:{opacity: 1}, delay: .2, ease:Power3.easeIn});
        TweenMax.to(lightboxControls, .4, {css:{opacity: 1}, delay: .2, ease:Power3.easeIn});
        
        validateButtons();    
    }
    
    
    function loadImage(callBack){
        var url = imagesAC[currentIndex].url;
        var axjReq = new JQueryAjax();        
        axjReq.loadImage(url, function(img){            
            callBack(img);
        });        
    }
    
    //resize handler
    function windowResize(){
        if(isActive){
            if(currentImage!=null&&currentImage!=undefined){
                currentImage.css('left', lightboxImageContainer.width()/2-currentImage.width()/2+'px');
                currentImage.css('top', lightboxImageContainer.height()/2-currentImage.height()/2+'px');
            }
            lightboxTitle.css('left', lightboxImageContainer.width()/2-lightboxTitle.width()/2+'px');
            lightboxControls.css('left', lightboxImageContainer.width()/2-lightboxControls.width()/2+'px');
            
            preloaderGIF.css('left', lightboxImageContainer.width()/2-25/2+'px');
            preloaderGIF.css('top', lightboxImageContainer.height()/2-25/2+'px');
        }
    }    
}
PortfolioLightbox.first = true;




