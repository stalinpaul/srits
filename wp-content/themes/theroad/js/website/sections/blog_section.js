function BlogSection(){
    
    var currentBlogPageContentUI;
    this.init = function(){
        currentBlogPageContentUI = jQuery('.blogPageContent');
        initBehavior();
    }
    
    var boxes;
    var pagination;
    var urlToLoad;
    var buttonStyle;
    function initBehavior(){
        boxes = new Array();
        pagination = new Array();        
        jQuery('.blogPageContent .postBox').each(function(indx){
            //boxes.push();
            jQuery(this).find('.blogReadMore').click(function(e){
                e.preventDefault();
                var url_single = jQuery(this).attr('href');
                openBlogPostSingle(url_single);
            });
            
            jQuery(this).find('.blogBoxTitle').click(function(e){
                e.preventDefault();
                var url_single = jQuery(this).attr('href');
                openBlogPostSingle(url_single);
            });            
            
            var featuredImageA = jQuery(this).find('.aBlogFeaturedImg');
            featuredImageA.click(function(e){
                e.preventDefault();
                var url_single = jQuery(this).attr('href');
                openBlogPostSingle(url_single);                
            });
            
            featuredImageA.hover(function(e){
                var idBox = jQuery(this).attr('data-boxid');
                var shadow = jQuery('#'+idBox).find('.blogPImageShadow');
                TweenMax.to(shadow, .1, {css:{opacity: .3}});
            }, function(e){
                var idBox = jQuery(this).attr('data-boxid');
                var shadow = jQuery('#'+idBox).find('.blogPImageShadow');
                TweenMax.to(shadow, .2, {css:{opacity: 1}, ease:Power3.easeIn});
            });            
            
        });
        
        jQuery('.blogPagination a').each(function(indx){
            jQuery(this).click(function(e){
                e.preventDefault();
                var url = jQuery(this).attr('href');
                openNewBlogPage(url);
            });
        });
    }
    
    
    function openNewBlogPage(url){
        urlToLoad = url;
        secure_screen.secure(true);
        TweenMax.to(jQuery('.blogPageContent'), .9, {css:{height: 150+'px'}, ease:Power3.easeIn});
        TweenMax.to(jQuery('.innerPageContent'), .8, {css:{opacity: 0}, ease:Power3.easeIn, onComplete: initPreloader});
        mainWebsite.goToSlideByDataType('blogPage');
    }
    
    function initPreloader(){
        jQuery('.innerPageContent').remove();
        
        removeLoading();
        
        mainPreloaderLogo.appendTo(jQuery('.blogPageContent'));
        preloadTextGlobal.appendTo(jQuery('.blogPageContent'));  
        
        TweenMax.to(mainPreloaderLogo, 0, {css:{scale: 0, opacity: 0}});
        preloadTextGlobal.css('opacity', 0);
        TweenMax.to(preloadTextGlobal, 0, {css:{scale: 0, opacity: 0}, delay:.2, onComplete: iOSpatch001});               
                      
        
    }
    
    //pach for ios
    function iOSpatch001(){
        mainPreloaderLogo.css('left', jQuery('.blogPageContent').width()/2-mainPreloaderLogoW/2+'px');
        mainPreloaderLogo.css('top', 20+'px');
        
        preloadTextGlobal.css('left', jQuery('.blogPageContent').width()/2-preloadTextGlobalW/2+'px');
        preloadTextGlobal.css('top', 20+mainPreloaderLogoH+10+'px');        
        
        TweenMax.to(mainPreloaderLogo, .5, {css:{scale: 1, opacity: 1}, ease:Power3.easeIn});
        TweenMax.to(preloadTextGlobal, .5, {css:{scale: 1, opacity: 1}, delay: .1, ease:Power3.easeIn, onComplete: loadBlogPage});        
    }
    
    function removeLoading(){
        try{
            mainPreloaderLogo.remove();
            preloadTextGlobal.remove();            
        }catch(e){}        
    }
    
    //load blog page
    function loadBlogPage(){
        var axjReq = new JQueryAjax();
        axjReq.getData(urlToLoad, function(data){
            //page loaded
           closePreloader(data);
        }, function(){alert('could not load blog page!')});        
    }
    
    var newPageData;
    function closePreloader(data){
        newPageData = jQuery(data);
        TweenMax.to(mainPreloaderLogo, .5, {css:{scale:0}, ease:Back.easeIn});
        TweenMax.to(preloadTextGlobal, .4, {css:{scale:0}, ease:Power3.easeOut, delay: .3, onComplete: addNewPage});      
    }
    
    //add new page content
    function addNewPage(){
        removeLoading();
        try{
            resetSingle();
        }catch(e){}
        var innerContent = newPageData.find('.innerPageContent');
        innerContent.css('opacity', 0);
        innerContent.appendTo(jQuery('.blogPageContent'));
        TweenMax.to(innerContent, .5, {css:{opacity:1}, ease:Power3.easeIn});
        jQuery('.blogPageContent').css('height', 'auto');
        initBehavior();
        secure_screen.secure(false);
    }
    
    
    
    var currentSingleURL;
    var singleTempUrl;
    var isSingleOpen;
    //open blog single page
    function openBlogPostSingle(url){
        singleTempUrl = url;
        var pozition = jQuery('.openedBlogPost').offset();
        if(isMobile){           
            jQuery('html,body').animate({
                scrollTop: pozition.top-80
            }, 1500, 'easeOutQuad', function onComplete(){
                scrolledToSingle();
            });
        }else{            
            var is_safari = navigator.userAgent.indexOf("Safari") > -1;
            is_safari = true;
            if(is_safari){
                jQuery('html,body').animate({
                    scrollTop: pozition.top-80
                }, 1500, 'easeInOutQuad', function onComplete(){
                    scrolledToSingle();
                });           
            }else{
                TweenMax.to(jQuery(window), 1.3, {scrollTo:{y: pozition.top-80}, ease:Power3.easeInOut, onComplete: scrolledToSingle});
            }  
        }        
    }
    //open single
    function scrolledToSingle(){
        if(currentSingleURL==singleTempUrl){
            return;
        }
        currentSingleURL = singleTempUrl;
        if(isSingleOpen){
            //close blog
            singleTempUrl = currentSingleURL;
            closeSingle(closeSingleAndLoadNew, 1);            
            return;
        }
        openSingle();
    }
    
    //open single
    function openSingle(fromStart){
        secure_screen.secure(true); 
        isSingleOpen = true;
        removeLoading();
        
        showPreloaderSingle();
    }
    
    function resetSingle(){
        isSingleOpen = false;
    }
    
    function showPreloaderSingle(){
        mainPreloaderLogo.appendTo(jQuery('.openedBlogPost'));
        preloadTextGlobal.appendTo(jQuery('.openedBlogPost'));  
        
        
        TweenMax.to(mainPreloaderLogo, 0, {css:{scale: 0, opacity: 0}});
        preloadTextGlobal.css('opacity', 0);
        TweenMax.to(preloadTextGlobal, 0, {css:{scale: 0, opacity: 0}, delay: .2, onComplete: iOSpatch002});               
                                
    }
    
    function iOSpatch002(){
        mainPreloaderLogo.css('left', jQuery('.openedBlogPost').width()/2-mainPreloaderLogoW/2+'px');
        mainPreloaderLogo.css('top', 20+'px');
        
        preloadTextGlobal.css('left', jQuery('.openedBlogPost').width()/2-preloadTextGlobalW/2+'px');
        preloadTextGlobal.css('top', 20+mainPreloaderLogoH+10+'px');        
        
        TweenMax.to(jQuery('.openedBlogPost'), .3, {css:{height: 150+'px'}, ease:Power3.easeIn});
        TweenMax.to(mainPreloaderLogo, .5, {css:{scale: 1, opacity: 1}, ease:Power3.easeIn});
        TweenMax.to(preloadTextGlobal, .5, {css:{scale: 1, opacity: 1}, delay: .1, ease:Power3.easeIn, onComplete: loadSinglePage});         
    }
    
    function loadSinglePage(){
        var axjReq = new JQueryAjax();
        axjReq.getData(currentSingleURL, function(data){
            //page loaded
           closePreloaderSingle(data);
        }, function(){alert('could not load single blog page!')});         
    }
    
    var currentSinglePageContent;
    var currentData;
    function closePreloaderSingle(data){
        currentData = data;        
        TweenMax.to(mainPreloaderLogo, .5, {css:{scale:0}, ease:Back.easeIn});
        TweenMax.to(preloadTextGlobal, .4, {css:{scale:0}, ease:Power3.easeOut, delay: .3, onComplete: addNewSinglePage});         
    }
    
    //add new single page
    function addNewSinglePage(){
        removeLoading();

        currentSinglePageContent = jQuery(currentData);                
        currentSinglePageContent.css('opacity', 0);
        currentSinglePageContent.appendTo(jQuery('.openedBlogPost'));                
        TweenMax.to(jQuery('.blogSingleMainContent'), .5, {css:{opacity:1}, ease:Power3.easeIn});
        jQuery('.openedBlogPost').css('height', 'auto');
        secure_screen.secure(false);
        buttonStyle = jQuery('.blogSingleMainContent').attr('data-buttonStyle');
        buttonsSingleHover(); 
        buttonsSingleAction();
        initFormActions();    
    }
    
    function buttonsSingleAction(){
        jQuery('.blogSingleMainContent .closeBlogSingleBTN').click(function(e){
            e.preventDefault();
            closeSingle(closedForGood);
        });
        jQuery('.blogSingleMainContent .nextSingleBTN').click(function(e){
            e.preventDefault();
            singleTempUrl = jQuery(this).attr('href');
            closeSingle(closeSingleAndLoadNew, 1);
        });
        jQuery('.blogSingleMainContent .prevSingleBTN').click(function(e){
            e.preventDefault();
            singleTempUrl = jQuery(this).attr('href');
            closeSingle(closeSingleAndLoadNew, 1);
        });
        jQuery('.blogSingleMainContent .commentsBTN').click(function(e){
            e.preventDefault();
            var pozition = jQuery('.commentsContent').offset();
            if(isMobile){           
                jQuery('html,body').animate({
                    scrollTop: pozition.top-80
                }, 1500, 'easeOutQuad');
            }else{            
                TweenMax.to(jQuery(window), 1.3, {scrollTo:{y: pozition.top-80}, ease:Power3.easeInOut});
            } 
        });              
                        
    }
    
    function buttonsSingleHover(){
        jQuery('.blogSingleMainContent .genericBoxButton').each(function(indx){
            jQuery(this).hover(function(e){
                TweenMax.to(jQuery(this), .1, {css:{opacity:.8}, ease:Power3.easeIn});
            }, function(e){
                TweenMax.to(jQuery(this), .1, {css:{opacity:1}, ease:Power3.easeIn});
            });
        });
    }    
    
    //close project
    function closeSingle(callBack, closeFactor){
        (closeFactor==undefined)?closeFactor=0:closeFactor=150;
        secure_screen.secure(true); 
        isSingleOpen = false;
        TweenMax.to(jQuery('.openedBlogPost'), .7, {css:{height: closeFactor+'px'}, ease:Power3.easeIn, onComplete: callBack});
        TweenMax.to(jQuery('.blogSingleMainContent'), .7, {css:{opacity: 0}, delay: .3, ease:Power3.easeIn});
    }
    
    function closedForGood(){
        secure_screen.secure(false);
        gccSingle();
        //return to blog;
        mainWebsite.goToSlideByDataType('blogPage');
    }
    
    function closeSingleAndLoadNew(){
        secure_screen.secure(false);
        gccSingle();
        currentSingleURL = singleTempUrl;
        openSingle();       
    }
    
    function gccSingle(){
        try{
            currentSingleURL = '-283498';
            jQuery('.openedBlogPost').empty();
        }catch(e){}        
    }
    
    
        
        //HANDLE COMMENTS            
        var postCommentsForm;
        var postCommentsFormBTN;
        var commentsUI;
        var noticeMsg;
        var errorMsg;        
                
        //default post comment
        function initFormActions(){
            commentsUI = jQuery('#commentsUI')
            noticeMsg = commentsUI.attr('data-notice');
            errorMsg = commentsUI.attr('data-error');
            postCommentsForm = jQuery('#commentform');
            postCommentsFormBTN = jQuery('#submit');
            
            postCommentsFormBTN.addClass('genericButton');
            //console.log(buttonStyle+'button style');
            postCommentsFormBTN.addClass(buttonStyle);      
            
            postCommentsFormBTN.click(function(e){
                e.preventDefault();
                processForm(postCommentsForm);
            });           
        }
        
        function processForm(commentform){
            secure_screen.secure(true);
            var formdata=commentform.serialize();
            var formurl=commentform.attr('action');

            jQuery.ajax({
                type: 'post',
                url: formurl,
                data: formdata,
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    secure_screen.secure(false);
                    jQuery('#comment_notice').html(errorMsg);
                },
                success: function(data, textStatus){
                    secure_screen.secure(false);
                    if(data=="success"){
                        //success
                    }
                    else{
                        //'***Your comment is awaiting moderation***');
                        jQuery('#comment_notice').html(noticeMsg);
                    }
                    //alert(data);
                    reloadComments();               
                }
            });
        }
        
        function reloadComments(){
            reloadloadSinglePage();
        }
        
        function reloadloadSinglePage(){
            var axjReq = new JQueryAjax();
            axjReq.getData(currentSingleURL, function(data){
                //page loaded
               refreshComments(data);
            }, function(){alert('could not reload comments!')});         
        } 
        
        function refreshComments(data){
            jQuery('.commentsContent').find('.commentlist').empty();
            var allpage = jQuery(data);
            var newContentHTML = allpage.find('.commentlist').html();
            jQuery(newContentHTML).appendTo(jQuery('.commentlist'));
            document.getElementById('commentform').value = "";
            initFormActions();
        }             
        
}




