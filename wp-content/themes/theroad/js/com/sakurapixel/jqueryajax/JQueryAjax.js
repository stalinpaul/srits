
function JQueryAjax(){
    /**
     * load data trough get
     */
    this.getData = function(path, successCallBack, failCallBack){
        jQuery.get(path, function(response){
           //first responce                   
        }).error(function() { failCallBack(); })
        .success(function(response) {
            successCallBack(response);           
        })        
    }
    
    this.loadImage = function(path, successCallBack, failCallBack){
        var _url = path;
        var _im =jQuery("<img />");
        _im.bind("load",function(){ 
                successCallBack(jQuery(this));
            });
        _im.attr('src',_url);
    }
}
