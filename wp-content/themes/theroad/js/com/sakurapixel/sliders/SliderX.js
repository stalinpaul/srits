function SliderX(){
    
    var sliderUI;
    var sliderID;
    var sliderH;
    
    var largeImages;
    var thumbImages;
    
    var holdObjects;
    
    var countLoaded;
    this.init = function(id, h){
        sliderID = id;
        sliderUI = jQuery('#'+id);
        sliderH = h;
        
        largeImages = new Array();
        thumbImages = new Array();
        holdObjects = new Array();
        
        jQuery('#'+id+' .sliderx-large li').each(function(indx){            
            largeImages.push(jQuery(this).find('img').attr('src'));
        });
        
        jQuery('#'+id+' .sliderx-thumbs li').each(function(indx){            
            thumbImages.push(jQuery(this).find('img').attr('src'));
        });        
        
        jQuery('#'+id+' .sliderx-large').remove();        
        jQuery('#'+id+' .sliderx-thumbs').remove();                
        initCSS();                
        EventBus.addEventListener(Event.WINDOW_RESIZE, windowResize);
        addControls();
        countLoaded = -1;
        loadImage();
    }
    
    var sliderNav;
    var sliderHoverNav;
    var slidesContainer;
    function addControls(){
        slidesContainer = jQuery('<div class="slidesContainer"></div>');
        slidesContainer.appendTo(sliderUI);        
        sliderNav = jQuery('<ul class="sliderXNav"></ul>');
        sliderNav.appendTo(sliderUI);
        
        sliderHoverNav = jQuery('<ul class="sliderHoverNav"></ul>');
        sliderHoverNav.appendTo(sliderUI);
    }
    function loadImage(){
        countLoaded++;
        if(countLoaded<largeImages.length){
            //load image
            var url = largeImages[countLoaded];
            var axjReq = new JQueryAjax();        
            axjReq.loadImage(url, function(img){ 
                axjReq.loadImage(thumbImages[countLoaded], function(small){
                    imageLoaded(img, small);
                });                           
            });             
        }else{
            //finish        
        }
    }
    
    function handleHoverOn(id){
        var indx = extractIndex(id);
        if(indx==currentIndex){
            return;
        }
        var thumb = holdObjects[indx].small;
        thumb.css('top', '0px');
        TweenMax.to(thumb, .4, {css:{opacity:1, top: '-59px'}, ease:Power3.easeOut});        
    }
    
    function handleHoverOff(id){
        var indx = extractIndex(id);
        if(indx==currentIndex){
            return;
        }        
        var thumb = holdObjects[indx].small;
        TweenMax.to(thumb, .3, {css:{opacity:0, top: '0px'}, ease:Power3.easeOut});
    }    
    
    function imageLoaded(img, small){
        addItem(img, small);
        loadImage();
    }
    
    function addItem(img, small){
        img.css('position', 'absolute');
        img.css('left', '0px');
        img.css('top', '0px');
        img.css('opacity', 0);
        img.appendTo(slidesContainer);        
        
        var uid = uuid();        
        
        var linkLISmall = jQuery('<li id="'+uid+'"></li>');
        small.appendTo(linkLISmall);
        linkLISmall.appendTo(sliderHoverNav);        
        
        var linkLI = jQuery('<li><a href="'+uid+'" class="sliderXNavLink"></a></li>');
        linkLI.appendTo(sliderNav);
        linkLI.find('a').click(function(e){
            e.preventDefault();
            handleHoverOff(jQuery(this).attr('href'));
            showSlide(jQuery(this).attr('href'));
        });
        
        linkLI.find('a').hover(function(e){
            handleHoverOn(jQuery(this).attr('href'));
        }, function(e){
            handleHoverOff(jQuery(this).attr('href'));
        });
        
        small.css('opacity', 0);
        holdObjects.push({img:img, id: uid, linkHref: linkLI.find('a'), small: small, linkLISmall: linkLISmall});
        
        jQuery('#'+sliderID+' .sliderXNav li').each(function(indx){            
            jQuery(this).css('width', (100/holdObjects.length)+'%');
            if(indx==largeImages.length-1){
                jQuery(this).find('a').css('marginRight', '0px');                            
            }
        });
        
        jQuery('#'+sliderID+' .sliderHoverNav li').each(function(indx){            
            jQuery(this).css('width', (100/holdObjects.length)+'%');
            jQuery(this).find('img').css('left', jQuery(this).width()/2-jQuery(this).find('img').width()/2+'px');
        });                        
        
        if(holdObjects.length==1){
            showSlide(uid);
        }
    }
    
    var currentSlide;
    var currentIndex;
    function showSlide(id){        
        var indx = extractIndex(id);
        if(indx==currentIndex){
            //console.log('CURRENT - SAME');
            return;
        }
        var direction = "default";
        if(currentIndex!=undefined&&currentIndex<indx){
            direction = "right";
        } 
        if(currentIndex!=undefined&&currentIndex>indx){
            direction = "left";
        }         
        currentIndex = indx;
        selectNewObject(currentIndex, direction);
    }
    
    var currentSelectedSlide=0;
    function selectNewObject(indx, direction){
        for(var i=0;i<holdObjects.length;i++){
            holdObjects[i].linkHref.removeClass('selectedSliderColor');
            var slide = holdObjects[i].img;
            slide.removeClass('slideTopIndex');            
            if(i!=indx){
                //slide.css('opacity', 0);
                TweenMax.to(slide, .3, {css:{opacity:0}, ease:Power3.easeIn});
            }            
            if(i==indx){
                currentSelectedSlide = i;
                TweenMax.to(slide, .3, {css:{opacity:1}, ease:Power3.easeIn});
                slide.addClass('slideTopIndex');                
                holdObjects[i].linkHref.addClass('selectedSliderColor');                                              
            }
        }
    }
    
    function extractIndex(id){
        var indx = 0;
        for(var i=0;i<holdObjects.length;i++){
            if(holdObjects[i].id==id){
                indx = i;
                break;
            }
        }
        return indx;
    }
    
    this.getCurrent = function(){
        return currentSelectedSlide;
    }
    
    function initCSS(){
        sliderUI.css('height', sliderH+'px');
        sliderUI.css('overflow', 'hidden');
        sliderUI.css('backgroundColor', '#000');
        sliderUI.css('position', 'relative');        
    }
    
    function windowResize(){
        for(var i=0;i<holdObjects.length;i++){
            holdObjects[i].img.css('left', sliderUI.width()/2-holdObjects[i].img.width()/2+'px');                        
            holdObjects[i].small.css('left', holdObjects[i].linkLISmall.width()/2-holdObjects[i].small.width()/2+'px');
        }
    }
    
    
    function uuid()
    {
       var chars = '0123456789abcdef'.split('');
    
       var uuid = [], rnd = Math.random, r;
       uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
       uuid[14] = '4'; // version 4
    
       for (var i = 0; i < 36; i++)
       {
          if (!uuid[i])
          {
             r = 0 | rnd()*16;
    
             uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
          }
       }
    
       return uuid.join('');
    }    
    
}
