//items {imgURL: val, itemDescription: val}
function PhotoShootLightbox(items, startIndex){
    
    //var lightBoxImageMouseMove = true;//change to false if you don't want image to move'
    var lightBoxImageMouseMove = false;
    if(globalJS.lightboxisMouseMove=='ON'){
        lightBoxImageMouseMove = true;
    }
    
    var galleryItems = items;
    var currentIndex = startIndex;
    var preloader;
    
    init();
    /**
     * init lightbox container
     */
    function init(){
        //main lightbox container
        jQuery('<div id="photoShootLightbox"></div>').appendTo(jQuery('#contentWrapper'));
        jQuery('#photoShootLightbox').css('position', 'fixed');
        jQuery('#photoShootLightbox').css('top', '0px');
        jQuery('#photoShootLightbox').css('left', '0px');
        jQuery('#photoShootLightbox').css('width', '100%');
        jQuery('#photoShootLightbox').css('height', '100%');
        jQuery('#photoShootLightbox').css("background-image", "url('"+globalJS.RESOURCES_PATH+"/photoshootlightbox/blackBackground.png')");
        jQuery('#photoShootLightbox').css('z-index', 9000);
                
        
        //lightbox content
        jQuery('<div id="lightboxContent"></div>').appendTo(jQuery('#photoShootLightbox'));
        jQuery('#lightboxContent').css('opacity', 0);
        jQuery('#lightboxContent').css('position', 'absolute');
        jQuery('#lightboxContent').css('z-index', 2001);
        jQuery('#lightboxContent').css('width', '80%');
        jQuery('#lightboxContent').css('height', '80%');
        jQuery('#lightboxContent').css('background-color', '#FFFFFF');
        jQuery('#lightboxContent').animate({
            opacity: 1
        }, 200);
        
        //image content
        jQuery('<div id="imageContent"></div>').appendTo(jQuery('#lightboxContent'));
        jQuery('#imageContent').css('background-color', '#0e0e0e');
        jQuery('#imageContent').css('position', 'relative');
        jQuery('#imageContent').css('left', '10px');
        jQuery('#imageContent').css('top', '10px');
        jQuery('#imageContent').css('overflow', 'hidden');
        
        jQuery('#imageContent').bind('mousemove', function(event){
            onMouseMove(event);            
        });
                
        
        //left button
        jQuery('<div id="leftBTN"><img src="'+globalJS.RESOURCES_PATH+'/'+'photoshootlightbox/left_btn.png" alt="left" /></div>').appendTo(jQuery('#lightboxContent'));
        jQuery('#leftBTN').css('position', 'absolute');
        jQuery('#leftBTN').css('z-index', 2005);
        jQuery('#leftBTN').css('left', '-40px');
        jQuery('#leftBTN').css('cursor', 'pointer');
        jQuery('#leftBTN').hover(function(){
            jQuery(this).css('left', '-41px');
        }, function(){
            jQuery(this).css('left', '-40px');
        });
        jQuery('#leftBTN').click(function(){
            currentIndex--;
            openImage(currentIndex);
        });
        
        //right button
        jQuery('<div id="rightBTN"><img src="'+globalJS.RESOURCES_PATH+'/'+'photoshootlightbox/right_btn.png" alt="right" /></div>').appendTo(jQuery('#lightboxContent'));
        jQuery('#rightBTN').css('position', 'absolute');
        jQuery('#rightBTN').css('z-index', 2006);
        jQuery('#rightBTN').css('right', '-40px');
        jQuery('#rightBTN').css('cursor', 'pointer');
        jQuery('#rightBTN').hover(function(){
            jQuery(this).css('right', '-41px');
        }, function(){
            jQuery(this).css('right', '-40px');
        });
        jQuery('#rightBTN').click(function(){
            currentIndex++;
            openImage(currentIndex);
        });                       
        
        //close lightbox button
        jQuery('<div id="lightboxClose"><img src="'+globalJS.RESOURCES_PATH+'/'+'photoshootlightbox/close_lightbox.png" alt="" /></div>').appendTo(jQuery('#lightboxContent'));
        jQuery('#lightboxClose').css('position', 'absolute');
        jQuery('#lightboxClose').css('z-index', 2010);
        jQuery('#lightboxClose').css('top', '0px');
        jQuery('#lightboxClose').css('right', '0px');
        jQuery('#lightboxClose').css('cursor', 'pointer');
        jQuery('#lightboxClose').click(function(){
            try{
                jQuery('#imageContent').unbind("mousemove");
            }catch(e){}
            jQuery('#photoShootLightbox').animate({
            opacity: 0
            }, 300, 'easeOutQuint', function onComplete(){
               jQuery('#photoShootLightbox').remove(); 
            });            
        });
        
        //text container
        jQuery('<div id="textContainer"></div>').appendTo(jQuery('#lightboxContent'));
        jQuery('#textContainer').css('position', 'absolute');
        jQuery('#textContainer').css('z-index', 2020);
        jQuery('#textContainer').css('bottom', '-50px');
        jQuery('#textContainer').css('height', '40px');
        jQuery('#textContainer').css('overflow', 'hidden');
        jQuery('<p id="txt"></p>').appendTo(jQuery('#textContainer'));
        jQuery('#txt').css('width', '100%');
        jQuery('#txt').css('font-family', 'Open Sans, sans-serif');
        jQuery('#txt').css('font-weight', '300');
        jQuery('#txt').css('font-size', '12px');
        jQuery('#txt').css('position', 'relative');
        jQuery('#txt').css('color', '#FFFFFF');
        jQuery('#txt').css('text-align', 'center');
        jQuery('#txt').css('height', '22px');
        jQuery('#txt').css('overflow', 'hidden');
        jQuery('#txt').css('opacity', 0);

        //preloader
        jQuery('<div id="lightBoxPreloader"><img src="'+globalJS.RESOURCES_PATH+'/'+'photoshootlightbox/preloader.gif" /></div>').appendTo(jQuery('#photoShootLightbox'));
        preloader = jQuery('#lightBoxPreloader');
        preloader.css('position', 'absolute');
        preloader.css('z-index', 2050);
        
        
        //lightbox restrict
        jQuery('<div id="restrictLightbox"></div>').appendTo(jQuery('#photoShootLightbox'));
        jQuery('#restrictLightbox').css('position', 'fixed');
        jQuery('#restrictLightbox').css('width', '100%');
        jQuery('#restrictLightbox').css('height', '100%');
        jQuery('#restrictLightbox').css('z-index', 2100);        
        
                     
        //add resize listener
        jQuery(window).resize(function(){
            widowResized();
        });
        widowResized();
        showPreloader(false);
        openImage(currentIndex);        
    }
    
    var loadingNewImage=false;
    //move picture
    function onMouseMove(e){
        if(!lightBoxImageMouseMove){
            return;
        }
        if(loadingNewImage){
            return;
        }
        if(currentImageUI==null||currentImageUI==undefined){
            return;
        }
        var img = currentImageUI.find('img');
        if(img.width()<jQuery('#imageContent').width() && img.height()<jQuery('#imageContent').height()){
            return;
        }
        
        var mouseCoordsX=(e.pageX - jQuery('#imageContent').offset().left);
        var mouseCoordsY=(e.pageY - jQuery('#imageContent').offset().top);
        var mousePercentX=mouseCoordsX/jQuery('#imageContent').width();
        var mousePercentY=mouseCoordsY/jQuery('#imageContent').height();
                
        var xratio = (img.width() - jQuery('#imageContent').width())/(jQuery('#imageContent').width()); //Map the difference of the picframe and the width of the image to the mouseX of the picframe as a ratio.
        var yratio = (img.height() - jQuery('#imageContent').height())/(jQuery('#imageContent').height());
        
        var xoff = -mouseCoordsX * xratio; //We want the mouseX of the frame, not the stage so it starts at 0
        var yoff = -mouseCoordsY * yratio; //same for the mouseY.                
      
        currentImageUI.stop().animate({
            left: xoff,
            top: yoff
        }, 2000, 'easeOutCirc');                                                     
    }
    
    //check display left right buttons
    function checkLeftRightButtons(){
        if(currentIndex<=0){
            currentIndex = 0;
            jQuery('#leftBTN').css('visibility', 'hidden');
        }else{
            jQuery('#leftBTN').css('visibility', 'visible');
        }
        if(currentIndex>=galleryItems.length-1){
            currentIndex = galleryItems.length-1;
            jQuery('#rightBTN').css('visibility', 'hidden');
        }else{
            jQuery('#rightBTN').css('visibility', 'visible');
        }
    }
    
    var currentText;
    function openImage(index){
        loadingNewImage = true;
        try{
            currentImageUI.stop();
        }catch(e){}
        currentText = "";
        showRestrict(true);
        checkLeftRightButtons();
        showPreloader(true);
        try{
            currentText = galleryItems[index].itemDescription;
        }catch(e){}        
        loadImage(galleryItems[index].imgURL, imageLoaded);
    }
    
    var currentImageUI;
    var tempImageUI;
    var originalImageW=0;
    var originalImageH=0;
    function imageLoaded(img){
        loadingNewImage = false;
        showPreloader(false);
        if(currentImageUI!=null&&currentImageUI!=undefined){
            removeExistingImage();
        }
        tempImageUI = jQuery('<div></div>');
        img.appendTo(tempImageUI);
        tempImageUI.appendTo(jQuery('#imageContent'));
        jQuery('#imageContent img').bind('contextmenu', function(e){
    return false;
}); 
        tempImageUI.css('opacity', 0);     
        showNewImage();        
    }
    //remove existing image
    function removeExistingImage(){
        currentImageUI.animate({
           opacity: 0 
        }, 300, 'easeInQuad', function onComplete(){
            currentImageUI.remove();
        });
        jQuery('#txt').animate({
            opacity: 0
        }, 300, 'easeInQuad');        
    }
    function showNewImage(){
        var img = tempImageUI.find('img');
        originalImageW = img.width();
        originalImageH = img.height();        
        tempImageUI.css('position', 'relative');
        tempImageUI.css('left', jQuery('#imageContent').width()/2-img.width()/2);
        tempImageUI.css('top', jQuery('#imageContent').height()/2-img.height()/2);
        checkResponsiveImage(tempImageUI);
        tempImageUI.animate({
           opacity: 1 
        }, 700, 'easeInQuad', function onComplete(){                     
            currentImageUI = tempImageUI;
            showRestrict(false);
            //add text
            jQuery('#txt').css('opacity', 0);
            jQuery('#txt').html(currentText);
            jQuery('#txt').animate({
                opacity: 1
            }, 700, 'easeOutQuad');                        
        });        
    }
    
    function showPreloader(val){
        if(val){
            preloader.css('visibility', 'visible');
        }else{
            preloader.css('visibility', 'hidden');
        }
    }
    
    //restrict click
    function showRestrict(val){
        if(val){
            jQuery('#restrictLightbox').css('visibility', 'visible');
        }else{
            jQuery('#restrictLightbox').css('visibility', 'hidden');
        }
    }
    
    function loadImage(path, successCallBack){
        var _url = path;
        var _im =jQuery("<img />");
        _im.bind("load",function(){ 
                successCallBack(jQuery(this));
            });
        _im.attr('src',_url);
    }
    
    /**
     * resize image within the lightbox
     */
    function checkResponsiveImage(imageUI){
        if(!lightBoxImageMouseMove){
           if(imageUI!=undefined){
               var img = imageUI.find('img');
               var imgWidth = img.width();
               var imgHeight = img.height();
               var responsiveTriger = 80;
               var lightboxContent = jQuery('#lightboxContent');
               
               if(imgWidth>lightboxContent.width()){
                   var scaleDownPer = (lightboxContent.width()*100)/imgWidth;
                   var newW = (scaleDownPer*imgWidth)/100;
                   var newH = (scaleDownPer*imgHeight)/100;
               }
                   img.css('width', newW+'px'); 
                   img.css('height', newH+'px'); 
                   imageUI.css('left', jQuery('#imageContent').width()/2-newW/2);
                   imageUI.css('top', jQuery('#imageContent').height()/2-newH/2);
                   
               var img = imageUI.find('img');
               var imgWidth = img.width();
               var imgHeight = img.height();                                  
               if(imgHeight>lightboxContent.height()){
                   var scaleDownPer = (lightboxContent.height()*100)/imgHeight;
                   var newW = (scaleDownPer*imgWidth)/100;
                   var newH = (scaleDownPer*imgHeight)/100;
               }  
                   img.css('width', newW+'px'); 
                   img.css('height', newH+'px'); 
                   imageUI.css('left', jQuery('#imageContent').width()/2-newW/2);
                   imageUI.css('top', jQuery('#imageContent').height()/2-newH/2);                                                  
           }           
        }          
    }
    
    /**
     * resized window
     */
    function widowResized(){
        jQuery('#lightboxContent').css('top', jQuery('#photoShootLightbox').height()/2-jQuery('#lightboxContent').height()/2);
        jQuery('#lightboxContent').css('left', jQuery('#photoShootLightbox').width()/2-jQuery('#lightboxContent').width()/2);
        
        jQuery('#imageContent').css('width', jQuery('#lightboxContent').width()-20+'px');
        jQuery('#imageContent').css('height', jQuery('#lightboxContent').height()-20+'px');
        
            jQuery('#imageContent img').css('width', originalImageW+'px');
            jQuery('#imageContent img').css('height', originalImageH+'px');      
        
        checkResponsiveImage(currentImageUI);    
        
        jQuery('#textContainer').css('width', jQuery('#lightboxContent').width());
        
        preloader.css('top', jQuery('#photoShootLightbox').height()/2-50/2);
        preloader.css('left', jQuery('#photoShootLightbox').width()/2-50/2);
        
        jQuery('#leftBTN').css('top', jQuery('#lightboxContent').height()/2-37/2+'px');
        jQuery('#rightBTN').css('top', jQuery('#lightboxContent').height()/2-37/2+'px');
        
        if(!lightBoxImageMouseMove){
            return;
        }
        if(currentImageUI!=null && currentImageUI!=undefined){
            var img = currentImageUI.find('img');
            currentImageUI.css('left', jQuery('#imageContent').width()/2-img.width()/2);
            currentImageUI.css('top', jQuery('#imageContent').height()/2-img.height()/2);
        }            
    }
}
